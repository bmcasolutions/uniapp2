import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { LocationDetailPage } from './location.detail';

@NgModule({
  declarations: [
    LocationDetailPage
  ],
  imports: [
    IonicPageModule.forChild(LocationDetailPage),
    TranslateModule
  ],
  exports: [
    LocationDetailPage,
    TranslateModule
  ]
})
export class LocationDetailPageModule { }
