import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable, Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AccountService } from './account.service';

@Injectable()
export class HttpAuthInjectionInterceptor implements HttpInterceptor {
    private _accountSvc: AccountService;

    constructor(private injector: Injector) { }

    public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (this._accountSvc == null) {
            this._accountSvc = this.injector.get(AccountService, null);
        }
        if (request.url.endsWith('.asp') && request.url.indexOf('login.asp') === -1) {
            return this._accountSvc.getUserInfo()
                .mergeMap((user) => {
                    const customReq = request.clone({
                        body: JSON.stringify({
                            ...JSON.parse(request.body),
                            ...{
                                id: user.id.toString(),
                                pw: user.password,
                                un: user.userName
                            }
                        })
                    });
                    return next.handle(customReq);
                });
        }
        return next.handle(request);
    }
}
