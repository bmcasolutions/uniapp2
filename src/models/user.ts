import { SQLiteObject } from '@ionic-native/sqlite';
import { Observable } from 'rxjs/Observable';
import { BaseModel } from './baseModel';

export class User extends BaseModel {
    public id: number;
    public accountId: number;
    public displayName: string;
    public userName: string;
    public password: string;
    public email: string;
    public lastSync: string;
    public loggedIn: boolean;

    public insert(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`insert into users(id,accountId,displayName,userName,password,
            email,lastSync,loggedIn) values(?,?,?,?,?,?,?,?)`, [this.id, this.accountId, this.displayName,
            this.userName, this.password, this.email, this.lastSync, +this.loggedIn]))
            .map((results) => !!results.rowsAffected);
    }

    public static updateSyncTime(db: SQLiteObject, time: string): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`update users set lastSync=?`, [time]))
            .map((results) => !!results.rowsAffected);
    }

    public static getUser(db: SQLiteObject): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from users', []))
            .map((results) => User.convertToArray(results.rows)[0]);
    }

    public static getLoggedInUser(db: SQLiteObject): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from users where loggedIn=1', []))
            .map((results) => User.convertToArray(results.rows)[0]);
    }

    public static logOutUser(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('update users set loggedIn=0', []))
            .map((results) => !!results.rowsAffected);
    }

    public static deleteLoggedInUser(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('delete from users', []))
            .map((results) => !!results.rowsAffected);
    }
}
