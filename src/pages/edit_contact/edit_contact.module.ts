import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { Edit_contactComponent } from './edit_contact';

@NgModule({
  declarations: [
    Edit_contactComponent
  ],
  imports: [
    IonicPageModule.forChild(Edit_contactComponent),
    TranslateModule
  ],
  exports: [
    Edit_contactComponent,
    TranslateModule
  ]
})
export class Edit_contactModule { }
