import { enableProdMode, NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { MODULES, NATIVES, PROVIDERS } from './app.imports';
import { SharedModule } from './shared.module';

enableProdMode();

@NgModule({
	bootstrap: [IonicApp],
	declarations: [
		MyApp
	],
	entryComponents: [
		MyApp
	],
	imports: [
		MODULES,
		IonicModule.forRoot(MyApp),
		SharedModule
	],
	providers: [
		PROVIDERS,
		NATIVES
	]
})
export class AppModule { }
