import { ErrorHandler, Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

@Injectable()
export class UIErrorHandler extends ErrorHandler {

    constructor(private alertCtrl: AlertController) {
        super();
    }

    public handleError(error) {
        super.handleError(error);
        const alert = this.alertCtrl.create({
            buttons: ['Dismiss'],
            subTitle: error.message,
            title: 'Error!'
        });
        alert.present();
    }
}
