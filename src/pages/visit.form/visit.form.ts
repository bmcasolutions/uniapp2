import { Component } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { Events, IonicPage, LoadingController, ModalController, NavParams, ViewController } from 'ionic-angular';
import { Customer } from '../../models/customer';
import { CustomerContact } from '../../models/customerContact';
import { CustomerLocation } from '../../models/customerLocation';
import { CustomerVisit } from '../../models/customerVisit';
import { Objective } from '../../models/objective';
import { IQuickAddVisitModel, IVisitModel } from '../../models/visit.models';
import { DatabaseProvider } from '../../providers/database';
import { AccountService } from '../../services/account.service';
import { ApiGateway } from '../../services/api.gateway.service';

@IonicPage()
@Component({
    selector: 'visit-form',
    templateUrl: 'visit.form.html'
})
export class VisitFormPage {
    private _db: SQLiteObject;
    private location: any = [];
    public custs: any[] = [];
    private _showCustomerSelecteion: boolean;
    private _minDate: string;
    private _maxDate: string;
    private visit: IVisitModel;
    private quickVisit: IQuickAddVisitModel;

    // tslint:disable-next-line:max-line-length
    private users = [{ UID: 1, DisplayName: 'Brendan McAnerney' }, { UID: 2, DisplayName: 'Graham Milligan' }, { UID: 3, DisplayName: 'Ian Dalgety' }, { UID: 25, DisplayName: 'Anthony Massingham' }, { UID: 26, DisplayName: 'Carl Darnell-Green' }, { UID: 27, DisplayName: 'Ivan Major' }, { UID: 28, DisplayName: 'Jo-Ann James' }, { UID: 29, DisplayName: 'James Gunning' }, { UID: 30, DisplayName: 'Jim Loxton' }, { UID: 31, DisplayName: 'Michael Kendra' }, { UID: 32, DisplayName: 'Neil Bullock' }, { UID: 33, DisplayName: 'Pat Dawson' }, { UID: 34, DisplayName: 'Phil Wakelam' }, { UID: 35, DisplayName: 'Steve Alcock' }, { UID: 36, DisplayName: 'Steve Whyte' }, { UID: 38, DisplayName: 'Tony Da-Silva' }, { UID: 40, DisplayName: 'Scott MacLeod' }, { UID: 42, DisplayName: 'Holly Hood' }, { UID: 46, DisplayName: 'Andrew Hodge' }, { UID: 47, DisplayName: 'Martin Gregg' }];
    private objectives = [];
    private sites = [];
    private contacts = [];

    constructor(public loadingCtrl: LoadingController,
        public params: NavParams, public viewCtrl: ViewController, db: DatabaseProvider,
        private api: ApiGateway, private modalCtrl: ModalController,
        private accSvc: AccountService) {
        this._db = db.database;
        this._minDate = '' + (new Date().getFullYear() - 30);
        this._maxDate = '' + (new Date().getFullYear() + 10);

        if (this.params.get('action') === 'quick') {
            this.quickVisit = {
                customerId: this.params.get('cId') || 0
            } as IQuickAddVisitModel;
            this._showCustomerSelecteion = !this.quickVisit.customerId;
        } else {
            this.visit = {
                id: this.params.get('id'),
                customerId: this.params.get('cId') || 0
            } as IVisitModel;
            this._showCustomerSelecteion = !this.visit.customerId;
        }

        Objective.getAll(this._db)
            .subscribe((objs) => {
                this.objectives = objs;
            });

        if (this.params.get('id') > 0) {
            new CustomerVisit().getById(this._db, this.params.get('id'))
                .subscribe((v: CustomerVisit) => {
                    new CustomerLocation().getByCustomerId(this._db, v.customerId)
                        .subscribe((locations) => {
                            new CustomerContact().getByCustomerId(this._db, v.customerId)
                                .subscribe((contacts) => {
                                    this.contacts = contacts;
                                    this.sites = locations;
                                    this.visit = {
                                        id: this.params.get('id'),
                                        customerId: v.customerId,
                                        siteId: v.siteId,
                                        objective: v.objective,
                                        startDate: v.startDate,
                                        startTime: `${('00' + v.startHour).slice(-2)}:${('00' +
                                            v.startQuarter).slice(-2)}`,
                                        endTime: `${('00' + v.endHour).slice(-2)}:${('00' +
                                            v.endQuarter).slice(-2)}`,
                                        additionalStaff: '',
                                        contactId: (this.contacts
                                            .filter((c) => c.name === v.contactName)[0] || {}).id || 0,
                                        positionHeld: v.positionHeld,
                                        notes: v.notes
                                    } as IVisitModel;
                                    if (this.visit.startDate) {
                                        this.visit.startDate = new Date(+new Date(this.visit.startDate
                                            .replace(/(\d+)\/(\d+)\/(\d+)/, '$2/$1/$3'))
                                            - new Date().getTimezoneOffset() * 60000).toISOString();
                                    }
                                });
                        });
                });
        } else {
            this.loadLocationAndContacts((this.visit || this.quickVisit).customerId || 0);
        }
        Customer.getMyCustomers(this._db)
            .subscribe((c: any[]) => {
                this.custs = c.sort((a: Customer, b: Customer) => {
                    if (a.name.toLowerCase() === b.name.toLowerCase()) {
                        return 0;
                    }
                    return a.name.toLowerCase() > b.name.toLowerCase() ? 1 : -1;
                });
            });
    }

    private loadLocationAndContacts(cId: number): void {
        new CustomerLocation().getByCustomerId(this._db, cId)
            .subscribe((locations) => {
                new CustomerContact().getByCustomerId(this._db, cId)
                    .subscribe((contacts) => {
                        this.contacts = contacts;
                        this.sites = locations;
                    });
            });
    }

    private setEndTimeAtPlus1Hour(visit) {
        if (visit.startTime) {
            const time = visit.startTime;
            visit.endTime = time.replace(/(\d+):(\d+)/, ($0, $1, $2) => {
                let t = +$1 + 1;
                if (t > 23) { t = 0; }
                return `${('00' + t).slice(-2)}:${$2}`;
            });
        }
    }

    public customerChanged($event) {
        this.loadLocationAndContacts((this.visit || this.quickVisit).customerId);
    }

    public save() {
        const loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        if (this.visit) {
            if (!isNaN(new Date(this.visit.startDate).getTime())) {
                this.visit.startDate = new Date(this.visit.startDate).toLocaleDateString('en-GB');
            } else {
                this.visit.startDate = '';
            }
            this.accSvc.getUserInfo()
                .flatMap((u) => {
                    return new CustomerVisit().addEditVisit(this.visit, this.api, this._db);
                }).subscribe((success) => {
                    loading.dismiss();
                    if (success) {
                        this.dismiss(true);
                    }
                }, () => {
                    loading.dismiss();
                });
        } else {
            this.accSvc.getUserInfo()
                .flatMap((u) => {
                    return new CustomerVisit().addQuickVisit(this.quickVisit, this.api, this._db);
                }).subscribe((success) => {
                    loading.dismiss();
                    if (success) {
                        this.dismiss(true);
                    }
                }, () => {
                    loading.dismiss();
                });
        }
    }

    public addContact($event) {
        $event.stopPropagation();
        const cId = (this.visit || this.quickVisit).customerId;
        const modal = this.modalCtrl.create('ContactFormPage', {
            id: 0,
            cid: cId
        });
        modal.onDidDismiss((dataChanged) => {
            if (dataChanged) {
                new CustomerContact().getByCustomerId(this._db, cId)
                    .subscribe((contacts) => {
                        this.contacts = contacts;
                    });
            }
        });
        modal.present();
    }

    public dismiss(certChanged: boolean) {
        this.viewCtrl.dismiss(!!certChanged);
    }
}
