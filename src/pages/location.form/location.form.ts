
import { Component } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { IonicPage, LoadingController, NavParams, ViewController } from 'ionic-angular';
import { CustomerLocation } from '../../models/customerLocation';
import { ILocationModel } from '../../models/cutomer.models';
import { DatabaseProvider } from '../../providers/database';
import { AccountService } from '../../services/account.service';
import { ApiGateway } from '../../services/api.gateway.service';

@IonicPage()
@Component({
    selector: 'location-form',
    templateUrl: 'location.form.html'
})
export class LocationFormPage {

    private _db: SQLiteObject;
    private _loc: ILocationModel = {
        id: 0,
        name: '',
        address1: '',
        address2: '',
        address3: '',
        address4: '',
        postCode: '',
        customerId: this.params.get('cid'),
        isDefault: 0
    };

    constructor(public loadingCtrl: LoadingController,
        public params: NavParams, public viewCtrl: ViewController, db: DatabaseProvider,
        private api: ApiGateway,
        private accSvc: AccountService) {
        this._db = db.database;

        if (this.params.get('id') > 0) {
            new CustomerLocation().getById(this._db, this.params.get('id'))
                .subscribe((loc: CustomerLocation) => {
                    this._loc = {
                        id: loc.id,
                        name: loc.name,
                        address1: loc.address1,
                        address2: loc.address2,
                        address3: loc.address3,
                        address4: loc.address4,
                        postCode: loc.postCode,
                        isDefault: +loc.isDefault,
                        latitude: 0,
                        longitude: 0,
                        customerId: this.params.get('cid')
                    } as ILocationModel;
                });
        }
    }

    public save() {
        const loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.accSvc.getUserInfo()
            .flatMap(() => {
                return new CustomerLocation().addEditLocation(this._loc, this.api, this._db);
            }).subscribe((success) => {
                loading.dismiss();
                if (success) {
                    this.dismiss(true);
                }
            }, () => {
                loading.dismiss();
            });
    }

    public dismiss(certChanged: boolean) {
        this.viewCtrl.dismiss(!!certChanged);
    }
}
