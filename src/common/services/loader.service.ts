import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';

@Injectable()
export class LoaderService {
  private loading: any;
  constructor(private _loading: LoadingController) { }
  public presentLoading() {
    this.loading = this._loading.create({
      content: 'Please wait..',
      spinner: 'crescent'
    });
    return this.loading.present();
  }
  public hideLoading() {
    return this.loading.dismiss();
  }
}
