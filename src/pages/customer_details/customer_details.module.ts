import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { SharedModule } from '../../app/shared.module';
import { Customer_detailsComponent } from './customer_details';

@NgModule({
  declarations: [
    Customer_detailsComponent
  ],
  imports: [
    IonicPageModule.forChild(Customer_detailsComponent),
    TranslateModule,
    SharedModule
  ],
  exports: [
    Customer_detailsComponent,
    TranslateModule
  ]
})
export class Customer_detailsModule { }
