import { SQLiteDatabaseConfig, SQLiteTransaction } from '@ionic-native/sqlite';

declare var window: any;

export class SQLiteMock {
    public db: any;
    public create(config: SQLiteDatabaseConfig): Promise<SQLiteObject> {
        return new Promise((resolve) => {
            this.db = window.openDatabase(config.name, '0.1', config.name, 1024 * 1024);
            resolve(new SQLiteObject(this.db));
        });
    }
}

// tslint:disable-next-line:max-classes-per-file
class MockSQLiteTransaction {
    public _objectInstance: any;
    constructor(obj: any) {
        this._objectInstance = obj;
    }

    public executeSql(statement: string, params: any, success: any, error: any): void {
        this._objectInstance.executeSql(statement, params, success, error);
    }
}

// tslint:disable-next-line:max-classes-per-file
class SQLiteObject {
    public _objectInstance: any;
    constructor(_objectInstance: any) {
        this._objectInstance = _objectInstance;
    }
    public databaseFeatures: {
        isSQLitePluginDatabase: boolean;
    };
    public openDBs: any;

    public transaction(fn: any) {
        this._objectInstance.transaction((t) => {
            fn(new MockSQLiteTransaction(t));
        });
    }

    public executeSql(statement: string, params: any): Promise<any> {
        return new Promise((resolve, reject) => {
            const success = (_t: SQLiteTransaction, dbrv: any) => {
                resolve(dbrv);
            };

            const error = (_t: SQLiteTransaction, err: Error) => {
                // tslint:disable-next-line:no-console
                console.log(statement, params.join(','));
                reject(err);
                return false;
            };
            this._objectInstance.transaction((t: SQLiteTransaction) => {
                t.executeSql(statement, params, success, error);
            });
        });
    }

    public sqlBatch(sqlStatements: any[]): Promise<any> {
        const rv: Promise<any> = new Promise((resolve, reject) => {
            const success = (dbrv: any) => {
                resolve(dbrv);
            };

            const error = (err: Error) => {
                reject(err);
                return false;
            };

            this._objectInstance.transaction((tx: any) => {
                const nsqlStatements = sqlStatements.length;
                for (let ix = 0; ix < nsqlStatements; ix++) {
                    let sql: string;
                    let phs: any[];
                    if (Array.isArray(sqlStatements[ix])) {
                        sql = sqlStatements[ix][0];
                        phs = sqlStatements[ix][1];
                    } else {
                        sql = sqlStatements[ix];
                        phs = [];
                    }
                    tx.executeSql(sql, phs, undefined, error);
                }
            }, error, success);
        });
        return rv;
    }
}

// tslint:disable-next-line:max-classes-per-file
export class SQLitePorterMock {
    public trimWhitespace(str) {
        return str.replace(/^\s+/, '').replace(/\s+$/, '');
    }

    public importSqlToDb(db, sql) {
        const statementRegEx = /(?!\s|;|$)(?:[^;"']*(?:"(?:\\.|[^\\"])*"|'(?:\\.|[^\\'])*')?)*/g;
        let statements = sql
            .replace(/(?:\/\*(?:[\s\S]*?)\*\/)|(?:([\s;])+\/\/(?:.*)$)/gm, '') // strip out comments
            .match(statementRegEx);

        if (statements === null || (Array.isArray && !Array.isArray(statements))) { statements = []; }

        // Strip empty statements
        for (let i = 0; i < statements.length; i++) {
            if (!statements[i]) {
                delete statements[i];
            }
        }
        return db.sqlBatch(statements);
    }
}
