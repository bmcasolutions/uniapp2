import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VisitFormPage } from './visit.form';

@NgModule({
    declarations: [
        VisitFormPage
    ],
    exports: [
        VisitFormPage
    ],
    imports: [
        IonicPageModule.forChild(VisitFormPage)
    ]
})
export class VisitFormPageModule { }
