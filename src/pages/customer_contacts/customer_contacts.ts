import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams, ToastController } from 'ionic-angular';
import { Customer } from '../../models/customer';
import { CustomerContact } from '../../models/customerContact';
import { DatabaseProvider } from '../../providers/database';

@IonicPage()
@Component({
	selector: 'customer_contacts',
	templateUrl: 'customer_contacts.html'
})
export class Customer_contactsComponent {

	public customerContacts: any[] = [];
	private terms: string;
	private cust: Customer = new Customer();

	constructor(public toastCtrl: ToastController,
		public navCtrl: NavController,
		private dbSvc: DatabaseProvider,
		private param: NavParams, private modalCtrl: ModalController
	) {
		this.navCtrl = navCtrl;
		this.cust.isOther = true;
	}

	public ionViewDidEnter() {
		this.loadCustomerContacts();
	}

	public loadCustomerContacts() {
		CustomerContact.getAll(this.dbSvc.database)
			.subscribe((c) => {
				c.forEach((con) => {
					con.text = con.name;
				});
				this.customerContacts = c;
			});
	}

	public addEditContact($event, contact) {
		$event.stopPropagation();
		contact = contact || { id: 0 };
		const modal = this.modalCtrl.create('ContactFormPage', {
			id: contact.id,
			// cid: this.param.get('id')
			cid: contact.customerId
		});
		modal.onDidDismiss((dataChanged) => {
			if (dataChanged) { this.loadCustomerContacts(); }
		});
		modal.present();
	}

	public contactDetail(id: number) {
		this.navCtrl.push('Contact_detailsComponent', { id });
	}
}
