import { SQLiteObject } from '@ionic-native/sqlite';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { Customer_detailsComponent } from '../pages/customer_details/customer_details';
import { ApiGateway } from '../services/api.gateway.service';
import { BaseModel } from './baseModel';
import { Customer } from './customer';

export class CustomerTradeLink extends BaseModel {
    public id: number;
    public customerId: number;
    public name: string;

    public insert(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`insert into customer_trade_links(id,customerId,name)
        values(?,?,?)`, [this.id, this.customerId, this.name]))
            .map((results) => !!results.rowsAffected);
    }

    public update(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`update customer_trade_links set customerId=?,name=?
        where id=?`, [this.customerId, this.name, this.id]))
            .map((results) => !!results.rowsAffected);
    }
    public deleteById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('delete from customer_trade_links where id=?', [id]))
            .map((results) => !!results.rowsAffected);
    }
    public getById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from customer_trade_links where id=?', [id]))
            .map((results) => Customer.convertToArray(results.rows)[0]);
    }

    public getByCustomerId(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from customer_trade_links where customerId=?', [id]))
            .map((results) => CustomerTradeLink.convertToArray(results.rows));
    }

    public static deleteAll(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('delete from customer_trade_links', []))
            .map((results) => !!results.rowsAffected);
    }

    public static syncAllData(api: ApiGateway, db: SQLiteObject): Observable<[string, number]> {
        return api.post('/synctradelinksall.asp', null, null)
            .pipe(map((res: any) => {
                if (!res || !res.CustomerTradelinks) {
                    return [];
                }
                return res.CustomerTradelinks
                    .map((datum) => {
                        const con = new CustomerTradeLink();
                        return Object.assign(con,
                            {
                                id: datum.TradelinkID,
                                customerId: datum.CustomerID,
                                name: datum.TradelinkName
                            } as CustomerTradeLink);
                    });
            })).flatMap((custs: CustomerTradeLink[]) => {
                return Observable.from(custs)
                    .flatMap((datum) => datum.getById(db, datum.id)
                        .flatMap((d) => {
                            if (d) {
                                return datum.update(db);
                            } else {
                                return datum.insert(db);
                            }
                        }).flatMap(() => Observable.of(['customer_trade_link', 1] as [string, number])));
            });
    }

    // public static syncData(api: ApiGateway, db: SQLiteObject): Observable<[string, number]> {
    //     return Customer.getMyCustomers(db)
    //         .flatMap((arr) => Observable.from(arr))
    //         .flatMap((customer: any) => {
    //             return api.post('/synccustomertradelinks.asp', null, { cid: customer.id })
    //                 .pipe(map((res: any) => {
    //                     if (!res || !res.CustomerTradelinks) {
    //                         return [];
    //                     }
    //                     return res.CustomerTradelinks
    //                         .map((datum) => {
    //                             const con = new CustomerTradeLink();
    //                             return Object.assign(con,
    //                                 {
    //                                     id: datum.TradelinkID,
    //                                     customerId: customer.id,
    //                                     name: datum.TradelinkName
    //                                 } as CustomerTradeLink);
    //                         });
    //                 })).flatMap((custs: CustomerTradeLink[]) => {
    //                     return Observable.from(custs)
    //                         .flatMap((datum) => datum.getById(db, datum.id)
    //                             .flatMap((d) => {
    //                                 if (d) {
    //                                     return datum.update(db);
    //                                 } else {
    //                                     return datum.insert(db);
    //                                 }
    //                             }).flatMap(() => Observable.of(['customer_trade_link', 1] as [string, number])));
    //                 });
    //         });
    // }

    public addEditTradelink(model: any, api: ApiGateway, db: SQLiteObject): Observable<boolean> {
        return new CustomerTradeLink().getById(db, model.id)
            .flatMap((v) => {
                const data: any = {
                    TradelinkID: model.id,
                    TradelinkName: model.name
                };
                if (model.id) {
                    data.cid = model.customerId;
                }
                data.cid = model.customerId;
                return api.post(model.id ? '/edittradelink.asp' : '/addtradelink.asp',
                    null, data)
                    .flatMap((res: any) => {
                        return CustomerTradeLink.syncAllData(api, db).last().map(() => true);
                    });
            });

    }

    public static deleteById(model: any, api: ApiGateway, db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('delete from customer_trade_links where id=?', [model.id]))
            .flatMap((v) => {
                const data: any = {
                    TradelinkID: model.id,
                    cid: model.customerId
                };
                return api.post('/deletetradelink.asp',
                    null, data)
                    .flatMap((res: any) => {
                        return CustomerTradeLink.syncAllData(api, db).last().map(() => true);
                    }).catch(() => Observable.of(false));
            });
    }
}
