import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { Edit_visitComponent } from './edit_visit';

@NgModule({
  declarations: [
    Edit_visitComponent
  ],
  imports: [
    IonicPageModule.forChild(Edit_visitComponent),
    TranslateModule
  ],
  exports: [
    Edit_visitComponent,
    TranslateModule
  ]
})
export class Edit_visitModule { }
