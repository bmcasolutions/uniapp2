import { Component } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { Events, IonicPage, LoadingController, NavParams, ViewController } from 'ionic-angular';
import { Action } from '../../models/action';
import { IActionModel } from '../../models/action.models';
import { Customer } from '../../models/customer';
import { DatabaseProvider } from '../../providers/database';
import { AccountService } from '../../services/account.service';
import { ApiGateway } from '../../services/api.gateway.service';

@IonicPage()
@Component({
    selector: 'action-form',
    templateUrl: 'action.form.html'
})
export class ActionFormPage {
    private _db: SQLiteObject;
    private _customers: any[] = [];
    private _minDate: string;
    private _maxDate: string;
    private _showCustomerSelecteion: boolean;
    private _ac: IActionModel = {
        id: 0,
        detail: '',
        name: '',
        customerId: this.params.get('cId'),
        visitId: this.params.get('vId'),
        assignedToId: 0,
        dueDate: '',
        priority: 0
    } as IActionModel;

    // tslint:disable-next-line:max-line-length
    private users = [{ UID: 1, DisplayName: 'Brendan McAnerney' }, { UID: 2, DisplayName: 'Graham Milligan' }, { UID: 3, DisplayName: 'Ian Dalgety' }, { UID: 25, DisplayName: 'Anthony Massingham' }, { UID: 26, DisplayName: 'Carl Darnell-Green' }, { UID: 27, DisplayName: 'Ivan Major' }, { UID: 28, DisplayName: 'Jo-Ann James' }, { UID: 29, DisplayName: 'James Gunning' }, { UID: 30, DisplayName: 'Jim Loxton' }, { UID: 31, DisplayName: 'Michael Kendra' }, { UID: 32, DisplayName: 'Neil Bullock' }, { UID: 33, DisplayName: 'Pat Dawson' }, { UID: 34, DisplayName: 'Phil Wakelam' }, { UID: 35, DisplayName: 'Steve Alcock' }, { UID: 36, DisplayName: 'Steve Whyte' }, { UID: 38, DisplayName: 'Tony Da-Silva' }, { UID: 40, DisplayName: 'Scott MacLeod' }, { UID: 42, DisplayName: 'Holly Hood' }, { UID: 46, DisplayName: 'Andrew Hodge' }, { UID: 47, DisplayName: 'Martin Gregg' }];

    constructor(public loadingCtrl: LoadingController,
        public params: NavParams, public viewCtrl: ViewController, db: DatabaseProvider,
        private api: ApiGateway,
        private accSvc: AccountService) {
        this._db = db.database;
        this._minDate = new Date().toLocaleDateString('en-GB').replace(/(\d+)\/(\d+)\/(\d+)/, '$3-$2-$1');
        this._maxDate = '' + (new Date().getFullYear() + 10);
        Customer.getAll(this._db)
            .subscribe((c) => {
                this._customers = c;
            });
        this._showCustomerSelecteion = !this.params.get('cId');
        if (this.params.get('id') > 0) {
            new Action().getById(this._db, this.params.get('id'))
                .subscribe((ac: Action) => {
                    this._ac = {
                        id: ac.id,
                        detail: ac.detail,
                        name: ac.name,
                        customerId: this.params.get('cId'),
                        assignedToId: ac.assignedTo,
                        dueDate: ac.dueDate,
                        priority: ac.priority
                    } as IActionModel;
                    if (this._ac.dueDate) {
                        this._ac.dueDate = new Date(+new Date(this._ac.dueDate
                            .replace(/(\d+)\/(\d+)\/(\d+)/, '$2/$1/$3'))
                            - new Date().getTimezoneOffset() * 60000).toISOString();
                    }
                });
        }
    }

    public save() {
        if (!isNaN(new Date(this._ac.dueDate).getTime())) {
            this._ac.dueDate = new Date(this._ac.dueDate).toLocaleDateString('en-GB');
        } else {
            this._ac.dueDate = '';
        }
        const loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.accSvc.getUserInfo()
            .flatMap((u) => {
                return new Action().addEditAction(this._ac, this.api, this._db);
            }).subscribe((success) => {
                loading.dismiss();
                if (success) {
                    this.dismiss(true);
                }
            }, (err) => {
                loading.dismiss();
            });
    }

    public dismiss(certChanged: boolean) {
        this.viewCtrl.dismiss(!!certChanged);
    }
}
