import { Component } from '@angular/core';
import {
	AlertController, IonicPage, LoadingController, ModalController, NavController,
	NavParams, ToastController
} from 'ionic-angular';
import { Action } from '../../models/action';
import { DatabaseProvider } from '../../providers/database';
import { ApiGateway } from '../../services/api.gateway.service';

@IonicPage()
@Component({
	selector: 'action_details',
	templateUrl: 'action_details.html'
})

export class Action_detailsComponent {

	private action: any = {};

	constructor(public toastCtrl: ToastController,
		public navCtrl: NavController, private alertCtrl: AlertController,
		private loadingCtrl: LoadingController,
		private dbSvc: DatabaseProvider,
		private param: NavParams,
		private api: ApiGateway,
		private modalCtrl: ModalController) {
		this.navCtrl = navCtrl;
	}

	public ionViewDidEnter() {
		this.loadAction();
	}

	public loadAction() {
		new Action().getById(this.dbSvc.database, this.param.get('id'))
			.subscribe((ac) => {
				this.action = ac;
			});
	}

	public addEditAction($event, ac) {
		$event.stopPropagation();
		ac = ac || { id: 0 };
		const modal = this.modalCtrl.create('ActionFormPage', {
			id: ac.id,
			vId: ac.visitId || 0,
			cId: ac.customerId || 0
		});
		modal.onDidDismiss((dataChanged) => {
			if (dataChanged) { this.loadAction(); }
		});
		modal.present();
	}

	public deleteAction($event, action) {
		const confirm = this.alertCtrl.create({
			buttons: [
				{
					handler: () => {
						// console.log('Disagree clicked');
					},
					text: 'No'
				},
				{
					handler: () => {
						const loading = this.loadingCtrl.create({
							content: 'Please wait...'
						});
						loading.present();

						const v = new Action();
						// v.customerId = this.contact.customerId;
						Action.deleteByActionId(this.action, this.api, this.dbSvc.database)
							.subscribe(() => {
								loading.dismiss();
								this.navCtrl.pop();
							}, () => {
								loading.dismiss();
							});

					},
					text: 'Yes'
				}
			],
			message: 'Are you sure you want to delete this action?',
			title: `Delete action ${this.action.name}?`
		});
		confirm.present();
	}

	public openActionCompletionPage(action) {
		const modal = this.modalCtrl.create('ActionCompletionPage', { id: action.id });
		modal.onDidDismiss((data) => {
			if (data) { this.loadAction(); }
		});
		modal.present();
	}
}
