import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { CustomerProductsPage } from './customer.products';

@NgModule({
	declarations: [
		CustomerProductsPage
	],
	imports: [
		IonicPageModule.forChild(CustomerProductsPage),
		TranslateModule
	],
	exports: [
		CustomerProductsPage,
		TranslateModule
	]
})
export class CustomerProductsModule { }
