/** Represents a Component of Search. */

/** Imports Modules */
import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { Customer } from '../../models/customer';
import { DatabaseProvider } from '../../providers/database';

@IonicPage()
@Component({
	selector: 'locations',
	templateUrl: 'locations.html'
})
export class LocationsComponent {

	public customers: any[] = [];

	constructor(public toastCtrl: ToastController,
		public navCtrl: NavController,
		private dbSvc: DatabaseProvider) {
		this.navCtrl = navCtrl;
	}

	public ionViewDidEnter() {
		this.loadCustomers();
	}

	public loadCustomers() {
		Customer.getAll(this.dbSvc.database)
			.subscribe((c) => {
				this.customers = c;
			});
	}

	public customerLocations(id: number) {
		this.navCtrl.push('Customer_locationsComponent', { id });
	}
}
