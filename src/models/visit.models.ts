export interface IVisitModel {
    id: number;
    customerId: number;
    siteId: number;
    objective: string;
    startDate: string;
    startTime: string;
    endTime: string;
    additionalStaff: string;
    contactId: number;
    positionHeld: string;
    notes: string;
}

export interface IQuickAddVisitModel {
    customerId: number;
    siteId: number;
    objective: string;
    contactId: number;
}
