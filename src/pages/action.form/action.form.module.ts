import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActionFormPage } from './action.form';

@NgModule({
    declarations: [
        ActionFormPage
    ],
    exports: [
        ActionFormPage
    ],
    imports: [
        IonicPageModule.forChild(ActionFormPage)
    ]
})
export class ActionFormPageModule { }
