import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { IonicPage, LoadingController, Platform } from 'ionic-angular';
import { Report } from '../../models/report';
import { ToastService } from '../../providers/util/toast.service';
import { ApiGateway } from '../../services/api.gateway.service';

@IonicPage()
@Component({
  selector: 'page-reports',
  templateUrl: 'reports.html'
})
export class ReportsPage {

  private colorScheme = {
    domain: ['#373331', '#605a56', '#80ac7b', '#e8eaa1', '#596e79', '#c7b198']
  };

  private view = [];

  private _report: Report;

  constructor(public loadingCtrl: LoadingController, public toastSvc: ToastService,
    private api: ApiGateway,
    private platform: Platform) {
    this._report = new Report();
    this.view = [platform.width(), platform.width() - 50];
  }

  public doRefresh(refresher) {
    this.fetch(refresher);
  }

  private fetch(refresher) {
    const loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    if (!refresher) {
      loading.present();
    }
    Report.fetchReport(this.api)
      .subscribe((report) => {
        this._report = report;
      }, (err) => {
        let msg = err.message;
        loading.dismiss();
        if (refresher) { refresher.complete(); }
        if (err instanceof HttpErrorResponse) {
          msg = 'Bad internet connection';
        }
        this.toastSvc.create(msg);
      }, () => {
        loading.dismiss();
        if (refresher) { refresher.complete(); }
      });
  }

  private onSelect(event) {
    // tslint:disable-next-line:no-console
    console.log(event);
  }
}
