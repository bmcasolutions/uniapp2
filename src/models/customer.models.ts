export interface ICustomerAddModel {
    name: string;
    typeId: number;
    keyRankId: number;
    address1: string;
    address2: string;
    address3: string;
    address4: string;
    postCode: string;
    contactName: string;
    positionHeld: string;
    contactPhone: string;
    contactEmail: string;
}

export interface ICustomerEditModel {
    id: number;
    name: string;
    typeId: number;
    keyRankId: number;
    siteId: string;
    contactId: string;
    notes: string;
}
