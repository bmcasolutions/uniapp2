CREATE TABLE IF NOT EXISTS users(
    id INTEGER PRIMARY KEY,
    accountId NUMBER,
    displayName TEXT,
    userName TEXT,
    password TEXT,
    email TEXT,
    lastSync TEXT,
    loggedIn NUMBER);

CREATE TABLE IF NOT EXISTS customers(
    id INTEGER PRIMARY KEY,
    refNo TEXT,
    name TEXT,
    address1 TEXT,
    address2 TEXT,
    address3 TEXT,
    address4 TEXT,
    postCode TEXT,
    telephone TEXT,
    type TEXT,
    notes TEXT,
    contactName TEXT,
    contactPhone TEXT,
    contactEmail TEXT,
    countryId INTEGER,
    url TEXT,
    active boolean,
    nextContactOn TEXT,
    lastContactOn TEXT,
    billingEmail1 TEXT,
    billingEmail2 TEXT,
    financeCode TEXT,
    vatNo TEXT,
    country TEXT,
    creditLimit INTEGER,
    typeId INTEGER,
    keyRankId INTEGER,
    isOther INTEGER,
    accountId INTEGER,
    depotId INTEGER,
    contactId INTEGER,
    siteId INTEGER);

CREATE TABLE IF NOT EXISTS customer_contacts(
    id INTEGER PRIMARY KEY,
    customerId INTEGER,
    siteId INTEGER,
    name TEXT,
    phone TEXT,
    email1 TEXT,
    email2 TEXT,
    isDefault INTEGER,
    notes TEXT,
    positionHeld TEXT);

CREATE TABLE IF NOT EXISTS customer_locations(
    id INTEGER PRIMARY KEY,
    customerId INTEGER,
    name TEXT,
    address1 TEXT,
    address2 TEXT,
    address3 TEXT,
    address4 TEXT,
    postCode TEXT,
    latitude INTEGER,
    longitude INTEGER,
    isDefault INTEGER,
    notes TEXT);

CREATE TABLE IF NOT EXISTS customer_trade_links(
    id INTEGER PRIMARY KEY,
    customerId INTEGER,
    name TEXT);

CREATE TABLE IF NOT EXISTS customer_account(
    id INTEGER PRIMARY KEY,
    name TEXT);

CREATE TABLE IF NOT EXISTS customer_visits(
    id INTEGER PRIMARY KEY,
    customerId INTEGER,
    siteId INTEGER,
    locationName TEXT,
    customerName TEXT,
    meetingOwner TEXT,
    objective TEXT,
    objectiveMet NUMBER,
    isClosed NUMBER,
    uId INTEGER,
    contactName TEXT,
    positionHeld TEXT,
    additionalStaff INTEGER,
    monthStart INTEGER,
    startDate TEXT,
    endDate TEXT,
    startHour INTEGER,
    startQuarter INTEGER,
    endHour INTEGER,
    endQuarter INTEGER,
    comments TEXT,
    notes TEXT,
    outcome TEXT,
    isCancelled NUMBER,
    cancelledOn TEXT,
    cancelledBy TEXT,
    cancelleReason TEXT,
    isOther NUMBER,
    checkinDateTime INTEGER,
    checkInGeoLat INTEGER,
    checkInGeoLng INTEGER,
    checkInComplete INTEGER,
    complete INTEGER,
    outofSync INTEGER);


CREATE TABLE IF NOT EXISTS actions(
    id INTEGER PRIMARY KEY,
    customerId INTEGER,
    visitId INTEGER,
    name TEXT,
    detail TEXT,
    dueDate TEXT,
    assignedTo INTEGER,
    assignedToName TEXT,
    complete boolean,
    completedOn TEXT,
    completedBy INTEGER,
    comments TEXT,
    status INTEGER,
    priority INTEGER,
    outofSync INTEGER);

CREATE TABLE IF NOT EXISTS customer_product(
    id INTEGER PRIMARY KEY,
    customerId INTEGER,
    productId INTEGER,
    supplierId INTEGER,
    productName TEXT,
    isStockist boolean,
    supplierName TEXT
   );

CREATE TABLE IF NOT EXISTS suppliers(
    id INTEGER PRIMARY KEY,
    name TEXT
    );

CREATE TABLE IF NOT EXISTS objectives(
    id INTEGER PRIMARY KEY,
    name TEXT
    );

CREATE TABLE IF NOT EXISTS supplier_products(
    id INTEGER PRIMARY KEY,
    supplierId INTEGER,
    productId INTEGER
    );

/*CREATE TABLE IF NOT EXISTS certificates(
    id INTEGER,
    type TEXT,
    refId INTEGER,
    name TEXT,
    expiresOn TEXT,
    expires INTEGER,
    notifyDays INTEGER,
    status TEXT,
    PRIMARY KEY(id, type));



CREATE TABLE IF NOT EXISTS employees(
    id INTEGER PRIMARY KEY,
    userId INTEGER,
    firstName TEXT,
    lastName TEXT);

CREATE TABLE IF NOT EXISTS vehicles(
    id INTEGER PRIMARY KEY,
    vehicleMake INTEGER,
    vehicleModel TEXT,
    registrationNo TEXT);

CREATE TABLE IF NOT EXISTS visits(
    id INTEGER PRIMARY KEY,
    leadId INTEGER,
    leadRef TEXT,
    leadName TEXT,
    companyName INTEGER,
    ownerName TEXT,
    contactName TEXT,
    contactPhone TEXT,
    address1 TEXT,
    address2 TEXT,
    address3 TEXT,
    address4 TEXT,
    postCode TEXT,
    visitDate TEXT,
    notes TEXT,
    visitDetails TEXT,
    status TEXT,
    sent TEXT);

CREATE TABLE IF NOT EXISTS visit_photos(
    id INTEGER PRIMARY KEY,
    visitId INTEGER,
    description TEXT,
    imageData TEXT,
    sent INTEGER);

CREATE TABLE IF NOT EXISTS jobs(
    id INTEGER PRIMARY KEY,
    jobRef TEXT,
    jobName TEXT,
    customerName TEXT,
    locationName TEXT,
    address1 TEXT,
    address2 TEXT,
    address3 TEXT,
    address4 TEXT,
    postCode TEXT,
    dueDate INTEGER,
    jobDetails TEXT,
    csmName TEXT,
    csmPhone TEXT,
    startDateTime INTEGER,
    startGeoLat INTEGER,
    startGeoLng INTEGER,
    vehicleId INTEGER,
    vehicleCheck INTEGER,
    partsCheck INTEGER,
    equipmentCheck INTEGER,
    checkinDateTime INTEGER,
    checkInGeoLat INTEGER,
    checkInGeoLng INTEGER,
    engineeringNotes TEXT,
    completionDateTime INTEGER,
    completionGeoLat INTEGER,
    completionGeoLng INTEGER,
    isComplete INTEGER,
    rescheduleDateTime INTEGER,
    isIssue INTEGER,
    issue TEXT,
    isSatisfied INTEGER,
    comments TEXT,
    signatureName TEXT,
    signatureData TEXT,
    status TEXT,
    sent TEXT);

CREATE TABLE IF NOT EXISTS job_risks(
    id INTEGER PRIMARY KEY,
    jobId INTEGER,
    riskId INTEGER,
    isRisk Number,
    sent INTEGER);

CREATE TABLE IF NOT EXISTS job_photos(
    id INTEGER PRIMARY KEY,
    jobId INTEGER,
    type TEXT,
    description TEXT,
    imageData TEXT,
    sent INTEGER);

CREATE TABLE IF NOT EXISTS tasks(
    id INTEGER PRIMARY KEY,
    taskName TEXT,
    ownerId INTEGER,
    taskDueOn INTEGER,
    taskDetails TEXT,
    status TEXT,
    sent INTEGER);

CREATE TABLE IF NOT EXISTS timesheet_jobs(
    id INTEGER PRIMARY KEY,
    jobName TEXT,
    siteName TEXT,
    salesId INTEGER);

CREATE TABLE IF NOT EXISTS timesheets(
    id INTEGER PRIMARY KEY,
    jobId INTEGER,
    startDateTime INTEGER,
    endDateTime INTEGER,
    signatureData TEXT,
    sent TEXT);

CREATE TABLE IF NOT EXISTS courses(
    id INTEGER PRIMARY KEY,
    attended INTEGER,
    signatureCheck INTEGER,
    signatureName TEXT,
    signatureData TEXT,
    courseName TEXT,
    courseDate INTEGER,
    courseBy TEXT,
    locationName TEXT,
    startHour INTEGER,
    startQuarter INTEGER,
    content TEXT,
    status TEXT,
    sent TEXT);

CREATE TABLE IF NOT EXISTS checks(
    id INTEGER PRIMARY KEY,
    type TEXT,
    objectId INTEGER,
    checkDate INTEGER,
    sent TEXT);

CREATE TABLE IF NOT EXISTS check_parameter_data(
    id INTEGER PRIMARY KEY,
    checkId INTEGER,
    checkParameterId INTEGER,
    checked INTEGER);
*/