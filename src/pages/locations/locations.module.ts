import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { LocationsComponent } from './locations';

@NgModule({
	declarations: [
		LocationsComponent
	],
	imports: [
		IonicPageModule.forChild(LocationsComponent),
		TranslateModule
	],
	exports: [
		LocationsComponent,
		TranslateModule
	]
})
export class LocationsModule {}
