import { NgModule } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { IonicPageModule } from 'ionic-angular';
import { ReportsPage } from './reports';

@NgModule({
    declarations: [
        ReportsPage
    ],
    exports: [
        ReportsPage
    ],
    imports: [
        NgxChartsModule,
        IonicPageModule.forChild(ReportsPage)
    ]
})
export class ReportsPageModule { }
