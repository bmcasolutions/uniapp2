import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { StockistComponent } from './stockist';

@NgModule({
  declarations: [
    StockistComponent
  ],
  imports: [
    IonicPageModule.forChild(StockistComponent),
    TranslateModule
  ],
  exports: [
    StockistComponent,
    TranslateModule
  ]
})
export class StockistModule { }
