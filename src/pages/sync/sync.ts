import { Component } from '@angular/core';
import { IonicPage, LoadingController, NavController } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { Action } from '../../models/action';
import { Customer } from '../../models/customer';
import { CustomerContact } from '../../models/customerContact';
import { CustomerProduct } from '../../models/customerProduct';
import { Supplier } from '../../models/supplier';

import { HttpErrorResponse } from '@angular/common/http';
import { CustomerAccount } from '../../models/customerAccount';
import { CustomerLocation } from '../../models/customerLocation';
import { CustomerTradeLink } from '../../models/customerTradeLink';
import { CustomerVisit } from '../../models/customerVisit';
import { Objective } from '../../models/objective';
import { SupplierProduct } from '../../models/supplierProduct';
import { User } from '../../models/user';
import { DatabaseProvider } from '../../providers/database';
import { ToastService } from '../../providers/util/toast.service';
import { AccountService } from '../../services/account.service';
import { ApiGateway } from '../../services/api.gateway.service';

@IonicPage()
@Component({
  selector: 'page-sync',
  templateUrl: 'sync.html'
})
export class SyncPage {

  private _upMessages: any[] = [{
    count: 0,
    icon: 'cloud-upload',
    name: 'Visit Data',
    tag: 'visit_up'
  }, {
    count: 0,
    icon: 'cloud-upload',
    name: 'Job Data',
    tag: 'job_up'
  }, {
    count: 0,
    icon: 'cloud-upload',
    name: 'Timesheet Data',
    tag: 'timesheet_up'
  }, {
    count: 0,
    icon: 'cloud-upload',
    name: 'Vehicle Check Data',
    tag: 'vehicle_check_up'
  }, {
    count: 0,
    icon: 'cloud-upload',
    name: 'Equipment Check Data',
    tag: 'equipment_check_up'
  }, {
    count: 0,
    icon: 'cloud-upload',
    name: 'Training Data',
    tag: 'training_up'
  }];

  private _downMessages: any[] = [{
    count: 0,
    icon: 'cloud-download',
    name: 'Customers',
    tag: 'customer'
  }, {
    count: 0,
    icon: 'cloud-download',
    name: 'Customer Contacts',
    tag: 'customer_contact'
  }, {
    count: 0,
    icon: 'cloud-download',
    name: 'Customer Locations',
    tag: 'customer_location'
  }, {
    count: 0,
    icon: 'cloud-download',
    name: 'Customer Trade Links',
    tag: 'customer_trade_link'
  }, {
    count: 0,
    icon: 'cloud-download',
    name: 'Customer Visit',
    tag: 'customer_visit'
  }, {
    count: 0,
    icon: 'cloud-download',
    name: 'Customer Visit Actions',
    tag: 'action'
  }, {
    count: 0,
    icon: 'cloud-download',
    name: 'Customer Product',
    tag: 'customer_product'
  }, {
    count: 0,
    icon: 'cloud-download',
    name: 'Customer Suppliers',
    tag: 'suppliers'
  }, {
    count: 0,
    icon: 'cloud-download',
    name: 'Objectives',
    tag: 'objective'
  }, {
    count: 0,
    icon: 'cloud-download',
    name: 'Supplier Products',
    tag: 'supplier_products'
  }, {
    count: 0,
    icon: 'cloud-download',
    name: 'Supplier Account',
    tag: 'customer_account'
  }];

  private _userInfo: User;

  constructor(public loadingCtrl: LoadingController, public toastSvc: ToastService,
    private api: ApiGateway, private db: DatabaseProvider,
    private nav: NavController, private accSvc: AccountService) { }

  public ionViewDidEnter() {
    this.sync(null);
  }

  public doRefresh(refresher) {
    this.sync(refresher);
  }

  public sync(refresher) {
    this._upMessages.forEach((el) => {
      el.count = 0;
    });
    this._downMessages.forEach((el) => {
      el.count = 0;
    });

    this.accSvc.getUserInfo()
      .do((uInfo) => this._userInfo = uInfo)
      .subscribe(() => this.syncData(refresher));
  }

  private syncData(refresher) {
    const loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    if (!refresher) {
      loading.present();
    }
    this.uploadData()
      .concat(this.deleteAll())
      .concat(this.downloadData())
      .subscribe((message) => {
        let tag = this._upMessages.filter((m) => m.tag === message[0]);
        if (tag[0]) {
          tag[0].count += message[1];
        }
        tag = this._downMessages.filter((m) => m.tag === message[0]);
        if (tag[0]) {
          tag[0].count += message[1];
        }
      }, (err) => {
        const msg = err.message;
        loading.dismiss();
        if (refresher) { refresher.complete(); }
        this.toastSvc.create(msg);
      }, () => {
        loading.dismiss();
        if (refresher) { refresher.complete(); }
        this.toastSvc.create('Sync complete!');
      });
  }

  private uploadData(): Observable<[string, number]> {
    return Observable.from(CustomerVisit.syncBackOutOfSync(this.api, this.db.database));
  }

  private deleteAll(): Observable<any> {
    return Observable.forkJoin(
      Action.deleteAll(this.db.database),
      Customer.deleteAll(this.db.database),
      CustomerAccount.deleteAll(this.db.database),
      CustomerContact.deleteAll(this.db.database),
      CustomerLocation.deleteAll(this.db.database),
      CustomerProduct.deleteAll(this.db.database),
      CustomerTradeLink.deleteAll(this.db.database),
      CustomerVisit.deleteAll(this.db.database),
      Objective.deleteAll(this.db.database),
      Supplier.deleteAll(this.db.database),
      SupplierProduct.deleteAll(this.db.database)
    );
  }

  private downloadData(): Observable<[string, number]> {
    return Observable.concat(
      Customer.syncAllData(this.api, this.db.database, this.accSvc),
      CustomerContact.syncAllData(this.api, this.db.database),
      CustomerLocation.syncAllData(this.api, this.db.database),
      CustomerTradeLink.syncAllData(this.api, this.db.database),
      CustomerVisit.syncAllData(this.api, this.db.database),
      CustomerProduct.syncAllData(this.api, this.db.database),
      Supplier.syncData(this.api, this.db.database),
      Action.syncAllData(this.api, this.db.database),
      Objective.syncData(this.api, this.db.database),
      SupplierProduct.syncData(this.api, this.db.database),
      CustomerAccount.syncData(this.api, this.db.database)
    );
  }
}
