import { Component } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Action } from '../../models/action';
// tslint:disable-next-line:ordered-imports
import { Customer } from '../../models/customer';
import { CustomerVisit } from '../../models/customerVisit';
import { User } from '../../models/user';
import { DatabaseProvider } from '../../providers/database';
import { AccountService } from '../../services/account.service';

@IonicPage()
@Component({
	selector: 'actions',
	templateUrl: 'actions.html'
})
export class ActionsComponent {
	private _db: SQLiteObject;
	private actions: any[];
	private terms: string;
	private currentUser: User;
	private actionSegment: string = 'my';
	public customerId: number;

	constructor(public toastCtrl: ToastController,
		public navCtrl: NavController, private param: NavParams,
		db: DatabaseProvider,
		private accSvc: AccountService) {
		this.navCtrl = navCtrl;
		this._db = db.database;

	}

	public ionViewDidEnter() {
		this.customerId = this.param.get('cid') || 0;
		// if(this.customerId>0)
		// {
		// 	this.accSvc.getUserInfo()
		// 	.subscribe((u) => {
		// 		this.currentUser = u;
		// 		Action.getAll(this._db)
		// 			.flatMap((ac: any[]) => Observable.from(ac))
		// 			.flatMap((a: any) => new CustomerVisit().getByCustomerId(this._db, this.customerId)
		// 				, (a, v) => {
		// 					a.text = a.name;
		// 					a.visit = v;
		// 					return a;
		// 				})
		// 			.toArray()
		// 			.subscribe((ac: any[]) => {
		// 				this.actions = ac;
		// 			});
		// 	});
		// }
		// else
		// {
		this.accSvc.getUserInfo()
			.subscribe((u) => {
				this.currentUser = u;
				Action.getAll(this._db)
					.flatMap((ac: any[]) => Observable.from(ac))
					.pipe(filter((ac: any) => {
						if (this.customerId) {
							return ac.customerId === this.customerId;
						}
						return true;
					}))
					.flatMap((a: any) => new Customer().getById(this._db, a.customerId)
						, (a, c) => {
							a.text = a.name;
							a.color = !!a.complete ? '#4CAF50' : '#3286d0';
							a.customer = c;
							return a;
						}).toArray()
					.subscribe((ac: any[]) => {
						this.actions = ac.sort((a: Action, b: Action) => {
							if (a.complete === b.complete) {
								return 0;
							}
							return +a.complete - +b.complete;
						});
					});
			});
		// }
	}

	private myActions(ac: Action) {
		return ac.assignedTo === this.currentUser.id;
	}

	private otherActions(ac: Action) {
		return ac.assignedTo !== this.currentUser.id;
	}

	public openPage(component) {
		this.navCtrl.push(component);
	}

	public actionDetail(action) {
		this.navCtrl.push('Action_detailsComponent', { id: action.id });
	}
}
