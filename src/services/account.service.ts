import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs/Subject';
import { User } from '../models/user';
import { DatabaseProvider } from '../providers/database';
import { ApiGateway } from './api.gateway.service';

@Injectable()
export class AccountService {
  private _dbProvider: DatabaseProvider;
  private _cachedUser: any;

  private _authNotifier: Subject<boolean>;

  constructor(public api: ApiGateway, db: DatabaseProvider) {
    this._authNotifier = new Subject();
    this._dbProvider = db;
  }

  public login(model): Observable<boolean> {
    return this.api.post(
      '/login.asp', null, {
        pw: model.password,
        un: model.userName
      })
      .pipe(map((res: any) => {
        const user = new User();
        return Object.assign(user, {
          accountId: res.user_info[0].AccountID,
          displayName: res.user_info[0].DisplayName,
          email: res.user_info[0].Email,
          id: res.user_info[0].UserID,
          password: model.password,
          lastSync: res.user_info[0].LastSync,
          userName: res.user_info[0].UserName,
          loggedIn: true
        } as User);
      }))
      .flatMap((user) => {
        return User.deleteLoggedInUser(this._dbProvider.database)
          .flatMap(() => this.setUserInfo(user));
      });
  }

  public lastSyncTime(user: User): Observable<string> {
    return this.api.post(
      '/login.asp', null, {
        pw: user.password,
        un: user.userName
      })
      .flatMap((res: any) => {
        return Observable.of(res.user_info[0].LastSync);
      });
  }

  public unloadCachedUser(): void {
    this._cachedUser = null;
  }

  public getUserInfo(): Observable<User> {
    if (this._cachedUser) {
      return Observable.of(this._cachedUser);
    } else {
      return User.getLoggedInUser(this._dbProvider.database)
        .do((user) => this._cachedUser = user);
    }
  }

  public setUserInfo(user: User): Observable<boolean> {
    if (user) {
      return user.insert(this._dbProvider.database)
        .flatMap(() => {
          this._cachedUser = user;
          this._authNotifier.next(!!user);
          return Observable.of(true);
        });
    } else {
      User.logOutUser(this._dbProvider.database)
        .subscribe(() => {
          this._cachedUser = user;
          this._authNotifier.next(!!user);
          return Observable.of(true);
        });
    }
  }

  public isloggedIn(): Observable<boolean> {
    return this.getUserInfo()
      .map((info) => {
        return !!info;
      });
  }

  public authenticationNotifier(): Subject<boolean> {
    return this._authNotifier;
  }
}
