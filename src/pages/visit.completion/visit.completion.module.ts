import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VisitCompletionPage } from './visit.completion';

@NgModule({
    declarations: [
        VisitCompletionPage
    ],
    exports: [
        VisitCompletionPage
    ],
    imports: [
        IonicPageModule.forChild(VisitCompletionPage)
    ]
})
export class VisitCompletionPageModule { }
