import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { SharedModule } from '../../app/shared.module';
import { VisitsComponent } from './visits';

@NgModule({
  declarations: [
    VisitsComponent
  ],
  imports: [
    IonicPageModule.forChild(VisitsComponent),
    TranslateModule,
    SharedModule
  ],
  exports: [
    VisitsComponent,
    TranslateModule
  ]
})
export class VisitsModule { }
