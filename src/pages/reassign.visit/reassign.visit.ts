import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { IonicPage, LoadingController, NavController, ToastController, ViewController } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { CustomerVisit } from '../../models/customerVisit';
import { ToastService } from '../../providers/util/toast.service';
import { ApiGateway } from '../../services/api.gateway.service';

@IonicPage()
@Component({
	selector: 'reassign.visit',
	templateUrl: 'reassign.visit.html'
})

export class ReassignVisitModal {
	private assignedToId: number;
	private visitId: number;

	private users = [{ UID: 1, DisplayName: 'Brendan McAnerney' },
	{ UID: 2, DisplayName: 'Graham Milligan' }, { UID: 3, DisplayName: 'Ian Dalgety' },
	{ UID: 25, DisplayName: 'Anthony Massingham' }, { UID: 26, DisplayName: 'Carl Darnell-Green' },
	{ UID: 27, DisplayName: 'Ivan Major' }, { UID: 28, DisplayName: 'Jo-Ann James' },
	{ UID: 29, DisplayName: 'James Gunning' }, { UID: 30, DisplayName: 'Jim Loxton' },
	{ UID: 31, DisplayName: 'Michael Kendra' }, { UID: 32, DisplayName: 'Neil Bullock' },
	{ UID: 33, DisplayName: 'Pat Dawson' }, { UID: 34, DisplayName: 'Phil Wakelam' },
	{ UID: 35, DisplayName: 'Steve Alcock' }, { UID: 36, DisplayName: 'Steve Whyte' },
	{ UID: 38, DisplayName: 'Tony Da-Silva' }, { UID: 40, DisplayName: 'Scott MacLeod' },
	{ UID: 42, DisplayName: 'Holly Hood' }, { UID: 46, DisplayName: 'Andrew Hodge' },
	{ UID: 47, DisplayName: 'Martin Gregg' }];

	constructor(public toastCtrl: ToastController, public navCtrl: NavController,
		private param: NavParams, private api: ApiGateway,
		private loadingCtrl: LoadingController,
		private toastSvc: ToastService,
		private viewCtrl: ViewController) {
		this.navCtrl = navCtrl;
		this.visitId = this.param.get('vid');
	}

	public reassign() {
		const loading = this.loadingCtrl.create({
			content: 'Please wait...'
		});
		new CustomerVisit().reassign(this.visitId, this.assignedToId, this.api)
			.subscribe(() => {
				this.dismiss();
			}, (err) => {
				let msg = err.message;
				loading.dismiss();
				if (err instanceof HttpErrorResponse) {
					msg = 'Bad internet connection';
				}
				this.toastSvc.create(msg);
			}, () => {
				loading.dismiss();
				this.toastSvc.create('Visit Reassigned successfully!');
			});
	}

	public dismiss() {
		this.viewCtrl.dismiss();
	}
}
