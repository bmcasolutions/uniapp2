import { Component } from '@angular/core';
import {
	AlertController, IonicPage, LoadingController, MenuController,
	NavController, ToastController
} from 'ionic-angular';
import { User } from '../../models/user';
import { ToastService } from '../../providers/util/toast.service';
import { AccountService } from '../../services/account.service';

@IonicPage()
@Component({
	selector: 'home',
	templateUrl: 'home.html'
})
export class HomeComponent {
	private outOfSync: boolean;
	private loading: any;

	constructor(public navCtrl: NavController,
		public toastCtrl: ToastController, private loadingCtrl: LoadingController,
		public menu: MenuController, private accountSvc: AccountService,
		private alertCtrl: AlertController,
		private toastSvc: ToastService) {
		this.menu.enable(true);
		this.navCtrl = navCtrl;
		this.outOfSync = false;
	}

	public ionViewDidEnter() {
		this.loading = this.loadingCtrl.create({
			content: 'Please wait..',
			spinner: 'crescent'
		});
		let user: User = null;
		this.accountSvc.getUserInfo()
			.flatMap((u: User) => {
				user = u;
				this.loading.present();
				return this.accountSvc.lastSyncTime(user);
			}).subscribe((time) => {
				// this.outOfSync = true;
				// console.log(time.replace(/(\d+)\/(\d+)\/(\d+)/, '$2/$1/$3'));
				// console.log(user.lastSync.replace(/(\d+)\/(\d+)\/(\d+)/, '$2/$1/$3'));
				this.outOfSync = new Date(time.replace(/(\d+)\/(\d+)\/(\d+)/, '$2/$1/$3'))
					> new Date(user.lastSync.replace(/(\d+)\/(\d+)\/(\d+)/, '$2/$1/$3'));
			}, (error) => {
				this.loading.dismiss();
				this.toastSvc.create(error.message);
			}, () => {
				this.loading.dismiss();
			});
	}

	public forceSync() {
		const confirm = this.alertCtrl.create({
			buttons: [
				{
					handler: () => {
						// console.log('Disagree clicked');
					},
					text: 'No'
				},
				{
					handler: () => {
						this.openPage('SyncPage');
					},
					text: 'Yes'
				}
			],
			message: 'There are data in the server to be synced. Do you want to continue?',
			title: 'Sync'
		});
		confirm.present();
	}

	public openPage(component) {
		this.navCtrl.push(component);
	}
}
