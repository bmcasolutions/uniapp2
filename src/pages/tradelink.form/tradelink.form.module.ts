import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TradeLinkFormPage } from './tradelink.form';

@NgModule({
    declarations: [
        TradeLinkFormPage
    ],
    exports: [
        TradeLinkFormPage
    ],
    imports: [
        IonicPageModule.forChild(TradeLinkFormPage)
    ]
})
export class CustomerFormPageModule { }
