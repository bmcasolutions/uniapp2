import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { SETTING } from '../app/config';

// Import the rxjs operators we need (in a production app you'll
//  probably want to import only the operators you actually use)
//
import 'rxjs';

export class ApiGatewayOptions {
    public method: string;
    public url: string;
    public headers = {};
    public params = {};
    public data = {};
}

// tslint:disable-next-line:max-classes-per-file
@Injectable()
export class ApiGateway {

    // Define the internal Subject we'll use to push errors
    private errorsSubject = new Subject<any>();

    // Provide the *public* Observable that clients can subscribe to
    public errors$: Observable<any>;

    // Define the internal Subject we'll use to push the command count
    private pendingCommandsSubject = new Subject<number>();
    private pendingCommandCount = 0;

    // Provide the *public* Observable that clients can subscribe to
    public pendingCommands$: Observable<number>;

    constructor(
        private _http: HttpClient
    ) {

        // Create our observables from the subjects
        this.errors$ = this.errorsSubject.asObservable();
        this.pendingCommands$ = this.pendingCommandsSubject.asObservable();
    }

    // I perform a GET request to the API, appending the given params
    // as URL search parameters. Returns a stream.
    public get(url: string, params: any): Observable<Response> {
        url = `:endpoint${url}`;
        params = (params || {});
        params.endpoint = SETTING.API_ENDPOINT;

        const options = new ApiGatewayOptions();
        options.method = 'GET';
        options.url = url;
        options.params = params;
        return this.request(options);
    }

    // I perform a POST request to the API. If both the params and data
    // are present, the params will be appended as URL search parameters
    // and the data will be serialized as a JSON payload. If only the
    // data is present, it will be serialized as a JSON payload. Returns
    // a stream.
    public post(url: string, params: any, data: any): Observable<Response> {
        if (!url.startsWith('http')) {
            url = `:endpoint${url}`;
        }
        params = (params || {});
        params.endpoint = SETTING.API_ENDPOINT;
        if (!data) {
            data = params;
            params = {};
        }
        const options = new ApiGatewayOptions();
        options.method = 'POST';
        options.url = url;
        options.params = params;
        options.data = data;

        return this.request(options);
    }

    private request(options: ApiGatewayOptions): Observable<any> {

        options.method = (options.method || 'GET');
        options.url = (options.url || '');
        options.headers = (options.headers || {});
        options.params = (options.params || {});
        options.data = (options.data || {});

        this.interpolateUrl(options);
        this.addXsrfToken(options);
        this.addContentType(options);

        const requestOptions: any = {};
        requestOptions.method = options.method;
        requestOptions.url = options.url;
        requestOptions.headers = options.headers;
        requestOptions.search = this.buildUrlSearchParams(options.params);
        requestOptions.body = JSON.stringify(options.data);

        const isCommand = (options.method !== 'GET');

        if (isCommand) {
            this.pendingCommandsSubject.next(++this.pendingCommandCount);
        }

        const stream = this._http.request(options.method, options.url, requestOptions)
            .catch((error: any) => {
                this.errorsSubject.next(error);
                return Observable.throw(error);
            })
            .map(this.unwrapHttpValue)
            .catch((error: any) => {
                return Observable.throw(this.unwrapHttpError(error));
            })
            .finally(() => {
                if (isCommand) {
                    this.pendingCommandsSubject.next(--this.pendingCommandCount);
                }
            });

        return stream;
    }

    private addContentType(options: ApiGatewayOptions): ApiGatewayOptions {
        if (options.method !== 'GET') {
            options.headers['Content-Type'] = 'application/json; charset=UTF-8';
        }
        return options;
    }

    private extractValue(collection: any, key: string): any {
        const value = collection[key];
        delete (collection[key]);
        return value;
    }

    private addXsrfToken(options: ApiGatewayOptions): ApiGatewayOptions {
        const xsrfToken = this.getXsrfCookie();
        if (xsrfToken) {
            options.headers['X-XSRF-TOKEN'] = xsrfToken;
        }
        return options;
    }

    private getXsrfCookie(): string {
        const matches = document.cookie.match(/\bXSRF-TOKEN=([^\s;]+)/);
        try {
            return (matches && decodeURIComponent(matches[1]));
        } catch (decodeError) {
            return ('');
        }
    }

    private buildUrlSearchParams(params: any): URLSearchParams {
        const searchParams = new URLSearchParams();
        for (const key in params) {
            if (params.hasOwnProperty(key)) {
                searchParams.append(key, params[key]);
            }
        }
        return searchParams;
    }

    private interpolateUrl(options: ApiGatewayOptions): ApiGatewayOptions {
        options.url = options.url.replace(
            /:([a-zA-Z]+[\w-]*)/g,
            (_$0, token) => {
                // Try to move matching token from the params collection.
                if (options.params.hasOwnProperty(token)) {
                    return (this.extractValue(options.params, token));
                }
                // Try to move matching token from the data collection.
                if (options.data.hasOwnProperty(token)) {
                    return (this.extractValue(options.data, token));
                }
                // If a matching value couldn't be found, just replace
                // the token with the empty string.
                return ('');
            }
        );
        // Clean up any repeating slashes.
        // options.url = options.url.replace(/\/{2,}/g, "/");
        // Clean up any trailing slashes.
        options.url = options.url.replace(/\/+$/g, '');

        return options;
    }

    private unwrapHttpError(error: any): any {
        try {
            if (error.status === 0) {
                return {
                    code: error.status,
                    message: 'Bad internet connection'
                };
            }
            if (error.error.msg) {
                return {
                    code: error.status,
                    message: error.error.msg
                };
            }
            return error;
        } catch (jsonError) {
            return ({
                code: -1,
                message: 'An unexpected error occurred.'
            });
        }
    }

    private unwrapHttpValue(value: Response): any {
        return value;
    }
}
