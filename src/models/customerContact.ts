import { SQLiteObject } from '@ionic-native/sqlite';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { ApiGateway } from '../services/api.gateway.service';
import { BaseModel } from './baseModel';
import { Customer } from './customer';

export class CustomerContact extends BaseModel {
    public id: number;
    public customerId: number;
    public siteId: number;
    public name: string;
    public phone: string;
    public email1: string;
    public email2: string;
    public isDefault: boolean;
    public notes: string;
    public positionHeld: string;

    public insert(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`insert into customer_contacts(id,customerId,siteId,name,phone,
            email1,email2,isDefault,notes, positionHeld) values(?,?,?,?,?,?,?,?,?,?)`, [this.id, this.customerId,
            this.siteId, this.name, this.phone, this.email1, this.email2, +this.isDefault, this.notes,
            this.positionHeld]))
            .map((results) => !!results.rowsAffected);
    }

    public update(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`update customer_contacts set customerId=?,siteId=?,name=?,phone=?,
         email1=?,email2=?,isDefault=?,notes=?,positionHeld=? where id=?`, [this.customerId, this.siteId,
            this.name, this.phone, this.email1, this.email2, +this.isDefault, this.notes, this.positionHeld, this.id]))
            .map((results) => !!results.rowsAffected);
    }

    public getById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from customer_contacts where id=?', [id]))
            .map((results) => Customer.convertToArray(results.rows)[0]);
    }

    public static getAll(db: SQLiteObject): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from customer_contacts', []))
            .map((results) => Customer.convertToArray(results.rows));
    }

    public deleteById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('delete from customer_contacts where id=?', [id]))
            .map((results) => !!results.rowsAffected);
    }

    public getByCustomerId(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from customer_contacts where customerId=?', [id]))
            .map((results) => Customer.convertToArray(results.rows));
    }

    public static deleteAll(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('delete from customer_contacts', []))
            .map((results) => !!results.rowsAffected);
    }

    public static syncAllData(api: ApiGateway, db: SQLiteObject): Observable<[string, number]> {
        return api.post('/synccustomercontactsall.asp', null, null)
            .pipe(map((res: any) => {
                if (!res || !res.CustomerContacts) {
                    return [];
                }
                return res.CustomerContacts
                    .map((datum) => {
                        const con = new CustomerContact();
                        return Object.assign(con,
                            {
                                id: datum.ContactID,
                                customerId: datum.CustomerID || 0,
                                siteId: datum.SiteID || 0,
                                name: datum.ContactName || '',
                                phone: datum.ContactPhone || '',
                                email1: datum.ContactEmail || '',
                                email2: datum.ContactEmail2 || '',
                                isDefault: !!datum.IsDefault,
                                notes: datum.Notes || '',
                                positionHeld: datum.PositionHeld || ''
                            } as CustomerContact);
                    });
            })).flatMap((custs: CustomerContact[]) => {
                return Observable.from(custs)
                    .flatMap((datum) => datum.getById(db, datum.id)
                        .flatMap((d) => {
                            if (d) {
                                return datum.update(db);
                            } else {
                                return datum.insert(db);
                            }
                        }).flatMap(() => Observable.of(['customer_contact', 1] as [string, number])));
            });
    }

    public static syncData(id: number, api: ApiGateway, db: SQLiteObject): Observable<[string, number]> {
        return api.post('/synccustomercontacts.asp', null, { cid: id })
            .pipe(map((res: any) => {
                if (!res || !res.CustomerContacts) {
                    return [];
                }
                return res.CustomerContacts
                    .map((datum) => {
                        const con = new CustomerContact();
                        return Object.assign(con,
                            {
                                id: datum.ContactID,
                                customerId: datum.CustomerID || 0,
                                siteId: datum.SiteID || 0,
                                name: datum.ContactName || '',
                                phone: datum.ContactPhone || '',
                                email1: datum.ContactEmail || '',
                                email2: datum.ContactEmail2 || '',
                                isDefault: !!datum.IsDefault,
                                notes: datum.Notes || '',
                                positionHeld: datum.PositionHeld || ''
                            } as CustomerContact);
                    });
            })).flatMap((custs: CustomerContact[]) => {
                return Observable.from(custs)
                    .flatMap((datum) => datum.getById(db, datum.id)
                        .flatMap((d) => {
                            if (d) {
                                return datum.update(db);
                            } else {
                                return datum.insert(db);
                            }
                        }).flatMap(() => Observable.of(['customer_contact', 1] as [string, number])));
            });
    }

    public addEditContact(model: any, api: ApiGateway, db: SQLiteObject): Observable<boolean> {
        const data: any = {
            ContactName: model.name,
            SiteID: model.siteId,
            PositionHeld: model.positionHeld,
            ContactPhone: model.phone,
            ContactEmail: model.email1,
            isDefault: +model.isDefault,
            cid: model.customerId
        };
        if (model.id) {
            data.ContactID = model.id;
        }
        data.CustomerID = model.customerId;
        return api.post(model.id ? '/editcontact.asp' : '/addcontact.asp',
            null, data)
            .flatMap((res: any) => {
                return CustomerContact.syncAllData(api, db).last().map(() => true);
            }).catch(() => Observable.of(false));
    }

    public static deleteByContactId(model: any, api: ApiGateway, db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('delete from customer_contacts where id=?', [model.id]))
            .flatMap((v) => {
                const data: any = {
                    ContactID: model.id,
                    cid: model.cid
                };
                if (model.id) {
                    data.cid = model.customerId;
                }
                return api.post('/deletecontact.asp',
                    null, data)
                    .flatMap((res: any) => {
                        return new CustomerContact().deleteById(db, model.id);
                    }).catch(() => Observable.of(false));
            });
    }
}
