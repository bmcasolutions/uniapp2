import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams, ToastController } from 'ionic-angular';
import { CustomerLocation } from '../../models/customerLocation';
import { DatabaseProvider } from '../../providers/database';

@IonicPage()
@Component({
	selector: 'customer_locations',
	templateUrl: 'customer_locations.html'
})
export class Customer_locationsComponent {

	public customerLocations: any[] = [];

	constructor(public toastCtrl: ToastController,
		public navCtrl: NavController,
		private dbSvc: DatabaseProvider,
		private param: NavParams, private modalCtrl: ModalController
	) {
		this.navCtrl = navCtrl;
	}

	public ionViewDidEnter() {
		this.loadCustomerLocations();
	}

	public loadCustomerLocations() {
		new CustomerLocation().getByCustomerId(this.dbSvc.database, this.param.get('id'))
			.subscribe((c) => {
				this.customerLocations = c;
			});
	}
	public addEditLocation($event, contact) {
		$event.stopPropagation();
		contact = contact || { id: 0 };
		const modal = this.modalCtrl.create('LocationFormPage', {
			id: contact.id,
			cid: this.param.get('id')

		});
		modal.onDidDismiss((dataChanged) => {
			if (dataChanged) { this.loadCustomerLocations(); }
		});
		modal.present();
	}

	public locationDetail(id: number) {
		this.navCtrl.push('LocationDetailPage', { id });
	}
}
