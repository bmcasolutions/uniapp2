import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

@Injectable()
export class AlertService {
    constructor(public alertCtrl: AlertController) { }

    public presentAlert(title: string, message: string) {
        const alert = this.alertCtrl.create(
            {
                buttons: [{ text: 'OK' }],
                subTitle: message,
                title
            });
        return alert.present();
    }

    public presentErrorAlert(message: string) {
        return this.presentAlert('An error has occurred.', message);
    }

    public presentAlertWithCallback(title: string, message: string): Promise<boolean> {
        return new Promise((resolve) => {
            const confirm = this.alertCtrl.create({
                buttons: [{
                    handler: () => {
                        confirm.dismiss().then(() => resolve(false));
                        return false;
                    },
                    role: 'cancel',
                    text: 'Cancel'
                }, {
                    handler: () => {
                        confirm.dismiss().then(() => resolve(true));
                        return false;
                    },
                    text: 'Yes'
                }],
                message,
                title
            });
            return confirm.present();
        });
    }
}
