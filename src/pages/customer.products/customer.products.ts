import { Component } from '@angular/core';
import {
	AlertController, IonicPage, LoadingController, ModalController,
	NavController, NavParams, ToastController
} from 'ionic-angular';
import { CustomerProduct } from '../../models/customerProduct';
import { DatabaseProvider } from '../../providers/database';
import { ApiGateway } from '../../services/api.gateway.service';

@IonicPage()
@Component({
	selector: 'customer-products',
	templateUrl: 'customer.products.html'
})
export class CustomerProductsPage {
	public products: any[] = [];
	public customerId: number;
	constructor(public toastCtrl: ToastController, public navCtrl: NavController, private dbSvc: DatabaseProvider,
		private param: NavParams, private modalCtrl: ModalController,
		private alertCtrl: AlertController,
		private loadingCtrl: LoadingController,
		private api: ApiGateway) {
		this.navCtrl = navCtrl;
	}

	public openPage(component) {
		this.navCtrl.push(component);
	}
	public ionViewDidEnter() {
		this.loadCustomerProducts();
	}
	public loadCustomerProducts() {
		this.customerId = this.param.get('cid');
		new CustomerProduct().getByCustomerId(this.dbSvc.database, this.param.get('cid'))
			.subscribe((c) => {
				this.products = c;
			});
	}

	public editProduct($event, contact) {
		$event.stopPropagation();
		contact = contact || { id: 0 };
		const modal = this.modalCtrl.create('ProductFormPage', {
			id: contact.id,
			cid: this.param.get('cid')
		});
		modal.onDidDismiss((dataChanged) => {
			if (dataChanged) { this.loadCustomerProducts(); }
		});
		modal.present();
	}
}
