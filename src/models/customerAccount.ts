import { SQLiteObject } from '@ionic-native/sqlite';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { ApiGateway } from '../services/api.gateway.service';
import { BaseModel } from './baseModel';

export class CustomerAccount extends BaseModel {
    public id: number;
    public name: string;

    public insert(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`insert into customer_account(id,name)
        values(?,?)`, [this.id, this.name]))
            .map((results) => !!results.rowsAffected);
    }

    public update(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`update customer_account set name=?
        where id=?`, [this.name, this.id]))
            .map((results) => !!results.rowsAffected);
    }

    public deleteById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('delete from customer_account where id=?', [id]))
            .map((results) => !!results.rowsAffected);
    }

    public getById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from customer_account where id=?', [id]))
            .map((results) => CustomerAccount.convertToArray(results.rows)[0]);
    }

    public static getAll(db: SQLiteObject): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from customer_account', []))
            .map((results) => CustomerAccount.convertToArray(results.rows));
    }

    public static deleteAll(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('delete from customer_account', []))
            .map((results) => !!results.rowsAffected);
    }

    public static syncData(api: ApiGateway, db: SQLiteObject): Observable<[string, number]> {
        return api.post('/syncaccounts.asp', null, null)
            .pipe(map((res: any) => {
                if (!res || !res.Accounts) {
                    return [];
                }
                return res.Accounts
                    .map((datum) => {
                        const con = new CustomerAccount();
                        return Object.assign(con,
                            {
                                id: datum.AccountID,
                                name: datum.AccountName
                            } as CustomerAccount);
                    });
            })).flatMap((acc: CustomerAccount[]) => Observable.from(acc)
                .flatMap((datum) => datum.getById(db, datum.id)
                    .flatMap((d) => {
                        if (d) {
                            return datum.update(db);
                        } else {
                            return datum.insert(db);
                        }
                    }).flatMap(() => Observable.of(['customer_account', 1] as [string, number]))));
    }
}
