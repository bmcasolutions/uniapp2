import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ActionCompletionPage } from './action.completion';

@NgModule({
    declarations: [
        ActionCompletionPage
    ],
    exports: [
        ActionCompletionPage
    ],
    imports: [
        IonicPageModule.forChild(ActionCompletionPage)
    ]
})
export class ActionCompletionPageModule { }
