import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { SETTING } from '../app/config';

@Injectable()
export class DatabaseProvider {
  public database: SQLiteObject;
  public databaseReady: BehaviorSubject<boolean>;

  constructor(private sqlitePorter: SQLitePorter, sqlite: SQLite, private storage: Storage,
    private http: HttpClient) {
    this.databaseReady = new BehaviorSubject(false);
    sqlite.create({
      location: 'default',
      name: '__unitrunk.db'
    }).then((db: SQLiteObject) => {
      this.database = db;
      this.storage.get('db-filled')
        .then((val: number) => {
          val = +val;
          if (val === SETTING.DB_VERSION) {
            this.databaseReady.next(true);
          } else {
            this.fillDatabase(val)
              .flatMap(() => Observable.fromPromise(this.storage.set('db-filled', SETTING.DB_VERSION))
                .flatMap(() => Observable.of(true))
              ).subscribe((success) => {
                this.databaseReady.next(success);
              }, (error) => {
                // tslint:disable-next-line:no-console
                console.log('Error while DB table creation ', error);
              });
          }
        })
        .catch(() => {
          // console.log(e)
        });
    });
  }

  public fillDatabase(curVersion): Observable<boolean> {
    return Observable.range(curVersion + 1, SETTING.DB_VERSION - curVersion)
      .concatMap((version) => this.fillDatabaseVersion(version))
      .last();
  }

  private fillDatabaseVersion(version: number): Observable<boolean> {
    return this.http.get(`assets/db_${version}.sql`, { responseType: 'text' })
      .flatMap((sql: string) => {
        return Observable.fromPromise(this.sqlitePorter.importSqlToDb(this.database, sql))
          .flatMap(() => Observable.of(true));
      });
  }

  public getDatabaseState() {
    return this.databaseReady.asObservable();
  }
}
