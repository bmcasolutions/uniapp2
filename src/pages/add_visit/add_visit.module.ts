import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { Add_visitComponent } from './add_visit';

@NgModule({
  declarations: [
    Add_visitComponent
  ],
  imports: [
    IonicPageModule.forChild(Add_visitComponent),
    TranslateModule
  ],
  exports: [
    Add_visitComponent,
    TranslateModule
  ]
})
export class Add_visitModule { }
