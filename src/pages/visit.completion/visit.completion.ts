import { Component } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import {
    AlertController, Events, IonicPage, LoadingController, NavController,
    NavParams, Platform, ViewController
} from 'ionic-angular';
import { CustomerVisit } from '../../models/customerVisit';
import { DatabaseProvider } from '../../providers/database';
import { ApiGateway } from '../../services/api.gateway.service';

@IonicPage()
@Component({
    selector: 'page-visit-completion',
    templateUrl: 'visit.completion.html'
})
export class VisitCompletionPage {
    public _db: SQLiteObject;
    public _navCtrl: NavController;
    public visit: CustomerVisit = new CustomerVisit();

    public _rescheduleStartDate: any = new Date().toISOString();
    public _rescheduleStartTime: any = new Date().toISOString();

    constructor(
        public viewCtrl: ViewController,
        public platform: Platform,
        public loadingCtrl: LoadingController, public alertCtrl: AlertController, db: DatabaseProvider,
        private navParams: NavParams, navCtrl: NavController,
        private api: ApiGateway, private events: Events) {
        this._db = db.database;
        this._navCtrl = navCtrl;
    }

    public ionViewDidEnter() {
        new CustomerVisit().getById(this._db, +this.navParams.get('id'))
            .subscribe((v) => {
                if (v.rescheduleDateTime) {
                    this._rescheduleStartDate = new Date(v.rescheduleDateTime).toISOString();
                    this._rescheduleStartTime = new Date(v.rescheduleDateTime).toISOString();
                }
                this.visit = v;
            });
    }

    public completeVisit() {
        const loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        const v = new CustomerVisit();
        v.customerId = this.visit.customerId;
        v.completeVisit(this._db, this.visit.id, this.visit.objectiveMet, this.visit.comments, this.api)
            .subscribe(() => {
                loading.dismiss();
                this.events.publish('visit:changed');
                this.dismiss();
            }, () => {
                loading.dismiss();
            });
    }

    public dismiss() {
        this.viewCtrl.dismiss(true);
    }
}
