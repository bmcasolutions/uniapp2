import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { SharedModule } from '../../app/shared.module';
import { ContactsComponent } from './contacts';

@NgModule({
	declarations: [
		ContactsComponent
	],
	imports: [
		IonicPageModule.forChild(ContactsComponent),
		TranslateModule,
		SharedModule
	],
	exports: [
		ContactsComponent,
		TranslateModule
	]
})
export class ContactsModule { }
