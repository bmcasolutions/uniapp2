import { SQLiteObject } from '@ionic-native/sqlite';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { ApiGateway } from '../services/api.gateway.service';
import { IActionModel } from './action.models';
import { BaseModel } from './baseModel';
import { Customer } from './customer';
import { CustomerVisit } from './customerVisit';

export class Action extends BaseModel {
    public id: number;
    public customerId: number;
    public visitId: number;
    public name: string;
    public detail: string;
    public dueDate: string;
    public assignedTo: number;
    public assignedToName: string;
    public complete: boolean;
    public completedOn: string;
    public completedBy: number;
    public comments: string;
    public status: number;
    public priority: number;
    public outofSync: boolean;

    public insert(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`insert into actions(id,customerId,visitId,name,
            detail,dueDate,assignedTo,assignedToName,
            complete,completedOn,completedBy,comments,status,priority,outofSync)
        values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`, [this.id, this.customerId, this.visitId, this.name, this.detail,
            this.dueDate, this.assignedTo, this.assignedToName,
            +this.complete, this.completedOn, this.completedBy, this.comments, this.status, this.priority
                , +this.outofSync]))
            .map((results) => !!results.rowsAffected);
    }

    public update(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`update actions set customerId=?,visitId=?,name=?,
        detail=?,dueDate=?,assignedTo=?,
        assignedToName=?,completedOn=?,completedBy=?,status=?,priority=? where id=?`,
            [this.customerId, this.visitId, this.name, this.detail,
            this.dueDate, this.assignedTo, this.assignedToName,
            this.completedOn, this.completedBy, this.status, this.priority, this.id]))
            .map((results) => !!results.rowsAffected);
    }

    public getById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from actions where id=?', [id]))
            .map((results) => Customer.convertToArray(results.rows)[0]);
    }

    public deleteById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('delete from actions where id=?', [id]))
            .map((results) => !!results.rowsAffected);
    }

    public getByVisitId(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from actions where visitId=?', [id]))
            .map((results) => Customer.convertToArray(results.rows));
    }

    public static getAll(db: SQLiteObject): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from actions', []))
            .map((results) => Customer.convertToArray(results.rows));
    }

    public getOutofSync(db: SQLiteObject): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from actions where outofSync=1', []))
            .map((results) => Customer.convertToArray(results.rows));
    }

    public static deleteAll(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('delete from actions', []))
            .map((results) => !!results.rowsAffected);
    }

    public static syncAllData(api: ApiGateway, db: SQLiteObject): Observable<[string, number]> {
        return api.post('/syncvisitactionsall.asp', null, null)
            .pipe(map((res: any) => {
                if (!res || !res.CustomerActions) {
                    return [];
                }
                return res.CustomerActions
                    .map((datum) => {
                        const con = new Action();
                        return Object.assign(con,
                            {
                                id: datum.ActionID,
                                customerId: datum.CustomerID || 0,
                                name: datum.ActionName || '',
                                detail: datum.ActionDetails || '',
                                visitId: datum.VisitID || 0,
                                assignedTo: datum.AssignedTo || 0,
                                assignedToName: datum.AssignedToName || '',
                                dueDate: datum.DueDate || '',
                                comments: datum.Comments,
                                complete: !!datum.IsComplete,
                                completedOn: datum.CompletedOn,
                                completedBy: datum.CompletedBy,
                                status: datum.Status || 0,
                                priority: datum.Priority || 0,
                                outofSync: false
                            } as Action);
                    });
            })).flatMap((custs: Action[]) => {
                return Observable.from(custs)
                    .flatMap((datum) => datum.getById(db, datum.id)
                        .flatMap((d) => {
                            if (d) {
                                return datum.update(db);
                            } else {
                                return datum.insert(db);
                            }
                        }).flatMap(() => Observable.of(['action', 1] as [string, number])));
            });
    }

    // public static syncData(api: ApiGateway, db: SQLiteObject): Observable<[string, number]> {
    //     return Customer.getMyCustomers(db)
    //         .flatMap((arr) => Observable.from(arr))
    //         .flatMap((customer: any) => {
    //             return new CustomerVisit().getByCustomerId(db, customer.id)
    //                 .flatMap((arr) => Observable.from(arr))
    //                 .flatMap((visit: any) => {
    //                     return api.post('/syncvisitactions.asp', null, { cid: customer.id, VisitID: visit.id })
    //                         .pipe(map((res: any) => {
    //                             if (!res || !res.CustomerActions) {
    //                                 return [];
    //                             }
    //                             return res.CustomerActions
    //                                 .map((datum) => {
    //                                     const con = new Action();
    //                                     return Object.assign(con,
    //                                         {
    //                                             id: datum.ActionID,
    //                                             name: datum.ActionName || '',
    //                                             detail: datum.ActionDetails || '',
    //                                             visitId: datum.VisitID || 0,
    //                                             assignedTo: datum.AssignedTo || 0,
    //                                             assignedToName: datum.AssignedToName || '',
    //                                             dueDate: datum.DueDate || '',
    //                                             comments: datum.Comments,
    //                                             complete: !!datum.IsComplete,
    //                                             completedOn: datum.CompletedOn,
    //                                             completedBy: datum.CompletedBy,
    //                                             status: datum.Status || 0
    //                                         } as Action);

    //                                     // "ActionID": 101,
    //                                     // "CustomerID": 3197,
    //                                     // "ActionName": "Rfghj",
    //                                     // "ActionDetails": "Hhg",
    //                                     // "VisitID": 300,
    //                                     // "AssignedTo": 2,
    //                                     // "AssignedToName": "Graham Milligan",
    //                                     // "DueDate": "23/12/2018",
    //                                     // "Comments": null,
    //                                     // "Status": 1,
    //                                     // "IsComplete": false,
    //                                     // "CompletedOn": null,
    //                                     // "CompletedBy": null
    //                                 });
    //                         })).flatMap((custs: Action[]) => {
    //                             return Observable.from(custs)
    //                                 .flatMap((datum) => datum.getById(db, datum.id)
    //                                     .flatMap((d) => {
    //                                         if (d) {
    //                                             return datum.update(db);
    //                                         } else {
    //                                             return datum.insert(db);
    //                                         }
    //                                     }).flatMap(() => Observable.of(['action', 1] as [string, number])));
    //                         });
    //                 });
    //         });
    // }

    public completeAction(db: SQLiteObject, id: number, comments: string, api: ApiGateway): Observable<boolean> {
        const data = {
            Comments: comments,
            actionId: id
        };
        return api.post('/completeaction.asp', null, data)
            .flatMap(() => {
                return Observable.fromPromise(db.executeSql(`update actions set comments=?,
                complete=1,outofSync=0 where id=?`, [comments, id]))
                    .map((results) => !!results.rowsAffected)
                    .catch(() => Observable.of(true));
            }).catch(() => {
                return Observable.fromPromise(db.executeSql(`update actions set comments=?,
                complete=1,outofSync=1 where id=?`, [comments, id]))
                    .map((results) => !!results.rowsAffected)
                    .catch(() => Observable.of(true));
            });
    }

    public addEditAction(model: IActionModel, api: ApiGateway, db: SQLiteObject): Observable<boolean> {
        const data: any = {
            cid: model.customerId,
            VisitID: model.visitId || 0,
            ActionName: model.name,
            ActionDetails: model.detail,
            AssignedTo: model.assignedToId,
            DueDate: model.dueDate,
            Priority: model.priority
        };
        if (model.id) {
            data.CustomerActionID = model.id;
        }
        return api.post(model.id ? '/editaction.asp' : '/addaction.asp',
            null, data)
            .flatMap((res: any) => {
                return Action.syncAllData(api, db).last().map(() => true);
            }).catch(() => Observable.of(false));

    }
    public static deleteByActionId(model: any, api: ApiGateway, db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('delete from customer_contacts where id=?', [model.id]))
            .flatMap((v) => {
                const data: any = {
                    CustomerActionID: model.id,
                    cid: model.cid
                };
                if (model.id) {
                    data.cid = model.customerId;
                }
                return api.post('/deleteaction.asp',
                    null, data)
                    .flatMap((res: any) => {
                        return new Action().deleteById(db, model.id)
                            .flatMap(() => {
                                return Action.syncAllData(api, db).last().map(() => true);
                            });
                    }).catch(() => Observable.of(false));
            });
    }

}
