import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams, ToastController } from 'ionic-angular';
import { Customer } from '../../models/customer';
import { CustomerContact } from '../../models/customerContact';
import { CustomerLocation } from '../../models/customerLocation';
import { CustomerProduct } from '../../models/customerProduct';
import { CustomerVisit } from '../../models/customerVisit';
import { DatabaseProvider } from '../../providers/database';

@IonicPage()
@Component({
	selector: 'customer-details',
	templateUrl: 'customer_details.html'
})
export class Customer_detailsComponent {
	private customer: any = {};
	private segment: string = 'details';
	private contactSegment: string = 'contacts';
	private visitSegment: string = 'current';

	constructor(public toastCtrl: ToastController,
		public navCtrl: NavController,
		private dbSvc: DatabaseProvider,
		private param: NavParams, private modalCtrl: ModalController
	) {
		this.navCtrl = navCtrl;
	}

	public ionViewDidEnter() {
		this.loadCustomer();
	}

	public loadCustomer() {
		new Customer().getById(this.dbSvc.database, this.param.get('id'))
			.flatMap((c) => {
				return new CustomerLocation().getByCustomerId(this.dbSvc.database, this.param.get('id'));
			}, (c, locations) => {
				locations.forEach((l) => {
					l.text = l.name;
				});
				c.locations = locations;
				return c;
			})
			.flatMap((c) => {
				return new CustomerContact().getByCustomerId(this.dbSvc.database, this.param.get('id'));
			}, (c, contacts) => {
				contacts.forEach((con) => {
					con.text = con.name;
				});
				c.contacts = contacts;
				return c;
			})
			.flatMap((c) => {
				return new CustomerProduct().getByCustomerId(this.dbSvc.database, this.param.get('id'));
			}, (c, products) => {
				products.forEach((p) => {
					p.text = p.name;
				});
				c.products = products;
				return c;
			})
			.flatMap((c) => {
				return new CustomerVisit().getByCustomerId(this.dbSvc.database, this.param.get('id'));
			}, (c, visits) => {
				visits.forEach((v) => {
					v.text = v.locationName ? v.locationName : '';
					v.color = (!!v.isClosed || !!v.complete) ? '#4CAF50' : '#3286d0';
				});
				c.visits = visits;
				return c;
			})
			.subscribe((c) => {
				this.customer = c;
			});
	}

	private currentVisits(): any[] {
		return this.customer.visits.filter((v: CustomerVisit) => !v.isClosed && !v.complete).sort((a, b) => {
			if (!a.startDate || !b.startDate) {
				return -1;
			}
			return +new Date(a.startDate.replace(/(\d+)\/(\d+)\/(\d+)/, '$2/$1/$3'))
				< +new Date(b.startDate.replace(/(\d+)\/(\d+)\/(\d+)/, '$2/$1/$3'));
		});
	}

	private historicVisits(): any[] {
		return this.customer.visits.filter((v: CustomerVisit) => !!v.isClosed || !!v.complete).sort((a, b) => {
			if (!a.startDate || !b.startDate) {
				return -1;
			}
			return +new Date(a.startDate.replace(/(\d+)\/(\d+)\/(\d+)/, '$2/$1/$3'))
				< +new Date(b.startDate.replace(/(\d+)\/(\d+)\/(\d+)/, '$2/$1/$3'));
		});
	}

	public addEditCustomer($event, cust) {
		$event.stopPropagation();
		cust = cust || { id: 0 };
		const modal = this.modalCtrl.create('CustomerFormPage', {
			id: cust.id
		});
		modal.onDidDismiss((dataChanged) => {
			if (dataChanged) { this.loadCustomer(); }
		});
		modal.present();
	}

	public addLocationOrContactOrVisit($event) {
		if (this.contactSegment === 'locations') {
			this.addEditLocation($event, null);
		} else {
			this.addEditContact($event, null);
		}
	}

	public addEditLocation($event, loc) {
		$event.stopPropagation();
		loc = loc || { id: 0 };
		const modal = this.modalCtrl.create('LocationFormPage', {
			id: loc.id,
			cid: this.param.get('id')
		});
		modal.onDidDismiss((dataChanged) => {
			if (dataChanged) { this.loadCustomer(); }
		});
		modal.present();
	}

	public addEditContact($event, contact) {
		$event.stopPropagation();
		contact = contact || { id: 0 };
		const modal = this.modalCtrl.create('ContactFormPage', {
			id: contact.id,
			cid: this.param.get('id')
		});
		modal.onDidDismiss((dataChanged) => {
			if (dataChanged) { this.loadCustomer(); }
		});
		modal.present();
	}

	public editProduct($event, contact) {
		$event.stopPropagation();
		contact = contact || { id: 0 };
		const modal = this.modalCtrl.create('ProductFormPage', {
			id: contact.id,
			cid: this.param.get('id')
		});
		modal.onDidDismiss((dataChanged) => {
			if (dataChanged) { this.loadCustomer(); }
		});
		modal.present();
	}

	public visitDetail(id: number) {
		this.navCtrl.push('Visit_detailsComponent', { id });
	}

	public addEditVisit($event, visit) {
		$event.stopPropagation();
		visit = visit || { id: 0 };
		const modal = this.modalCtrl.create('VisitFormPage', {
			id: visit.id,
			cId: this.param.get('id'),
			action: 'full'
		});
		modal.onDidDismiss((dataChanged) => {
			if (dataChanged) { this.loadCustomer(); }
		});
		modal.present();
	}

	public openPage(component) {
		this.navCtrl.push(component);
	}

	public contactDetail(id: number) {
		this.navCtrl.push('Contact_detailsComponent', { id, cid: this.param.get('id') });
	}

	public locationDetail(id: number) {
		this.navCtrl.push('LocationDetailPage', { id, cid: this.param.get('id') });
	}

	public createTradeLink() {
		this.navCtrl.push('CustomerTradeLinksPage', { cid: this.param.get('id') });
	}

	public openStockist() {
		this.navCtrl.push('CustomerProductsPage', { cid: this.param.get('id') });
	}
	public openActions() {
		this.navCtrl.push('ActionsComponent', { cid: this.param.get('id') });
	}
}
