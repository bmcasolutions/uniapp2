import { IonicStorageModule } from '@ionic/storage';

import { AlertService } from '../providers/util/alert.service';
import { ToastService } from '../providers/util/toast.service';

import { Geolocation } from '@ionic-native/geolocation';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

// DIRECTIVES
import { DatePipe } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { Http } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SQLite } from '@ionic-native/sqlite';
import { SQLitePorter } from '@ionic-native/sqlite-porter';
import { TranslateLoader, TranslateModule, TranslateStaticLoader } from 'ng2-translate';
import { LoaderService } from '../common/services/loader.service';
import { Autosize } from '../components/autosize';
import { TextAvatarDirective } from '../components/text-avatar/text-avatar';
import { CallbackPipe } from '../pipes/callback/callback';
import { SearchPipe } from '../pipes/search/search';
import { SortPipe } from '../pipes/sort/sort';
import { DatabaseProvider } from '../providers/database';
import { SQLiteMock, SQLitePorterMock } from '../providers/sqlite.mock';
import { AccountService } from '../services/account.service';
import { ApiGateway } from '../services/api.gateway.service';
import { HttpAuthInjectionInterceptor } from '../services/http.auth.injector.interceptor';

export const MODULES = [
    BrowserAnimationsModule,
    HttpClientModule,
    IonicStorageModule.forRoot({
        driverOrder: ['sqlite', 'websql', 'indexeddb'],
        name: '__unitrunk_ionic'
    }),
    TranslateModule.forRoot({
        provide: TranslateLoader,
        useFactory: (http: Http) => new TranslateStaticLoader(http, 'assets/i18n', '.json'),
        deps: [Http]
    })
];

export const PROVIDERS = [
    AlertService,
    ToastService,
    LoaderService,
    SQLitePorter,
    SQLite,
    // { provide: SQLitePorter, useClass: SQLitePorterMock },
    // { provide: SQLite, useClass: SQLiteMock },
    DatabaseProvider,
    ApiGateway,
    AccountService,
    {
        multi: true,
        provide: HTTP_INTERCEPTORS,
        useClass: HttpAuthInjectionInterceptor
    },
    DatePipe
];

export const NATIVES = [
    SplashScreen,
    StatusBar,
    Geolocation
];

export const COMPONENTS = [
];

export const DIRECTIVES = [
    Autosize,
    TextAvatarDirective
];

export const PIPES = [
    CallbackPipe,
    SearchPipe,
    SortPipe
];
