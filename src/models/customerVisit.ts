import { SQLiteObject } from '@ionic-native/sqlite';
import { Observable } from 'rxjs/Observable';
import { defer } from 'rxjs/observable/defer';
import { map } from 'rxjs/operators';
import { ApiGateway } from '../services/api.gateway.service';
import { Action } from './action';
import { BaseModel } from './baseModel';
import { Customer } from './customer';
import { CustomerContact } from './customerContact';
import { CustomerLocation } from './customerLocation';
import { Objective } from './objective';
import { IQuickAddVisitModel, IVisitModel } from './visit.models';

export class CustomerVisit extends BaseModel {
    public id: number;
    public customerId: number;
    public siteId: number;
    public locationName: string;
    public customerName: string;
    public meetingOwner: string;
    public objective: string;
    public isClosed: boolean;
    public uId: number;
    public contactName: string;
    public positionHeld: string;
    public additionalStaff: number;
    public monthStart: number;
    public startDate: string;
    public endDate: string;
    public startHour: number;
    public startQuarter: number;
    public endHour: number;
    public endQuarter: number;
    public notes: string;
    public outcome: string;
    public isCancelled: boolean;
    public cancelledOn: string;
    public cancelledBy: string;
    public cancelleReason: string;
    public checkinDateTime: number;
    public checkInGeoLat: number;
    public checkInGeoLng: number;
    public comments: string;
    public objectiveMet: boolean;
    public isOther: boolean;
    public checkInComplete: boolean;
    public complete: boolean;
    public outofSync: boolean;

    public insert(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`insert into customer_visits(id,customerId,siteId,locationName,
            customerName,meetingOwner,objective,objectiveMet,isClosed,uId,contactName,positionHeld,additionalStaff,
            monthStart,startDate,endDate,startHour,startQuarter,endHour,endQuarter,comments,notes,outcome,isCancelled,
            cancelledOn,cancelledBy,cancelleReason,isOther,checkInComplete,complete,outofSync) values(?,?,?,?,?,?,?,
            ?,?,?,?,?,?,?,?,?,?,?,?,
            ?,?,?,?,?,?,?,?,?,?,?,?)`, [this.id, this.customerId, this.siteId, this.locationName, this.customerName,
            this.meetingOwner,
            this.objective, +this.objectiveMet, +this.isClosed, this.uId, this.contactName, this.positionHeld,
            this.additionalStaff, this.monthStart, this.startDate, this.endDate, this.startHour, this.startQuarter,
            this.endHour, this.endQuarter, this.comments, this.notes, this.outcome, +this.isCancelled, this.cancelledOn,
            this.cancelledBy, this.cancelleReason, +this.isOther, +this.checkInComplete, +this.complete,
            +this.outofSync]))
            .map((results) => !!results.rowsAffected);
    }

    public update(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`update customer_visits set customerId=?,siteId=?,locationName=?,
        customerName=?,meetingOwner=?,objective=?,objectiveMet=?,isClosed=?,uId=?,contactName=?,positionHeld=?,
        additionalStaff=?,monthStart=?,startDate=?,endDate=?,startHour=?,startQuarter=?,endHour=?,endQuarter=?,
        comments=?,notes=?,outcome=?,isCancelled=?,cancelledOn=?,cancelledBy=?,cancelleReason=?,isOther=? where id=?`,
            [this.customerId, this.siteId, this.locationName, this.customerName, this.meetingOwner, this.objective,
            +this.objectiveMet, +this.isClosed, this.uId, this.contactName, this.positionHeld, this.additionalStaff,
            this.monthStart, this.startDate, this.endDate, this.startHour, this.startQuarter, this.endHour,
            this.endQuarter, this.comments, this.notes, this.outcome, +this.isCancelled, this.cancelledOn,
            this.cancelledBy, this.cancelleReason, +this.isOther, this.id]))
            .map((results) => !!results.rowsAffected);
    }

    public static getAll(db: SQLiteObject): Observable<any[]> {
        return Observable.fromPromise(db.executeSql('select * from customer_visits', []))
            .map((results) => Customer.convertToArray(results.rows));
    }

    public getById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from customer_visits where id=?', [id]))
            .map((results) => Customer.convertToArray(results.rows)[0]);
    }
    public deleteById(db: SQLiteObject, id: number): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('delete from customer_visits where id=?', [id]))
            .map((results) => !!results.rowsAffected);
    }
    public getByCustomerId(db: SQLiteObject, id: number): Observable<any[]> {
        return Observable.fromPromise(db.executeSql('select * from customer_visits where customerId=?', [id]))
            .map((results) => Customer.convertToArray(results.rows));
    }

    public getOutofSync(db: SQLiteObject): Observable<any[]> {
        return Observable.fromPromise(db.executeSql('select * from customer_visits where outofSync=1', []))
            .map((results) => Customer.convertToArray(results.rows));
    }

    public static deleteAll(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('delete from customer_visits', []))
            .map((results) => !!results.rowsAffected);
    }

    public static syncBackOutOfSync(api: ApiGateway, db: SQLiteObject): Observable<[string, number]> {
        return Observable.forkJoin(
            new CustomerVisit().getOutofSync(db),
            new Action().getOutofSync(db))
            .flatMap((data1: any[]) => {
                const visits = data1[0];
                const actions = data1[1];
                const data = {
                    visitdata: visits.map((v) => ({
                        visitId: v.id,
                        Comments: v.comments,
                        ObjectiveMet: v.objectiveMet
                    })),
                    actiondata: actions.map((a) => ({
                        actionId: a.id,
                        Comments: a.comments
                    }))
                };
                return api.post('/completeupload.asp', null, data)
                    .flatMap(() => {
                        return Observable.from([['customer_visit', 1] as [string, number],
                        ['customer_visit', 1] as [string, number]]);
                    });
            });
    }

    public static syncAllData(api: ApiGateway, db: SQLiteObject): Observable<[string, number]> {
        return api.post('/synccustomervisitsall.asp', null, null)
            .pipe(map((res: any) => {
                if (!res || !res.CustomerVisits) {
                    return [];
                }
                return res.CustomerVisits
                    .map((datum: any) => {
                        const v = new CustomerVisit();
                        return Object.assign(v,
                            {
                                id: datum.VisitID,
                                customerId: datum.CustomerID,
                                siteId: datum.SiteID,
                                locationName: datum.LocationName,
                                customerName: datum.CustomerName,
                                meetingOwner: datum.MeetingOwner,
                                objective: datum.Objective,
                                objectiveMet: !!datum.ObjectiveMet,
                                isClosed: !!datum.IsClosed,
                                uId: datum.UID,
                                contactName: datum.ContactName,
                                positionHeld: datum.PositionHeld,
                                additionalStaff: datum.AdditionalStaff,
                                monthStart: datum.MonthStart,
                                startDate: datum.StartDate,
                                endDate: datum.EndDate,
                                startHour: datum.StartHour,
                                startQuarter: datum.StartQuarter,
                                endHour: datum.EndHour,
                                endQuarter: datum.EndQuarter,
                                comments: datum.Comments,
                                notes: datum.Notes,
                                outcome: datum.Outcome,
                                isCancelled: !!datum.IsCancelled,
                                cancelledOn: datum.CancelledOn,
                                cancelledBy: datum.CancelledBy,
                                cancelleReason: datum.CancelReason,
                                isOther: false,
                                checkInComplete: false,
                                complete: false,
                                outofSync: false
                            } as CustomerVisit);
                    });
            })).flatMap((custs: CustomerVisit[]) => {
                return Observable.from(custs)
                    .flatMap((datum) => datum.getById(db, datum.id)
                        .flatMap((d) => {
                            if (d) {
                                return datum.update(db);
                            } else {
                                return datum.insert(db);
                            }
                        }).flatMap(() => Observable.of(['customer_visit', 1] as [string, number])));
            });
    }

    // public static syncData(api: ApiGateway, db: SQLiteObject): Observable<[string, number]> {
    //     return this.ownVisits(api, db)
    //         .merge(this.otherVisits(api))
    //         .flatMap((custs: CustomerVisit[]) => {
    //             return Observable.from(custs)
    //                 .flatMap((datum) => datum.getById(db, datum.id)
    //                     .flatMap((d) => {
    //                         if (d) {
    //                             return datum.update(db);
    //                         } else {
    //                             return datum.insert(db);
    //                         }
    //                     }).flatMap(() => Observable.of(['customer_visit', 1] as [string, number])));
    //         });
    // }

    // public static ownVisits(api: ApiGateway, db: SQLiteObject) {
    //     return Customer.getMyCustomers(db)
    //         .flatMap((arr) => Observable.from(arr))
    //         .flatMap((customer: any) => {
    //             return api.post('/synccustomervisits.asp', null, { cid: customer.id })
    //                 .pipe(map((res: any) => {
    //                     if (!res || !res.CustomerVisits) {
    //                         return [];
    //                     }
    //                     return res.CustomerVisits
    //                         .map((datum) => {
    //                             const v = new CustomerVisit();
    //                             return Object.assign(v,
    //                                 {
    //                                     id: datum.VisitID,
    //                                     customerId: datum.CustomerID,
    //                                     siteId: datum.SiteID,
    //                                     locationName: datum.LocationName,
    //                                     customerName: datum.CustomerName,
    //                                     meetingOwner: datum.MeetingOwner,
    //                                     objective: datum.Objective,
    //                                     objectiveMet: !!datum.ObjectiveMet,
    //                                     isClosed: !!datum.IsClosed,
    //                                     uId: datum.UID,
    //                                     contactName: datum.ContactName,
    //                                     positionHeld: datum.PositionHeld,
    //                                     additionalStaff: datum.AdditionalStaff,
    //                                     monthStart: datum.MonthStart,
    //                                     startDate: datum.StartDate,
    //                                     endDate: datum.EndDate,
    //                                     startHour: datum.StartHour,
    //                                     startQuarter: datum.StartQuarter,
    //                                     endHour: datum.EndHour,
    //                                     endQuarter: datum.EndQuarter,
    //                                     comments: datum.Comments,
    //                                     notes: datum.Notes,
    //                                     outcome: datum.Outcome,
    //                                     isCancelled: !!datum.IsCancelled,
    //                                     cancelledOn: datum.CancelledOn,
    //                                     cancelledBy: datum.CancelledBy,
    //                                     cancelleReason: datum.CancelReason,
    //                                     isOther: false,
    //                                     checkInComplete: false,
    //                                     complete: false
    //                                 } as CustomerVisit);
    //                         });
    //                 }));
    //         });
    // }

    // public static otherVisits(api: ApiGateway) {
    //     return api.post('/syncothervisits.asp', null, null)
    //         .pipe(map((res: any) => {
    //             if (!res || !res.OtherCustomerVisits) {
    //                 return [];
    //             }
    //             return res.OtherCustomerVisits
    //                 .map((datum) => {
    //                     const v = new CustomerVisit();
    //                     return Object.assign(v,
    //                         {
    //                             id: datum.VisitID,
    //                             customerId: datum.CustomerID,
    //                             siteId: datum.SiteID,
    //                             locationName: datum.LocationName,
    //                             customerName: datum.CustomerName,
    //                             meetingOwner: datum.MeetingOwner,
    //                             objective: datum.Objective,
    //                             objectiveMet: !!datum.ObjectiveMet,
    //                             isClosed: !!datum.IsClosed,
    //                             uId: datum.UID,
    //                             contactName: datum.ContactName,
    //                             positionHeld: datum.PositionHeld,
    //                             additionalStaff: datum.AdditionalStaff,
    //                             monthStart: datum.MonthStart,
    //                             startDate: datum.StartDate,
    //                             endDate: datum.EndDate,
    //                             startHour: datum.StartHour,
    //                             startQuarter: datum.StartQuarter,
    //                             endHour: datum.EndHour,
    //                             endQuarter: datum.EndQuarter,
    //                             comments: datum.Comments,
    //                             notes: datum.Notes,
    //                             outcome: datum.Outcome,
    //                             isCancelled: !!datum.IsCancelled,
    //                             cancelledOn: datum.CancelledOn,
    //                             cancelledBy: datum.CancelledBy,
    //                             cancelleReason: datum.CancelReason,
    //                             isOther: true,
    //                             checkInComplete: false,
    //                             complete: false
    //                         } as CustomerVisit);
    //                 });
    //         }));
    // }

    public checkInVisit(db: SQLiteObject, id: number, lat: number, lng: number, dateTime: number,
        api: ApiGateway): Observable<boolean> {
        const data = {
            cid: this.customerId,
            visitdata: [{
                checkInGeoLat: lat,
                checkInGeoLng: lng,
                // tslint:disable-next-line:max-line-length
                checkinDateTime: `${new Date(dateTime).toLocaleDateString('en-GB')}, ${new Date(dateTime).toLocaleTimeString('en-GB')}`,
                visitId: id
            }]
        };
        return api.post('/checkinvisit.asp', null, data)
            .flatMap(() => {
                return Observable.fromPromise(db.executeSql(`update customer_visits set checkInGeoLat=?,checkInGeoLng=?,
                checkInDateTime=?,checkInComplete=1,outofSync=0 where id=?`, [lat, lng, dateTime, id]))
                    .map((results) => !!results.rowsAffected)
                    .catch(() => {
                        // tslint:disable-next-line:no-console
                        console.log('true1');
                        return Observable.of(true);
                    });
            }).catch(() => {
                return Observable.fromPromise(db.executeSql(`update customer_visits set checkInGeoLat=?,checkInGeoLng=?,
                checkInDateTime=?,checkInComplete=1,outofSync=1 where id=?`, [lat, lng, dateTime, id]))
                    .map((results) => !!results.rowsAffected)
                    .catch(() => Observable.of(true));
            });
    }

    public reassign(visitId: number, assignedToId: number, api: ApiGateway): Observable<boolean> {
        const data = {
            VisitID: visitId,
            AssignToID: assignedToId
        };
        return api.post('/reassignvisit.asp', null, data)
            .flatMap(() => Observable.of(true));
    }

    public completeVisit(db: SQLiteObject, id: number, objectiveMet: boolean,
        comments: string, api: ApiGateway): Observable<boolean> {
        const data = {
            cid: this.customerId,
            visitdata: [{
                Comments: comments,
                ObjectiveMet: +objectiveMet,
                visitId: id
            }]
        };
        return api.post('/completevisit.asp', null, data)
            .flatMap(() => {
                return Observable.fromPromise(db.executeSql(`update customer_visits set objectiveMet=?,comments=?,
                complete=1 where id=?`, [+objectiveMet, comments, id]))
                    .map((results) => !!results.rowsAffected)
                    .catch(() => Observable.of(true));
            }).catch(() => {
                return Observable.fromPromise(db.executeSql(`update customer_visits set objectiveMet=?,comments=?,
                complete=1 where id=?`, [+objectiveMet, comments, id]))
                    .map((results) => !!results.rowsAffected)
                    .catch(() => Observable.of(true));
            });
    }
    public static deleteByVisitId(model: any, api: ApiGateway, db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('delete from customer_visits where id=?', [model.id]))
            .flatMap((v) => {
                const data: any = {
                    VisitID: model.id,
                    cid: model.cid
                };
                if (model.id) {
                    data.cid = model.customerId;
                }
                return api.post('/deletevisit.asp',
                    null, data)
                    .flatMap((res: any) => {
                        return CustomerVisit.syncAllData(api, db).last().map(() => true);
                    }).catch(() => Observable.of(false));
            });
    }
    public addEditVisit(model: IVisitModel, api: ApiGateway, db: SQLiteObject): Observable<boolean> {
        return new Customer().getById(db, model.customerId)
            .flatMap((cust: Customer) => {
                const stream = defer(() => {
                    if (model.id) {
                        return Objective.getAll(db)
                            .flatMap((objs: any[]) => {
                                return Observable.of({
                                    cid: model.customerId,
                                    VisitID: model.id,
                                    SiteID: model.siteId,
                                    StartDate: model.startDate,
                                    StartTime: model.startTime,
                                    EndTime: model.endTime,
                                    Notes: model.notes,
                                    ObjectiveID: objs.filter((o) => o.name === model.objective)[0].id,
                                    ContactID: model.contactId
                                });
                            });

                    } else {
                        return Objective.getAll(db)
                            .flatMap((objs: any[]) => {
                                return Observable.of({
                                    cid: model.customerId,
                                    SiteID: model.siteId,
                                    ObjectiveID: objs.filter((o) => o.name === model.objective)[0].id,
                                    StartDate: model.startDate,
                                    StartTime: model.startTime,
                                    EndTime: model.endTime,
                                    AdditionalStaff: model.additionalStaff,
                                    Notes: model.notes,
                                    ContactID: model.contactId
                                });
                            });
                    }
                });
                return stream.flatMap((data) => {
                    return api.post(model.id ? '/editvisit.asp' : '/addvisit.asp',
                        null, data)
                        .flatMap((res: any) => {
                            if (!!cust.isOther) {
                                return new CustomerContact().getById(db, data.ContactID)
                                    .flatMap((contact: CustomerContact) => {
                                        return new CustomerLocation().getById(db, data.SiteID)
                                            .flatMap((site: CustomerLocation) => {
                                                const v = new CustomerVisit();
                                                Object.assign(v,
                                                    {
                                                        id: -new Date(),
                                                        customerId: data.cid,
                                                        siteId: data.SiteID,
                                                        locationName: site.name,
                                                        customerName: cust.name,
                                                        meetingOwner: '',
                                                        objective: model.objective,
                                                        objectiveMet: false,
                                                        isClosed: false,
                                                        uId: -1,
                                                        contactName: contact.name,
                                                        positionHeld: '',
                                                        additionalStaff: model.additionalStaff,
                                                        monthStart: '',
                                                        startDate: data.StartDate,
                                                        endDate: data.StartDate,
                                                        startHour: data.StartTime.split(':')[0],
                                                        startQuarter: data.StartTime.split(':')[1],
                                                        endHour: data.EndTime.split(':')[0],
                                                        endQuarter: data.EndTime.split(':')[1],
                                                        comments: '',
                                                        notes: data.Notes,
                                                        outcome: '',
                                                        isCancelled: false,
                                                        cancelledOn: '',
                                                        cancelledBy: '',
                                                        cancelleReason: '',
                                                        checkInComplete: false,
                                                        complete: false,
                                                        checkinDateTime: '',
                                                        checkInGeoLat: 0,
                                                        checkInGeoLng: 0
                                                    });
                                                return v.insert(db);
                                            });
                                    });
                            } else {
                                return CustomerVisit.syncAllData(api, db).last().map(() => true);
                            }
                        }).catch(() => Observable.of(false));
                });
            });
    }

    public addQuickVisit(model: IQuickAddVisitModel, api: ApiGateway, db: SQLiteObject): Observable<boolean> {
        return new Customer().getById(db, model.customerId)
            .flatMap((cust: Customer) => {
                const stream = defer(() => {
                    return Objective.getAll(db)
                        .flatMap((objs: any[]) => {
                            return Observable.of({
                                cid: model.customerId,
                                SiteID: model.siteId,
                                ObjectiveID: objs.filter((o) => o.name === model.objective)[0].id,
                                ContactID: model.contactId
                            });
                        });
                });
                return stream.flatMap((data) => {
                    return api.post('/addquickvisit.asp', null, data)
                        .flatMap((res: any) => {
                            return CustomerVisit.syncAllData(api, db).last().map(() => true);
                        }).catch(() => Observable.of(false));
                });
            });
    }
}
