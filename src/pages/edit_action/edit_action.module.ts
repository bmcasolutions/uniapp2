import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { Edit_actionComponent } from './edit_action';

@NgModule({
  declarations: [
    Edit_actionComponent
  ],
  imports: [
    IonicPageModule.forChild(Edit_actionComponent),
    TranslateModule
  ],
  exports: [
    Edit_actionComponent,
    TranslateModule
  ]
})
export class Edit_actionModule { }
