import { Component } from '@angular/core';
import {
	AlertController, IonicPage, LoadingController, ModalController, NavController,
	NavParams, ToastController
} from 'ionic-angular';
import { CustomerTradeLink } from '../../models/customerTradeLink';
import { DatabaseProvider } from '../../providers/database';
import { ApiGateway } from '../../services/api.gateway.service';

@IonicPage()
@Component({
	selector: 'customer-tradelinks',
	templateUrl: 'customer.tradelinks.html'
})
export class CustomerTradeLinksPage {
	public tradeLinks: any[] = [];
	public customerId: number;
	constructor(public toastCtrl: ToastController, public navCtrl: NavController, private dbSvc: DatabaseProvider,
		private param: NavParams, private modalCtrl: ModalController,
		private alertCtrl: AlertController,
		private loadingCtrl: LoadingController,
		private api: ApiGateway) {
		this.navCtrl = navCtrl;
	}

	public openPage(component) {
		this.navCtrl.push(component);
	}
	public ionViewDidEnter() {
		this.loadCustomersTradeLinks();
	}
	public loadCustomersTradeLinks() {
		this.customerId = this.param.get('cid');
		new CustomerTradeLink().getByCustomerId(this.dbSvc.database, this.param.get('cid'))
			.subscribe((c) => {
				c.forEach((el) => {
					el.text = el.name;
				});
				this.tradeLinks = c;
			});
	}

	public addEditTradelink($event, cust) {
		$event.stopPropagation();
		cust = cust || { id: 0 };
		const modal = this.modalCtrl.create('TradeLinkFormPage', {
			id: cust.id,
			customerId: this.param.get('cid')

		});
		modal.onDidDismiss((dataChanged) => {
			if (dataChanged) { this.loadCustomersTradeLinks(); }
		});
		modal.present();
	}

	public deleteTradeLink($event, tradelink) {
		const confirm = this.alertCtrl.create({
			buttons: [
				{
					handler: () => {
						// console.log('Disagree clicked');
					},
					text: 'No'
				},
				{
					handler: () => {
						const loading = this.loadingCtrl.create({
							content: 'Please wait...'
						});
						loading.present();

						const v = new CustomerTradeLink();
						v.customerId = tradelink.customerId;
						CustomerTradeLink.deleteById(tradelink, this.api, this.dbSvc.database)
							.subscribe(() => {
								loading.dismiss();
								this.loadCustomersTradeLinks();
							}, () => {
								loading.dismiss();
							});

					},
					text: 'Yes'
				}
			],
			message: 'Are you sure you want to delete this tradelink?',
			title: `Delete?`
		});
		confirm.present();
	}
}
