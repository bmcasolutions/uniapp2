import { Component } from '@angular/core';
import {
	AlertController, IonicPage, LoadingController,
	ModalController, NavController, NavParams, ToastController
} from 'ionic-angular';
import { CustomerContact } from '../../models/customerContact';
import { DatabaseProvider } from '../../providers/database';
import { ApiGateway } from '../../services/api.gateway.service';

@IonicPage()
@Component({
	selector: 'contact_details',
	templateUrl: 'contact_details.html'
})

export class Contact_detailsComponent {

	private contact: any = {};

	constructor(public toastCtrl: ToastController, private alertCtrl: AlertController,
		private loadingCtrl: LoadingController,
		public navCtrl: NavController,
		private dbSvc: DatabaseProvider,
		private api: ApiGateway,
		private param: NavParams, private modalCtrl: ModalController
	) {
		this.navCtrl = navCtrl;
	}

	public ionViewDidEnter() {
		this.loadCustomer();
	}

	public loadCustomer() {
		new CustomerContact().getById(this.dbSvc.database, this.param.get('id'))
			.subscribe((c) => {
				this.contact = c;
			});
	}
	public addEditContact($event, cust) {
		$event.stopPropagation();
		cust = cust || { id: 0 };
		const modal = this.modalCtrl.create('ContactFormPage', {
			id: cust.id,
			cid: cust.customerId || this.param.get('cid')
		});
		modal.onDidDismiss((dataChanged) => {
			if (dataChanged) { this.loadCustomer(); }
		});
		modal.present();
	}

	public deleteContact($event, contact) {
		const confirm = this.alertCtrl.create({
			buttons: [
				{
					handler: () => {
						// console.log('Disagree clicked');
					},
					text: 'No'
				},
				{
					handler: () => {
						const loading = this.loadingCtrl.create({
							content: 'Please wait...'
						});
						loading.present();

						const v = new CustomerContact();
						v.customerId = this.contact.customerId;
						CustomerContact.deleteByContactId(this.contact, this.api, this.dbSvc.database)
							.subscribe(() => {
								// tslint:disable-next-line:no-console
								console.log(true);

								loading.dismiss();
								this.navCtrl.pop();
								// this.customerContacts(this.contact.customerId);
								// this.loadVisit();
							}, () => {
								// tslint:disable-next-line:no-console
								console.log(false);
								loading.dismiss();
							});

					},
					text: 'Yes'
				}
			],
			message: 'Are you sure you want to delete this contact?',
			title: `Delete contact ${this.contact.id}?`
		});
		confirm.present();
	}
	public openPage(component) {
		this.navCtrl.push(component);
	}
	public customerContacts(id: number) {
		this.navCtrl.push('Customer_contactsComponent', { id });
	}
}
