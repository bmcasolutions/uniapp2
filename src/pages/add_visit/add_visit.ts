/** Represents a Component of Google Map. */

/** Imports Modules */
import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'add_visit',
	templateUrl: 'add_visit.html'
})

export class Add_visitComponent {

	constructor(public toastCtrl: ToastController, public navCtrl: NavController) {
		this.navCtrl = navCtrl;
	}

	public openPage(component) {
		this.navCtrl.setRoot(component);
	}

	public event = {
		month: '1990-02-19',
		timeStarts: '07:43',
		timeEnds: '1990-02-20'
	};
}
