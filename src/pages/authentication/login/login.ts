import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { AlertController, IonicPage, MenuController, NavController } from 'ionic-angular';
import { TranslateService } from 'ng2-translate'; // Translate Service
import { Observable } from 'rxjs';
import { LoaderService } from '../../../common/services/loader.service';
import { User } from '../../../models/user';
import { DatabaseProvider } from '../../../providers/database';
import { AlertService } from '../../../providers/util/alert.service';
import { AccountService } from '../../../services/account.service';

@IonicPage()
@Component({
  selector: 'login',
  templateUrl: 'login.html'
})
export class LoginComponent {

  private user: any = {};

  constructor(public navCtrl: NavController,
    private alertSvc: AlertService,
    public translate: TranslateService,
    private menu: MenuController,
    private loaderSvc: LoaderService,
    private accSvc: AccountService,
    private alertCtrl: AlertController,
    private db: DatabaseProvider,
    private accountService: AccountService) {
    this.navCtrl = navCtrl;
    this.menu = menu;
    this.menu.enable(false); // Disable sidemenu
  }

  public login() {
    this.loaderSvc.presentLoading();

    this.accSvc.login(this.user)
      .subscribe(() => {
        this.loaderSvc.hideLoading();
        // loading.dismiss({ success: true });
      }, (error) => {
        if (error instanceof HttpErrorResponse) {
          new Promise((resolve) => {
            switch (error.status) {
              case 0:
                User.getUser(this.db.database)
                  .flatMap((u: User) => {
                    if (u && u.userName.toLowerCase() === this.user.userName.toLowerCase()
                      && u.password === this.user.password) {
                      u.loggedIn = true;
                      let user = new User();
                      user = Object.assign(user, u);
                      return User.deleteLoggedInUser(this.db.database)
                        .flatMap(() => this.accountService.setUserInfo(user));
                    }
                    return Observable.of(false);
                  }).subscribe((success) => resolve({ success, message: success ? '' : 'Invalid credential' }));
                break;
              case 401:
                resolve({ success: false, message: 'Invalid credential' });
              default:
                resolve({ success: false, message: error.message });
            }
          }).then((data: any) => {
            this.loaderSvc.hideLoading();
            if (!data.success) {
              this.alertSvc.presentAlert('Error!', data.message);
            }
          });
        } else {
          this.loaderSvc.hideLoading();
          this.alertSvc.presentAlert('Error!', error.code === 401 ? 'Invalid Credential' : error.message);
        }
      });
  }

  /**
   * Forget Password
   * Open forget password alert box after click on forget password buttons
   * @input   email   Alert box contains one email input box and
   * translate  service used in placeholder
   * @button    Cancel
   * @button    Send
   */
  public showForgetPasswordPopup() {
    const prompt = this.alertCtrl.create({
      title: this.translate.instant('FORGET_PASSWORD.TITLE'),
      message: this.translate.instant('FORGET_PASSWORD.SUBTITLE'),
      inputs: [{
        name: 'email',
        placeholder: this.translate.instant('FORGET_PASSWORD.EMAIL')
      }],
      buttons: [{
        text: this.translate.instant('FORGET_PASSWORD.CANCEL'),
        handler: () => {
          // console.log('Cancel clicked');
        }
      }, {
        text: this.translate.instant('FORGET_PASSWORD.SEND'),
        handler: (data) => {
          // console.log('Send clicked');
        }
      }]
    });
    prompt.present();
  }

  public openPage(component) {
    this.navCtrl.setRoot(component);
  }
}
