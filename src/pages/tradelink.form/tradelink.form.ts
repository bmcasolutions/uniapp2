import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { IonicPage, LoadingController, NavParams, ViewController } from 'ionic-angular';
import { CustomerTradeLink } from '../../models/customerTradeLink';
import { DatabaseProvider } from '../../providers/database';
import { ToastService } from '../../providers/util/toast.service';
import { AccountService } from '../../services/account.service';
import { ApiGateway } from '../../services/api.gateway.service';

@IonicPage()
@Component({
    selector: 'tradelink-form',
    templateUrl: 'tradelink.form.html'
})
export class TradeLinkFormPage {
    private _db: SQLiteObject;
    private tradelink: any = {};

    constructor(public loadingCtrl: LoadingController,
        public params: NavParams, public viewCtrl: ViewController, db: DatabaseProvider,
        private api: ApiGateway,
        private accSvc: AccountService,
        private toastSvc: ToastService) {
        this._db = db.database;
        if (this.params.get('id') > 0) {
            new CustomerTradeLink().getById(this._db, this.params.get('id'))
                .subscribe((ac: CustomerTradeLink) => {
                    this.tradelink = ac;

                });
        }

    }

    public save() {
        this.tradelink.customerId = this.params.get('customerId');
        const loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.accSvc.getUserInfo()
            .flatMap(() => {
                return new CustomerTradeLink().addEditTradelink(this.tradelink, this.api, this._db);
            }).subscribe((success) => {
                loading.dismiss();
                if (success) {
                    this.dismiss(true);
                }
            }, (err) => {
                let msg = err.message;
                loading.dismiss();
                if (err instanceof HttpErrorResponse) {
                    msg = 'Bad internet connection';
                }
                this.toastSvc.create(msg);
            });
    }

    public dismiss(certChanged: boolean) {
        this.viewCtrl.dismiss(!!certChanged);
    }
}
