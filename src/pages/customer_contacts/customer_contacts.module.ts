import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { SharedModule } from '../../app/shared.module';
import { Customer_contactsComponent } from './customer_contacts';

@NgModule({
	declarations: [
		Customer_contactsComponent
	],
	imports: [
		IonicPageModule.forChild(Customer_contactsComponent),
		TranslateModule,
		SharedModule
	],
	exports: [
		Customer_contactsComponent,
		TranslateModule
	]
})
export class Customer_contactsModule { }
