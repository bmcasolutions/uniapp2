export interface ICustomerModel {
    id: number;
    refNo: string;
    name: string;
    address1: string;
    address2: string;
    address3: string;
    address4: string;
    postCode: string;
    telephone: string;
    type: string;
    notes: string;
    contactName: string;
    contactPhone: string;
    contactEmail: string;
    countryId: number;
    url: string;
    active: boolean;
    nextContactOn: string;
    lastContactOn: string;
    billingEmail1: string;
    billingEmail2: string;
    financeCode: string;
    vatNo: string;
    country: string;
    creditlimit: number;
}

export interface ILocationModel {
    id: number;
    customerId: number;
    name: string;
    address1: string;
    address2: string;
    address3: string;
    address4: string;
    postCode: string;
    isDefault: number;
}

export interface IProductModel {
    id: number;
    customerId: number;
    name: string;
    productId: number;
    supplierId: number;
    isStockist: boolean;
    supplierName: string;
}
