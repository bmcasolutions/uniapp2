import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { ApiGateway } from '../services/api.gateway.service';

export class Report {
    public report1Data: any[];
    public chart1Data: any[];
    public chart2Data: any[];

    constructor() {
        // this.chart1Data = [];
        // this.chart2Data = [];
    }

    public static fetchReport(api: ApiGateway): Observable<Report> {
        return Observable.combineLatest(
            api.post('/syncreport1.asp', null, null)
                .pipe(map((res: any) => {
                    if (!res || !res.Reports1) {
                        return [];
                    }
                    return res.Reports1;
                })),
            api.post('/syncreport2.asp', null, null)
                .pipe(map((res: any) => {
                    if (!res || !res.ChartsData) {
                        return [];
                    }
                    return res.ChartsData;
                })))
            .flatMap(([r1, r2]) => {
                const r = new Report();
                const c1Count = r2.WholeSaleInLastWeek + r2.WholeSaleInLastMonth + r2.WholeSaleInLast2Month
                    + r2.WholeSaleInLast4Month + r2.WholeSaleInLast6Months + r2.WholeSaleOver6Months;

                r.report1Data = r1;

                r.chart1Data = [{
                    name: ' Last Week : ' + +((r2.WholeSaleInLastWeek * 100) / c1Count).toFixed(2) + '% ',
                    value: r2.WholeSaleInLastWeek
                }, {
                    name: 'Last Month : ' + +((r2.WholeSaleInLastMonth * 100) / c1Count).toFixed(2) + '% ',
                    value: r2.WholeSaleInLastMonth
                }, {
                    name: '2 Months : ' + +((r2.WholeSaleInLast2Month * 100) / c1Count).toFixed(2) + '% ',
                    value: r2.WholeSaleInLast2Month
                }, {
                    name: '4 Months : ' + +((r2.WholeSaleInLast4Month * 100) / c1Count).toFixed(2) + '% ',
                    value: r2.WholeSaleInLast4Month
                }, {
                    name: '6 Months : ' + +((r2.WholeSaleInLast6Months * 100) / c1Count).toFixed(2) + '% ',
                    value: r2.WholeSaleInLast6Months
                }, {
                    name: '>6 Months : ' + +((r2.WholeSaleOver6Months * 100) / c1Count).toFixed(2) + '% ',
                    value: r2.WholeSaleOver6Months
                }];

                const c2Count = r2.ContactorInLastWeek + r2.ContactorInLastMonth + r2.ContactorInLast2Month
                    + r2.ContactorInLast4Month + r2.ContactorInLast6Months + r2.ContactorOver6Months;

                r.chart2Data = [{
                    name: ' Last Week : ' + +((r2.ContactorInLastWeek * 100) / c2Count).toFixed(2) + '% ',
                    value: r2.ContactorInLastWeek
                }, {
                    name: ' Last Month : ' + ((r2.ContactorInLastMonth * 100) / c2Count).toFixed(2) + '% ',
                    value: r2.ContactorInLastMonth
                }, {
                    name: ' 2 Months : ' + ((r2.ContactorInLast2Month * 100) / c2Count).toFixed(2) + '% ',
                    value: r2.ContactorInLast2Month
                }, {
                    name: ' 4 Months : ' + ((r2.ContactorInLast4Month * 100) / c2Count).toFixed(2) + '% ',
                    value: r2.ContactorInLast4Month
                }, {
                    name: ' 6 Months : ' + ((r2.ContactorInLast6Months * 100) / c2Count).toFixed(2) + '% ',
                    value: r2.ContactorInLast6Months
                }, {
                    name: ' >6 Months : ' + ((r2.ContactorOver6Months * 100) / c2Count).toFixed(2) + '% ',
                    value: r2.ContactorOver6Months
                }];
                return Observable.of(r);
            });
    }
}
