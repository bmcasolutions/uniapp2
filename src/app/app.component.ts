/** Imports Modules */
import { Component, Injector, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Config, Nav, Platform } from 'ionic-angular';
import { TranslateService } from 'ng2-translate';
import { Subject } from 'rxjs';
import { User } from '../models/user';
import { DatabaseProvider } from '../providers/database';
import { AccountService } from '../services/account.service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) public nav: Nav;

  /** Main root page */
  private rootPage: string = 'LandingPageComponent';

  private pages: Array<{ title: string, component: any, leftIcon: string }>;

  private componentDestroyed$: Subject<boolean> = new Subject();

  constructor(public platform: Platform, private config: Config, statusBar: StatusBar,
    splashScreen: SplashScreen, public translate: TranslateService, private injector: Injector,
    private accSvc: AccountService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.config.set('ios', 'backButtonText', '');

      const db: DatabaseProvider = this.injector.get(DatabaseProvider);

      db.getDatabaseState()
        .first((ready) => ready)
        .subscribe((rdy) => {
          if (rdy) {
            accSvc.isloggedIn()
              .subscribe(pageResolver);
          }
        });

      const pageResolver = (authed) => {
        if (authed) {
          accSvc.getUserInfo()
            .subscribe((uInfo: User) => {
              // SETTING.API_ENDPOINT = uInfo.baseUrl;
              this.nav.setRoot('HomeComponent');
            });
        } else {
          this.nav.setRoot(this.rootPage);
        }
      };

      accSvc.authenticationNotifier()
        .takeUntil(this.componentDestroyed$)
        .subscribe(pageResolver);
    });

    // Set default translate language
    // Default language english
    // Language code 'en'
    translate.setDefaultLang('en');

    // Page navigation component
    this.pages = [
      { title: 'Home Page', component: 'HomeComponent', leftIcon: 'home' },
      { title: 'Sync Data', component: 'SyncPage', leftIcon: 'sync' },
      { title: 'Calendar', component: 'FullCalendarComponent', leftIcon: 'calendar' },
      { title: 'Customers', component: 'CustomersComponent', leftIcon: 'contacts' },
      { title: 'Visits', component: 'VisitsComponent', leftIcon: 'car' },
      { title: 'Actions', component: 'ActionsComponent', leftIcon: 'clipboard' },
      { title: 'Contacts', component: 'Customer_contactsComponent', leftIcon: 'call' },
      { title: 'Reports', component: 'ReportsPage', leftIcon: 'stats' }
    ];
  }

  public openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.push(page.component);
  }

  // Logout
  // logout(component) {
  //   this.nav.setRoot(component);
  // }
  public logout() {
    this.accSvc.setUserInfo(null);
  }
}
