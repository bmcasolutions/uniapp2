import { SQLiteObject } from '@ionic-native/sqlite';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { ApiGateway } from '../services/api.gateway.service';
import { BaseModel } from './baseModel';
import { Customer } from './customer';
import { IProductModel } from './cutomer.models';

export class CustomerProduct extends BaseModel {
    public id: number;
    public customerId: number;
    public productId: number;
    public supplierId: number;
    public productName: string;
    public isStockist: boolean;
    public supplierName: string;

    public insert(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`insert into customer_product(id,customerId,productId,supplierId,
            productName,isStockist,supplierName) values(?,?,?,?,?,?,?)`, [this.id, this.customerId, this.productId,
            this.supplierId, this.productName, +this.isStockist, this.supplierName]))
            .map((results) => !!results.rowsAffected);
    }

    public update(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`update customer_product
         set  customerId=?,productId=?,supplierId=?,productName=?,isStockist=?,
        supplierName=? where id=?`, [this.customerId, this.productId, this.supplierId,
            this.productName, +this.isStockist, this.supplierName, this.id]))
            .map((results) => !!results.rowsAffected);
    }

    public getById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from customer_product where id=?', [id]))
            .map((results) => Customer.convertToArray(results.rows)[0]);
    }
    public deleteById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('delete from customer_product where id=?', [id]))
            .map((results) => !!results.rowsAffected);
    }

    public getByCustomerId(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from customer_product where customerId=?', [id]))
            .map((results) => Customer.convertToArray(results.rows));
    }

    public static deleteAll(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('delete from customer_product', []))
            .map((results) => !!results.rowsAffected);
    }

    public static syncAllData(api: ApiGateway, db: SQLiteObject): Observable<[string, number]> {
        return api.post('/syncproductsall.asp', null, null)
            .pipe(map((res: any) => {
                if (!res || !res.CustomerProducts) {
                    return [];
                }
                return res.CustomerProducts
                    .map((datum) => {
                        const con = new CustomerProduct();
                        return Object.assign(con,
                            {
                                id: datum.CustomerProductID,
                                customerId: datum.CustomerID,
                                productId: datum.ProductID,
                                supplierId: datum.SupplierID,
                                productName: datum.ProductName,
                                isStockist: datum.IsStockist,
                                supplierName: datum.SupplierName
                            } as CustomerProduct);
                    });
            })).flatMap((custs: CustomerProduct[]) => {
                return Observable.from(custs)
                    .flatMap((datum) => datum.getById(db, datum.id)
                        .flatMap((d) => {
                            if (d) {
                                return datum.update(db);
                            } else {
                                return datum.insert(db);
                            }
                        }).flatMap(() => Observable.of(['customer_product', 1] as [string, number])));
            });
    }

    public static syncData(id: number, api: ApiGateway, db: SQLiteObject): Observable<[string, number]> {
        return api.post('/synccustomerproducts.asp', null, { cid: id })
            .pipe(map((res: any) => {
                if (!res || !res.CustomerProducts) {
                    return [];
                }
                return res.CustomerProducts
                    .map((datum) => {
                        const con = new CustomerProduct();
                        return Object.assign(con,
                            {
                                id: datum.CustomerProductID,
                                customerId: datum.CustomerID,
                                productId: datum.ProductID,
                                supplierId: datum.SupplierID,
                                productName: datum.ProductName,
                                isStockist: datum.IsStockist,
                                supplierName: datum.SupplierName
                            } as CustomerProduct);
                    });
            })).flatMap((custs: CustomerProduct[]) => {
                return Observable.from(custs)
                    .flatMap((datum) => datum.getById(db, datum.id)
                        .flatMap((d) => {
                            if (d) {
                                return datum.update(db);
                            } else {
                                return datum.insert(db);
                            }
                        }).flatMap(() => Observable.of(['customer_product', 1] as [string, number])));
            });
    }

    public editProduct(model: IProductModel, api: ApiGateway, db: SQLiteObject): Observable<boolean> {
        const data: any = {
            cid: model.customerId,
            CustomerProductID: model.id,
            IsStockist: +model.isStockist,
            SupplierID: model.supplierId,
            SupplierName: model.supplierName
        };
        return api.post('/edit_customerproduct.asp', null, data)
            .flatMap((res: any) => {
                return CustomerProduct.syncAllData(api, db).last().map(() => true);
            }).catch(() => Observable.of(false));
    }
}
