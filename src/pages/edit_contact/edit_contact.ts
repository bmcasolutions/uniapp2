/** Represents a Component of Google Map. */

/** Imports Modules */
import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
	selector: 'edit_contact',
	templateUrl: 'edit_contact.html'
})

export class Edit_contactComponent {

	constructor(public toastCtrl: ToastController, public navCtrl: NavController) {
		this.navCtrl = navCtrl;
	}

	public openPage(component) {
		this.navCtrl.setRoot(component);
	}
}
