import { Directive, ElementRef, HostListener, OnInit } from '@angular/core';

@Directive({
  selector: 'ion-textarea[autosize]'
})
export class Autosize implements OnInit {
  @HostListener('input', ['$event.target'])
  public onInput(): void {
    this.adjust();
  }

  constructor(private element: ElementRef) { }

  public ngOnInit(): void {
    setTimeout(() => this.adjust(), 0);
  }

  public adjust(): void {
    const textArea = this.element.nativeElement.getElementsByTagName('textarea')[0];
    textArea.style.overflow = 'hidden';
    textArea.style.height = 'auto';
    textArea.style.height = textArea.scrollHeight + 'px';
  }
}
