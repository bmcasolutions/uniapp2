import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { SharedModule } from '../../app/shared.module';
import { Visit_detailsComponent } from './visit_details';

@NgModule({
  declarations: [
    Visit_detailsComponent
  ],
  imports: [
    IonicPageModule.forChild(Visit_detailsComponent),
    TranslateModule,
    SharedModule
  ],
  exports: [
    Visit_detailsComponent,
    TranslateModule
  ]
})
export class Visit_detailsModule { }
