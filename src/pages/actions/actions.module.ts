import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { SharedModule } from '../../app/shared.module';
import { ActionsComponent } from './actions';

@NgModule({
  declarations: [
    ActionsComponent
  ],
  imports: [
    IonicPageModule.forChild(ActionsComponent),
    TranslateModule,
    SharedModule
  ],
  exports: [
    ActionsComponent,
    TranslateModule
  ]
})
export class ActionsModule { }
