import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {
  public transform(array: string[], args?: any): string[] {
    return array.sort((a, b) => {
      if (a[args.property] < b[args.property]) {
        return -1 * args.order;
      } else if (a[args.property] > b[args.property]) {
        return 1 * args.order;
      } else {
        return 0;
      }
    });
  }
}
