import { Injectable } from '@angular/core';
import { Toast, ToastController } from 'ionic-angular';

@Injectable()
export class ToastService {

    public toast: Toast;
    constructor(public toastCtrl: ToastController) { }

    public create(message, ok = false, duration = 2000) {
        if (this.toast) {
            this.toast.dismiss();
        }

        this.toast = this.toastCtrl.create({
            closeButtonText: 'OK',
            duration: ok ? null : duration,
            message,
            position: 'bottom',
            showCloseButton: ok
        });
        this.toast.present();
    }
}
