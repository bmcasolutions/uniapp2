import { SQLiteObject } from '@ionic-native/sqlite';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { ApiGateway } from '../services/api.gateway.service';
import { BaseModel } from './baseModel';
import { Customer } from './customer';

export class Objective extends BaseModel {
    public id: number;
    public name: string;

    public insert(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`insert into objectives(id,name)
        values(?,?)`, [this.id, this.name]))
            .map((results) => !!results.rowsAffected);
    }

    public update(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`update objectives set name=?
        where id=?`, [this.name, this.id]))
            .map((results) => !!results.rowsAffected);
    }
    public deleteById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('delete from objectives where id=?', [id]))
            .map((results) => !!results.rowsAffected);
    }

    public getById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from objectives where id=?', [id]))
            .map((results) => Objective.convertToArray(results.rows)[0]);
    }

    public static getAll(db: SQLiteObject): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from objectives order by name', []))
            .map((results) => Objective.convertToArray(results.rows));
    }

    public static deleteAll(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('delete from objectives', []))
            .map((results) => !!results.rowsAffected);
    }

    public static syncData(api: ApiGateway, db: SQLiteObject): Observable<[string, number]> {
        return api.post('/syncobjectives.asp', null, null)
            .pipe(map((res: any) => {
                if (!res || !res.Objectives) {
                    return [];
                }
                return res.Objectives
                    .map((datum) => {
                        const con = new Objective();
                        return Object.assign(con,
                            {
                                id: datum.ObjectiveID,
                                name: datum.ObjectiveName
                            } as Objective);
                    });
            })).flatMap((custs: Objective[]) => {
                return Observable.from(custs)
                    .flatMap((datum) => datum.getById(db, datum.id)
                        .flatMap((d) => {
                            if (d) {
                                return datum.update(db);
                            } else {
                                return datum.insert(db);
                            }
                        }).flatMap(() => Observable.of(['objective', 1] as [string, number])));
            });
    }
}
