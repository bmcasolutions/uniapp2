import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { ReassignVisitModal } from './reassign.visit';

@NgModule({
  declarations: [
    ReassignVisitModal
  ],
  imports: [
    IonicPageModule.forChild(ReassignVisitModal),
    TranslateModule
  ],
  exports: [
    ReassignVisitModal,
    TranslateModule
  ]
})
export class ReassignVisitModalModule { }
