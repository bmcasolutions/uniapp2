import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { Action_detailsComponent } from './action_details';

@NgModule({
  declarations: [
    Action_detailsComponent
  ],
  imports: [
    IonicPageModule.forChild(Action_detailsComponent),
    TranslateModule
  ],
  exports: [
    Action_detailsComponent,
    TranslateModule
  ]
})
export class Action_detailsModule {}
