import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController } from 'ionic-angular';
import { Customer } from '../../models/customer';
import { DatabaseProvider } from '../../providers/database';

@IonicPage()
@Component({
	selector: 'customers',
	templateUrl: 'customers.html'
})
export class CustomersComponent {

	public customers: any[] = [];
	private terms: string;
	private type: string = 'my';
	private typeId: string = '0';

	constructor(public toastCtrl: ToastController,
		public navCtrl: NavController,
		private dbSvc: DatabaseProvider, private modalCtrl: ModalController) {
		this.navCtrl = navCtrl;
	}

	public ionViewDidEnter() {
		this.loadCustomers();
	}

	public loadCustomers() {
		Customer.getAll(this.dbSvc.database)
			.subscribe((c) => {
				c = c.sort((a, b) => {
					if (a.name < b.name) {
						return -1;
					}
					if (a.name > b.name) {
						return 1;
					}
					return 0;
				});
				c.forEach((cust) => {
					cust.text = (cust.name || '').toUpperCase();
				});
				this.customers = c;
			});
	}

	private myCustomers(c: Customer) {
		return !c.isOther;
	}

	private otherCustomers(c: Customer) {
		return !!c.isOther;
	}

	private filterTypeId(c: Customer) {
		if (this.typeId === '0') {
			return true;
		}
		return '' + c.typeId === this.typeId;
	}

	public customerDetail(id: number) {
		this.navCtrl.push('Customer_detailsComponent', { id });
	}

	public addEditCustomer($event, cust) {
		$event.stopPropagation();
		cust = cust || { id: 0 };
		const modal = this.modalCtrl.create('CustomerFormPage', {
			id: cust.id

		});
		modal.onDidDismiss((dataChanged) => {
			if (dataChanged) { this.loadCustomers(); }
		});
		modal.present();
	}
}
