import { SQLiteObject } from '@ionic-native/sqlite';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { ApiGateway } from '../services/api.gateway.service';
import { BaseModel } from './baseModel';
import { Customer } from './customer';
import { ILocationModel } from './cutomer.models';

export class CustomerLocation extends BaseModel {
    public id: number;
    public customerId: number;
    public name: string;
    public address1: string;
    public address2: string;
    public address3: string;
    public address4: string;
    public postCode: string;
    public latitude: number;
    public longitude: number;
    public isDefault: boolean;
    public notes: string;

    public insert(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`insert into customer_locations(id,customerId,name,address1,
            address2,address3,address4,postCode,latitude,longitude,isDefault,notes) values(?,?,?,?,?,?,?,?,
            ?,?,?,?)`, [this.id, this.customerId, this.name, this.address1, this.address2, this.address3,
            this.address4, this.postCode, this.latitude, this.longitude, +this.isDefault, this.notes]))
            .map((results) => !!results.rowsAffected);
    }

    public update(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`update customer_locations set customerId=?,name=?,address1=?,
        address2=?,address3=?,address4=?,postCode=?,latitude=?,longitude=?,isDefault=?,notes=? where id=?`,
            [this.customerId, this.name, this.address1, this.address2, this.address3, this.address4, this.postCode,
            this.latitude, this.longitude, +this.isDefault, this.notes, this.id]))
            .map((results) => !!results.rowsAffected);
    }

    public getById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from customer_locations where id=?', [id]))
            .map((results) => CustomerLocation.convertToArray(results.rows)[0]);
    }

    public deleteById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('delete from customer_locations where id=?', [id]))
            .map((results) => !!results.rowsAffected);
    }

    public getByCustomerId(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from customer_locations where customerId=?', [id]))
            .map((results) => CustomerLocation.convertToArray(results.rows));
    }

    public static deleteAll(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('delete from customer_locations', []))
            .map((results) => !!results.rowsAffected);
    }

    public static syncAllData(api: ApiGateway, db: SQLiteObject): Observable<[string, number]> {
        return api.post('/synccustomersitesall.asp', null, null)
            .pipe(map((res: any) => {
                if (!res || !res.CustomerSites) {
                    return [];
                }
                return res.CustomerSites
                    .map((datum) => {
                        const con = new CustomerLocation();
                        return Object.assign(con,
                            {
                                id: datum.SiteID,
                                customerId: datum.CustomerID || 0,
                                name: datum.SiteName || '',
                                address1: datum.Address1 || '',
                                address2: datum.Address2 || '',
                                address3: datum.Address3 || '',
                                address4: datum.Address4 || '',
                                postCode: datum.PostCode || '',
                                latitude: datum.Latitude || 0,
                                longitude: datum.Longitude || 0,
                                isDefault: !!datum.IsDefault,
                                notes: datum.Notes || ''
                            } as CustomerLocation);
                    });
            })).flatMap((custs: CustomerLocation[]) => {
                return Observable.from(custs)
                    .flatMap((datum) => datum.getById(db, datum.id)
                        .flatMap((d) => {
                            if (d) {
                                return datum.update(db);
                            } else {
                                return datum.insert(db);
                            }
                        }).flatMap(() => Observable.of(['customer_location', 1] as [string, number])));
            });
    }

    public static syncData(id: number, api: ApiGateway, db: SQLiteObject): Observable<[string, number]> {
        return api.post('/synccustomersites.asp', null, { cid: id })
            .pipe(map((res: any) => {
                if (!res || !res.CustomerSites) {
                    return [];
                }
                return res.CustomerSites
                    .map((datum) => {
                        const con = new CustomerLocation();
                        return Object.assign(con,
                            {
                                id: datum.SiteID,
                                customerId: datum.CustomerID || 0,
                                name: datum.SiteName || '',
                                address1: datum.Address1 || '',
                                address2: datum.Address2 || '',
                                address3: datum.Address3 || '',
                                address4: datum.Address4 || '',
                                postCode: datum.PostCode || '',
                                latitude: datum.Latitude || 0,
                                longitude: datum.Longitude || 0,
                                isDefault: !!datum.IsDefault,
                                notes: datum.Notes || ''
                            } as CustomerLocation);
                    });
            })).flatMap((custs: CustomerLocation[]) => {
                return Observable.from(custs)
                    .flatMap((datum) => datum.getById(db, datum.id)
                        .flatMap((d) => {
                            if (d) {
                                return datum.update(db);
                            } else {
                                return datum.insert(db);
                            }
                        }).flatMap(() => Observable.of(['customer_location', 1] as [string, number])));
            });
    }

    public addEditLocation(model: ILocationModel, api: ApiGateway, db: SQLiteObject): Observable<boolean> {
        const data: any = {
            LocationName: model.name,
            Address1: model.address1,
            Address2: model.address2,
            Address3: model.address3,
            Address4: model.address4,
            PostCode: model.postCode,
            IsDefault: +model.isDefault,
            cid: model.customerId
        };
        if (model.id) {
            data.SiteID = model.id;
        }
        return api.post(model.id ? '/editlocation.asp' : '/addlocation.asp',
            null, data)
            .flatMap((res: any) => {
                return CustomerLocation.syncAllData(api, db).last().map(() => true);
            }).catch(() => Observable.of(false));
    }

    public static deleteByLocationId(model: any, api: ApiGateway, db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('delete from customer_locations where id=?', [model.id]))
            .flatMap((v) => {
                const data: any = {
                    SiteID: model.id,
                    cid: model.cid
                };
                if (model.id) {
                    data.cid = model.customerId;
                }
                return api.post('/deletelocation.asp',
                    null, data)
                    .flatMap((res: any) => {
                        return CustomerLocation.syncAllData(api, db).last().map(() => true);
                    }).catch(() => Observable.of(false));
            });
    }
}
