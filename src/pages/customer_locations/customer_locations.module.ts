import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { Customer_locationsComponent } from './customer_locations';

@NgModule({
	declarations: [
		Customer_locationsComponent
	],
	imports: [
		IonicPageModule.forChild(Customer_locationsComponent),
		TranslateModule
	],
	exports: [
		Customer_locationsComponent,
		TranslateModule
	]
})
export class Customer_locationsModule {}
