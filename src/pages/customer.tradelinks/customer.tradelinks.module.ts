import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { SharedModule } from '../../app/shared.module';
import { CustomerTradeLinksPage } from './customer.tradelinks';

@NgModule({
	declarations: [
		CustomerTradeLinksPage
	],
	imports: [
		IonicPageModule.forChild(CustomerTradeLinksPage),
		TranslateModule,
		SharedModule
	],
	exports: [
		CustomerTradeLinksPage,
		TranslateModule
	]
})
export class CustomerTradeLinksModule { }
