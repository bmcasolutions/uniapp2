import { SQLiteObject } from '@ionic-native/sqlite';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiGateway } from '../services/api.gateway.service';
import { BaseModel } from './baseModel';

export class SupplierProduct extends BaseModel {
    public id: number;
    public supplierId: number;
    public productId: number;

    public insert(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`insert into supplier_products(id,supplierId,productId)
        values(?,?,?)`, [this.id, this.supplierId, this.productId]))
            .map((results) => !!results.rowsAffected);
    }

    public update(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`update supplier_products set supplierId=?, productId=?
        where id=?`, [this.supplierId, this.productId, this.id]))
            .map((results) => !!results.rowsAffected);
    }

    public getById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from supplier_products where id=?', [id]))
            .map((results) => SupplierProduct.convertToArray(results.rows)[0]);
    }

    public static deleteAll(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('delete from supplier_products', []))
            .map((results) => !!results.rowsAffected);
    }

    public static syncData(api: ApiGateway, db: SQLiteObject): Observable<[string, number]> {
        return api.post('/syncproductsuppliers.asp', null, null)
            .pipe(map((res: any) => {
                if (!res || !res.ProductSuppliers) {
                    return [];
                }
                return res.ProductSuppliers
                    .map((datum) => {
                        const con = new SupplierProduct();
                        return Object.assign(con,
                            {
                                id: datum.PSID,
                                supplierId: datum.SupplierID,
                                productId: datum.ProductID
                            } as SupplierProduct);
                    });
            })).flatMap((prod: SupplierProduct[]) => {
                return Observable.from(prod)
                    .flatMap((datum) => datum.getById(db, datum.id)
                        .flatMap((d) => {
                            if (d) {
                                return datum.update(db);
                            } else {
                                return datum.insert(db);
                            }
                        }).flatMap(() => Observable.of(['supplier_products', 1] as [string, number])));
            });
    }
}
