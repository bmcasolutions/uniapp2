/** Represents a Component of landing page. */

/** Imports Modules */
import { Component } from '@angular/core';
import { IonicPage, MenuController, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'landing-page',
  templateUrl: 'landing-page.html'
})
export class LandingPageComponent {
  constructor(public navCtrl: NavController,
    private menu: MenuController) {
    this.menu.enable(false); // Disable sidemenu
  }

  public openPage(component) {
    this.navCtrl.setRoot(component);
  }
}
