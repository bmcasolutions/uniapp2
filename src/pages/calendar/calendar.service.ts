import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Action } from '../../models/action';
import { CustomerVisit } from '../../models/customerVisit';
import { DatabaseProvider } from '../../providers/database';

@Injectable()

export class CalendarService {
  constructor(private dbSvc: DatabaseProvider) { }

  public getAllEvents(): Observable<any> {
    return Observable.concat(CustomerVisit.getAll(this.dbSvc.database)
      // .pipe(map((visits: any) => {
      //   return visits.filter((v) => !v.complete);
      // }))
      .flatMap((visits) => {
        return Observable.from(visits
          .filter((v) => v.startDate && v.endDate)
          .map((v: CustomerVisit) => {
            return {
              id: v.id,
              title: v.contactName,
              start: new Date(v.startDate
                .replace(/(\d+)\/(\d+)\/(\d+)/, '$2/$1/$3')).toLocaleDateString('en-US'),
              end: new Date(v.endDate
                .replace(/(\d+)\/(\d+)\/(\d+)/, '$2/$1/$3')).toLocaleDateString('en-US'),
              color: '#110749',
              date: v.startDate,
              type: 'visit',
              complete: v.complete || v.isClosed
            };
          }));
      }),
      Action.getAll(this.dbSvc.database)
        // .pipe(map((actions: any) => {
        //   return actions.filter((a) => !a.complete);
        // }))
        .flatMap((actions) => {
          return Observable.from(actions.map((a: Action) => {
            return {
              id: a.id,
              title: a.name,
              start: new Date(a.dueDate
                .replace(/(\d+)\/(\d+)\/(\d+)/, '$2/$1/$3')).toLocaleDateString('en-US'),
              end: new Date(a.dueDate
                .replace(/(\d+)\/(\d+)\/(\d+)/, '$2/$1/$3')).toLocaleDateString('en-US'),
              color: '#241680',
              date: a.dueDate,
              type: 'action',
              complete: a.complete
            };
          }));
        })).toArray();

    // const date = new Date();
    // const d = date.getDate();
    // const m = date.getMonth();
    // const y = date.getFullYear();
    // return [{
    //   title: 'All Day Event',
    //   start: new Date(y, m, 1).toISOString()
    // }, {
    //   title: 'Long Event',
    //   start: new Date(y, m, d - 5).toISOString(),
    //   end: new Date(y, m, d - 2).toISOString()
    // }, {
    //   id: 999,
    //   title: 'Repeating Event',
    //   start: new Date(y, m, d - 3, 16, 0).toISOString(),
    //   allDay: false
    // }, {
    //   id: 999,
    //   title: 'Repeating Event',
    //   start: new Date(y, m, d + 4, 16, 0).toISOString(),
    //   allDay: false
    // }, {
    //   title: 'Meeting',
    //   start: new Date(y, m, d, 10, 30).toISOString(),
    //   allDay: false
    // }, {
    //   title: 'Lunch',
    //   start: new Date(y, m, d, 12, 5).toISOString(),
    //   end: new Date(y, m, d, 14, 43).toISOString(),
    //   allDay: false
    // }, {
    //   title: 'Birthday Party',
    //   start: new Date(y, m, d + 1, 19, 0).toISOString(),
    //   end: new Date(y, m, d + 1, 22, 30).toISOString(),
    //   allDay: false
    // }, {
    //   title: 'Click for Google',
    //   start: new Date(y, m, 28).toISOString(),
    //   end: new Date(y, m, 29).toISOString(),
    //   url: 'http://google.com/'
    // }];
  }
}
