import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { SharedModule } from '../../app/shared.module';
import { CustomersComponent } from './customers';

@NgModule({
  declarations: [
    CustomersComponent
  ],
  imports: [
    IonicPageModule.forChild(CustomersComponent),
    TranslateModule,
    SharedModule
  ],
  exports: [
    CustomersComponent,
    TranslateModule
  ]
})
export class CustomersModule { }
