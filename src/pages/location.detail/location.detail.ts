import { Component } from '@angular/core';
import {
	AlertController, IonicPage, LoadingController,
	ModalController, NavController, NavParams, ToastController
} from 'ionic-angular';
import { CustomerLocation } from '../../models/customerLocation';
import { DatabaseProvider } from '../../providers/database';
import { ApiGateway } from '../../services/api.gateway.service';

@IonicPage()
@Component({
	selector: 'location-detail',
	templateUrl: 'location.detail.html'
})
export class LocationDetailPage {

	private location: any = {};

	constructor(public toastCtrl: ToastController, private alertCtrl: AlertController,
		private loadingCtrl: LoadingController,
		public navCtrl: NavController,
		private dbSvc: DatabaseProvider,
		private api: ApiGateway,
		private param: NavParams, private modalCtrl: ModalController
	) {
		this.navCtrl = navCtrl;
	}

	public ionViewDidEnter() {
		this.loadContact();
	}

	public loadContact() {
		new CustomerLocation().getById(this.dbSvc.database, this.param.get('id'))
			.subscribe((c) => {
				this.location = c;
			});
	}

	public addEditLocation($event, loc) {
		$event.stopPropagation();
		loc = loc || { id: 0 };
		const modal = this.modalCtrl.create('LocationFormPage', {
			id: loc.id,
			cid: this.param.get('id')
		});
		modal.onDidDismiss((dataChanged) => {
			if (dataChanged) { this.loadContact(); }
		});
		modal.present();
	}
	public deleteLocation($event, location) {
		const confirm = this.alertCtrl.create({
			buttons: [
				{
					handler: () => {
						// console.log('Disagree clicked');
					},
					text: 'No'
				},
				{
					handler: () => {
						const loading = this.loadingCtrl.create({
							content: 'Please wait...'
						});
						loading.present();

						const v = new CustomerLocation();
						v.customerId = this.location.customerId;
						CustomerLocation.deleteByLocationId(this.location, this.api, this.dbSvc.database)
							.subscribe(() => {
								// tslint:disable-next-line:no-console
								loading.dismiss();
								this.navCtrl.pop();
								// this.customerLocations(this.location.customerId);
								// this.loadVisit();
							}, () => {
								// tslint:disable-next-line:no-console
								console.log(false);
								loading.dismiss();
							});

					},
					text: 'Yes'
				}
			],
			message: 'Are you sure you want to delete this location?',
			title: `Delete location ${this.location.id}?`
		});
		confirm.present();
	}

	public customerLocations(id: number) {
		this.navCtrl.push('LocationsComponent', { id });
	}
}
