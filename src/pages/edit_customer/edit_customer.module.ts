import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { Edit_customerComponent } from './edit_customer';

@NgModule({
  declarations: [
    Edit_customerComponent
  ],
  imports: [
    IonicPageModule.forChild(Edit_customerComponent),
    TranslateModule
  ],
  exports: [
    Edit_customerComponent,
    TranslateModule
  ]
})
export class Edit_customerModule { }
