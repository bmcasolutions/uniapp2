import { SQLiteObject } from '@ionic-native/sqlite';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { ApiGateway } from '../services/api.gateway.service';
import { BaseModel } from './baseModel';
import { Customer } from './customer';

export class Supplier extends BaseModel {
    public id: number;
    public name: string;

    public insert(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`insert into suppliers(id,name)
        values(?,?)`, [this.id, this.name]))
            .map((results) => !!results.rowsAffected);
    }

    public update(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`update suppliers set name=?
        where id=?`, [this.name, this.id]))
            .map((results) => !!results.rowsAffected);
    }

    public getById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from suppliers where id=?', [id]))
            .map((results) => Customer.convertToArray(results.rows)[0]);
    }

    public static getAll(db: SQLiteObject): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from suppliers', []))
            .map((results) => Customer.convertToArray(results.rows));
    }

    public static getByProductId(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql(`select s.* from suppliers s, supplier_products sp
            where s.id=sp.supplierId and sp.productId=?`, [id]))
            .map((results) => Customer.convertToArray(results.rows));
    }

    public static deleteAll(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('delete from suppliers', []))
            .map((results) => !!results.rowsAffected);
    }

    public static syncData(api: ApiGateway, db: SQLiteObject): Observable<[string, number]> {
        return api.post('/synccustomersuppliers.asp', null, null)
            .pipe(map((res: any) => {
                if (!res || !res.CustomerSuppliers) {
                    return [];
                }
                return res.CustomerSuppliers
                    .map((datum) => {
                        const con = new Supplier();
                        return Object.assign(con,
                            {
                                id: datum.SupplierID,
                                name: datum.SupplierName
                            } as Supplier);
                    });
            })).flatMap((custs: Supplier[]) => {
                return Observable.from(custs)
                    .flatMap((datum) => datum.getById(db, datum.id)
                        .flatMap((d) => {
                            if (d) {
                                return datum.update(db);
                            } else {
                                return datum.insert(db);
                            }
                        }).flatMap(() => Observable.of(['suppliers', 1] as [string, number])));
            });
    }
}
