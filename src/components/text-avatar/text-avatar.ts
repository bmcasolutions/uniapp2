import { Directive, ElementRef, Input, SimpleChanges } from '@angular/core';
import { ColorGenerator } from './color-generator';

@Directive({
  providers: [ColorGenerator],
  selector: 'text-avatar'
})
export class TextAvatarDirective {

  constructor(private element: ElementRef, private colorGenerator: ColorGenerator) { }

  @Input() public color: string;
  @Input() public text: string;
  @Input() public textColor: string;

  public ngOnChanges(changes: SimpleChanges) {
    const text = changes.text ? changes.text.currentValue : null;
    const color = changes.color ? changes.color.currentValue : null;
    const textColor = changes.textColor ? changes.textColor.currentValue : '';

    this.element.nativeElement.setAttribute('value', this.extractFirstCharacter(text));
    this.element.nativeElement.style.backgroundColor = this.backgroundColorHexString(color, text);
    this.element.nativeElement.style.color = textColor ? textColor : '';
  }

  private extractFirstCharacter(text: string): string {
    return text.charAt(0).toUpperCase() || '';
  }

  private backgroundColorHexString(color: string, text: string): string {
    return color || this.colorGenerator.getColor(text);
  }
}
