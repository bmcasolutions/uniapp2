export class BaseModel {
    public static convertToArray = (arr): any[] => {
        const returnArray: any[] = [];
        for (let i = 0; i < arr.length; i++) {
            returnArray.push(arr.item(i));
        }
        return returnArray;
    }
}
