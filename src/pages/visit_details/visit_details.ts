import { ChangeDetectorRef, Component } from '@angular/core';
import {
	AlertController, IonicPage, LoadingController, ModalController, NavController,
	NavParams,
	ToastController
} from 'ionic-angular';
import { Action } from '../../models/action';
import { CustomerVisit } from '../../models/customerVisit';
import { User } from '../../models/user';
import { DatabaseProvider } from '../../providers/database';
import { AccountService } from '../../services/account.service';
import { ApiGateway } from '../../services/api.gateway.service';

@IonicPage()
@Component({
	selector: 'visit_details',
	templateUrl: 'visit_details.html'
})

export class Visit_detailsComponent {

	private visit: any = { actions: [] };
	private segment: string = 'details';
	private actionSegment: string = 'my';
	private currentUser: User;

	constructor(public toastCtrl: ToastController,
		private alertCtrl: AlertController,
		private loadingCtrl: LoadingController,
		public navCtrl: NavController,
		private dbSvc: DatabaseProvider,
		private api: ApiGateway,
		private modalCtrl: ModalController,
		private param: NavParams,
		private accSvc: AccountService,
		private cf: ChangeDetectorRef) {
		this.navCtrl = navCtrl;
	}

	public segmentChanged() {
		this.cf.detectChanges();
	}

	public ionViewDidEnter() {
		this.accSvc.getUserInfo()
			.subscribe((u) => {
				this.currentUser = u;
				this.loadVisit();
			});
	}

	public loadVisit() {
		new CustomerVisit().getById(this.dbSvc.database, this.param.get('id'))
			.subscribe((c) => {
				this.visit = c;
				new Action().getByVisitId(this.dbSvc.database, this.visit.id)
					.subscribe((ac) => {
						ac.forEach((a) => {
							a.visit = c;
							a.text = a.name;
						});
						this.visit.actions = ac;
					});
			});
	}

	private myActions(ac: Action) {
		return ac.assignedTo === this.currentUser.id;
	}

	private otherActions(ac: Action) {
		return ac.assignedTo !== this.currentUser.id;
	}

	public addEditAction($event, ac) {
		$event.stopPropagation();
		ac = ac || { id: 0 };
		const modal = this.modalCtrl.create('ActionFormPage', {
			id: ac.id,
			cId: this.visit.customerId || 0,
			vId: this.visit.id
		});
		modal.onDidDismiss((dataChanged) => {
			if (dataChanged) { this.loadVisit(); }
		});
		modal.present();
	}

	public addEditVisit($event, visit) {
		$event.stopPropagation();
		visit = visit || { id: 0 };
		const modal = this.modalCtrl.create('VisitFormPage', {
			id: visit.id
		});
		modal.onDidDismiss((dataChanged) => {
			if (dataChanged) { this.loadVisit(); }
		});
		modal.present();
	}

	public reassignVisit($event, visit) {
		const modal = this.modalCtrl.create('ReassignVisitModal', {
			vid: visit.id
		});
		modal.present();
	}

	public deleteVisit($event, visit) {
		const confirm = this.alertCtrl.create({
			buttons: [
				{
					handler: () => {
						// console.log('Disagree clicked');
					},
					text: 'No'
				},
				{
					handler: () => {
						const loading = this.loadingCtrl.create({
							content: 'Please wait...'
						});
						loading.present();

						const v = new CustomerVisit();
						v.customerId = this.visit.customerId;
						CustomerVisit.deleteByVisitId(this.visit, this.api, this.dbSvc.database)
							.subscribe(() => {
								loading.dismiss();
								this.navCtrl.pop();
							}, () => {
								loading.dismiss();
							});

					},
					text: 'Yes'
				}
			],
			message: 'Are you sure you want to delete this visit?',
			title: `Delete visit ${this.visit.customerName} on ${this.visit.startDate}?`
		});
		confirm.present();
	}

	public checkInVisit() {
		const confirm = this.alertCtrl.create({
			buttons: [
				{
					handler: () => {
						// console.log('Disagree clicked');
					},
					text: 'No'
				},
				{
					handler: () => {
						const loading = this.loadingCtrl.create({
							content: 'Please wait...'
						});
						loading.present();
						// this.geolocation.getCurrentPosition().then((resp) => {
						const date = new Date();
						const v = new CustomerVisit();
						v.customerId = this.visit.customerId;
						v.checkInVisit(this.dbSvc.database, this.visit.id, 0,
							0, +date, this.api)
							.subscribe(() => {
								loading.dismiss();
								this.loadVisit();
							}, () => {
								loading.dismiss();
							});
						// }).catch(() => {
						// 	loading.dismiss();
						// });
					},
					text: 'Yes'
				}
			],
			message: 'Are you sure you want to check into this visit?',
			title: `${this.visit.customerName} on ${this.visit.startDate}`
		});
		confirm.present();
	}

	public openJoCompletionPage(visit) {
		const modal = this.modalCtrl.create('VisitCompletionPage', { id: visit.id });
		modal.onDidDismiss((data) => {
			if (data) { this.loadVisit(); }
		});
		modal.present();
	}

	public actionDetail(action) {
		this.navCtrl.push('Action_detailsComponent', { id: action.id });
	}

	public openPage(component) {
		this.navCtrl.push(component);
	}
}
