import { NgModule } from '@angular/core';
import { CalendarComponent } from 'ap-angular2-fullcalendar/src/calendar/calendar';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { FullCalendarComponent } from './calendar';

@NgModule({
  declarations: [
    CalendarComponent,
    FullCalendarComponent
  ],
  imports: [
    IonicPageModule.forChild(FullCalendarComponent),
    TranslateModule
  ],
  exports: [
    FullCalendarComponent,
    TranslateModule
  ]
})
export class CalendarModule { }
