import { Component } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { IonicPage, LoadingController, NavParams, ViewController } from 'ionic-angular';
import { CustomerProduct } from '../../models/customerProduct';
import { IProductModel } from '../../models/cutomer.models';
import { Supplier } from '../../models/supplier';
import { DatabaseProvider } from '../../providers/database';
import { AccountService } from '../../services/account.service';
import { ApiGateway } from '../../services/api.gateway.service';

@IonicPage()
@Component({
    selector: 'product-form',
    templateUrl: 'product.form.html'
})
export class ProductFormPage {
    private _db: SQLiteObject;
    private _prod: IProductModel = {
        id: 0,
        customerId: +this.params.get('cid'),
        name: '',
        productId: 0,
        isStockist: true,
        supplierName: ''
    } as IProductModel;

    private _suppliers: any[] = [];

    constructor(public loadingCtrl: LoadingController,
        public params: NavParams, public viewCtrl: ViewController, db: DatabaseProvider,
        private api: ApiGateway,
        private accSvc: AccountService) {
        this._db = db.database;

        if (this.params.get('id') > 0) {
            new CustomerProduct().getById(this._db, this.params.get('id'))
                .subscribe((p: CustomerProduct) => {
                    Supplier.getByProductId(this._db, p.productId)
                        .subscribe((suppliers) => {
                            this._suppliers = suppliers;
                        });
                    this._prod = {
                        id: p.id,
                        customerId: +this.params.get('cid'),
                        name: p.productName,
                        productId: 0,
                        supplierId: p.supplierId,
                        isStockist: p.isStockist,
                        supplierName: p.supplierName
                    } as IProductModel;
                });
        }
    }

    public save() {
        const loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.accSvc.getUserInfo()
            .flatMap(() => {
                return new CustomerProduct().editProduct(this._prod, this.api, this._db);
            }).subscribe((success) => {
                loading.dismiss();
                if (success) {
                    this.dismiss(true);
                }
            }, () => {
                loading.dismiss();
            });
    }

    public dismiss(certChanged: boolean) {
        this.viewCtrl.dismiss(!!certChanged);
    }
}
