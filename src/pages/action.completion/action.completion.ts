import { Component } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { Events, IonicPage, LoadingController, NavParams, ViewController } from 'ionic-angular';
import { Action } from '../../models/action';
import { DatabaseProvider } from '../../providers/database';
import { AccountService } from '../../services/account.service';
import { ApiGateway } from '../../services/api.gateway.service';

@IonicPage()
@Component({
    selector: 'action-completion',
    templateUrl: 'action.completion.html'
})
export class ActionCompletionPage {
    private _db: SQLiteObject;
    private _ac: any = {};

    constructor(public loadingCtrl: LoadingController,
        public params: NavParams, public viewCtrl: ViewController, db: DatabaseProvider,
        private api: ApiGateway,
        private accSvc: AccountService) {
        this._db = db.database;
        if (this.params.get('id') > 0) {
            new Action().getById(this._db, this.params.get('id'))
                .subscribe((ac: Action) => {
                    this._ac = ac;
                });
        }
    }

    public complete() {
        const loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.accSvc.getUserInfo()
            .flatMap((u) => {
                return new Action().completeAction(this._db, this._ac.id, this._ac.comments, this.api);
            }).subscribe((success) => {
                loading.dismiss();
                if (success) {
                    this.dismiss(true);
                }
            }, (err) => {
                loading.dismiss();
            });
    }

    public dismiss(certChanged: boolean) {
        this.viewCtrl.dismiss(!!certChanged);
    }
}
