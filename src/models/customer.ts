import { SQLiteObject } from '@ionic-native/sqlite';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { AccountService } from '../services/account.service';
import { ApiGateway } from '../services/api.gateway.service';
import { BaseModel } from './baseModel';
import { ICustomerAddModel, ICustomerEditModel } from './customer.models';
import { CustomerContact } from './customerContact';
import { CustomerLocation } from './customerLocation';
import { CustomerProduct } from './customerProduct';
import { User } from './user';

export class Customer extends BaseModel {
    public id: number;
    public refNo: string;
    public name: string;
    public address1: string;
    public address2: string;
    public address3: string;
    public address4: string;
    public postCode: string;
    public telephone: string;
    public type: string;
    public notes: string;
    public contactName: string;
    public contactPhone: string;
    public contactEmail: string;
    public countryId: number;
    public url: string;
    public active: boolean;
    public nextContactOn: string;
    public lastContactOn: string;
    public billingEmail1: string;
    public billingEmail2: string;
    public financeCode: string;
    public vatNo: string;
    public country: string;
    public creditlimit: number;
    public typeId: number;
    public keyRankId: number;
    public isOther: boolean;
    public accountId: number;
    public depotId: number;
    public contactId: number;
    public siteId: number;

    public static _syncTime: string;

    public insert(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`insert into customers(id,refNo,name,address1,address2,address3,
            address4,postCode,telephone,type,notes,contactName,contactPhone,contactEmail,countryId,url,active,
            nextContactOn,lastContactOn,billingEmail1,billingEmail2,financeCode,vatNo,country,creditlimit,
            typeId,keyRankId,isOther,accountId,depotId,contactId,siteId)
            values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`, [this.id, this.refNo, this.name,
            this.address1, this.address2, this.address3, this.address4, this.postCode, this.telephone, this.type,
            this.notes, this.contactName, this.contactPhone, this.contactEmail, this.countryId, this.url, this.active,
            this.nextContactOn, this.lastContactOn, this.billingEmail1, this.billingEmail2, this.financeCode,
            this.vatNo, this.country, this.creditlimit, this.typeId, this.keyRankId, +this.isOther, this.accountId,
            this.depotId, this.contactId, this.siteId]))
            .map((results) => !!results.rowsAffected);
    }

    public update(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql(`update customers set refNo=?,name=?,address1=?,address2=?,
        address3=?,address4=?,postCode=?,telephone=?,type=?,notes=?,contactName=?,contactPhone=?,contactEmail=?,
        countryId=?,url=?,active=?,nextContactOn=?,lastContactOn=?,billingEmail1=?,billingEmail2=?,financeCode=?,
        vatNo=?,country=?,creditlimit=?,typeId=?,keyRankId=?,isOther=?,accountId=?,depotId=?,contactId=?,siteId=?
        where id=?`, [this.refNo,
            this.name,
            this.address1, this.address2, this.address3,
            this.address4, this.postCode, this.telephone, this.type, this.notes, this.contactName, this.contactPhone,
            this.contactEmail, this.countryId, this.url, this.active, this.nextContactOn, this.lastContactOn,
            this.billingEmail1, this.billingEmail2, this.financeCode, this.vatNo, this.country, this.creditlimit,
            this.typeId, this.keyRankId, +this.isOther, this.accountId, this.depotId, this.contactId, this.siteId,
            this.id]))
            .map((results) => !!results.rowsAffected);
    }

    public getById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from customers where id=?', [id]))
            .map((results) => Customer.convertToArray(results.rows)[0]);
    }

    public deleteById(db: SQLiteObject, id: number): Observable<any> {
        return Observable.fromPromise(db.executeSql('delete from customers where id=?', [id]))
            .map((results) => !!results.rowsAffected);
    }

    public static getAll(db: SQLiteObject): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from customers order by name', []))
            .map((results) => Customer.convertToArray(results.rows));
    }

    public static getMyCustomers(db: SQLiteObject): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from customers where isOther=0', []))
            .map((results) => Customer.convertToArray(results.rows));
    }

    public static getOtherCustomers(db: SQLiteObject): Observable<any> {
        return Observable.fromPromise(db.executeSql('select * from customers where isOther=1', []))
            .map((results) => Customer.convertToArray(results.rows));
    }

    public static deleteAll(db: SQLiteObject): Observable<boolean> {
        return Observable.fromPromise(db.executeSql('delete from customers', []))
            .map((results) => !!results.rowsAffected);
    }

    public static syncAllData(api: ApiGateway, db: SQLiteObject, accSvc: AccountService): Observable<[string, number]> {
        return this.allCustomers(api)
            // .merge(this.otherCustomers(api))
            .flatMap((datum: Customer) => {
                return User.updateSyncTime(db, this._syncTime.replace(/(\d+)-(\d+)-(\d+)/, '$3/$2/$1'))
                    .flatMap(() => {
                        accSvc.unloadCachedUser();
                        return Observable.of(datum);
                    });
            })
            .flatMap((datum: Customer) => {
                return datum.getById(db, datum.id)
                    .flatMap((d) => {
                        if (d) {
                            return datum.update(db);
                        } else {
                            return datum.insert(db);
                        }
                    }).flatMap(() => Observable.of(['customer', 1] as [string, number]));
            });
    }

    // public static syncData(api: ApiGateway, db: SQLiteObject, accSvc: AccountService): Observable<[string, number]> {
    //     return this.allCustomers(api)
    //         // .merge(this.otherCustomers(api))
    //         .flatMap((datum: Customer) => {
    //             return User.updateSyncTime(db, this._syncTime.replace(/(\d+)-(\d+)-(\d+)/, '$3/$2/$1'))
    //                 .flatMap(() => {
    //                     accSvc.unloadCachedUser();
    //                     return Observable.of(datum);
    //                 });
    //         })
    //         .flatMap((datum: Customer) => {
    //             return datum.getById(db, datum.id)
    //                 .flatMap((d) => {
    //                     if (d) {
    //                         return datum.update(db);
    //                     } else {
    //                         return datum.insert(db);
    //                     }
    //                 }).flatMap(() => Observable.of(['customer', 1] as [string, number]));
    //         });
    // }

    public static allCustomers(api: ApiGateway): Observable<Customer> {
        const time = new Date();
        this._syncTime = time.getFullYear() + '-' + ('0' + (time.getMonth() + 1)).slice(-2)
            + '-' + ('0' + time.getDate()).slice(-2) + ' ' + ('0' + time.getHours()).slice(-2)
            + ':' + ('0' + time.getMinutes()).slice(-2)
            + ':' + ('0' + time.getSeconds()).slice(-2);
        return api.post('/synccustomersall.asp', null, { LastSync: this._syncTime })
            .pipe(map((res: any) => {
                if (!res || !res.Customers) {
                    return [];
                }
                return res.Customers
                    .map((datum) => {
                        const cust = new Customer();
                        return Object.assign(cust,
                            {
                                id: datum.CustomerID || 0,
                                refNo: datum.CustomerRef || '',
                                address1: datum.Address1 || '',
                                address2: datum.Address2 || '',
                                address3: datum.Address3 || '',
                                address4: datum.Address4 || '',
                                name: datum.CustomerName || '',
                                postCode: datum.PostCode || '',
                                contactName: datum.ContactName || '',
                                contactPhone: datum.ContactPhone || '',
                                contactEmail: datum.ContactEmail || '',
                                countryId: datum.CountryID || 0,
                                url: datum.URL || '',
                                active: datum.IsActive || '',
                                nextContactOn: datum.NextContactOn || '',
                                lastContactOn: datum.LastContactOn || '',
                                billingEmail1: datum.BillingEmail1 || '',
                                billingEmail2: datum.BillingEmail2 || '',
                                financeCode: datum.FinanceCode || '',
                                vatNo: datum.VATNumber || '',
                                country: datum.Country || '',
                                telephone: datum.Telephone2 || '',
                                type: datum.Type || '',
                                notes: datum.Notes || '',
                                creditlimit: datum.CreditLimit || '',
                                typeId: datum.TypeID || 0,
                                keyRankId: datum.KeyRank || 0,
                                isOther: false,
                                accountId: datum.AccountID,
                                depotId: datum.DepotID,
                                contactId: datum.ContactID,
                                siteId: datum.SiteID
                            } as Customer);
                    });
            })).flatMap((custs: Customer[]) => Observable.from(custs));
    }

    // public static ownCustomers(api: ApiGateway): Observable<Customer> {
    //     const time = new Date();
    //     this._syncTime = time.getFullYear() + '-' + ('0' + (time.getMonth() + 1)).slice(-2)
    //         + '-' + ('0' + time.getDate()).slice(-2) + ' ' + ('0' + time.getHours()).slice(-2)
    //         + ':' + ('0' + time.getMinutes()).slice(-2)
    //         + ':' + ('0' + time.getSeconds()).slice(-2);
    //     return api.post('/synccustomers.asp', null, { LastSync: this._syncTime })
    //         .pipe(map((res: any) => {
    //             if (!res || !res.Customers) {
    //                 return [];
    //             }
    //             return res.Customers
    //                 .map((datum) => {
    //                     const cust = new Customer();
    //                     return Object.assign(cust,
    //                         {
    //                             id: datum.CustomerID || 0,
    //                             refNo: datum.CustomerRef || '',
    //                             address1: datum.Address1 || '',
    //                             address2: datum.Address2 || '',
    //                             address3: datum.Address3 || '',
    //                             address4: datum.Address4 || '',
    //                             name: datum.CustomerName || '',
    //                             postCode: datum.PostCode || '',
    //                             contactName: datum.ContactName || '',
    //                             contactPhone: datum.ContactPhone || '',
    //                             contactEmail: datum.ContactEmail || '',
    //                             countryId: datum.CountryID || 0,
    //                             url: datum.URL || '',
    //                             active: datum.IsActive || '',
    //                             nextContactOn: datum.NextContactOn || '',
    //                             lastContactOn: datum.LastContactOn || '',
    //                             billingEmail1: datum.BillingEmail1 || '',
    //                             billingEmail2: datum.BillingEmail2 || '',
    //                             financeCode: datum.FinanceCode || '',
    //                             vatNo: datum.VATNumber || '',
    //                             country: datum.Country || '',
    //                             telephone: datum.Telephone2 || '',
    //                             type: datum.Type || '',
    //                             notes: datum.Notes || '',
    //                             creditlimit: datum.CreditLimit || '',
    //                             typeId: datum.TypeID || 0,
    //                             keyRankId: datum.KeyRank || 0,
    //                             isOther: false

    //                         } as Customer);
    //                 });
    //         })).flatMap((custs: Customer[]) => Observable.from(custs));
    // }

    // public static otherCustomers(api: ApiGateway): Observable<Customer> {
    //     return api.post('/syncothercustomers.asp', null, null)
    //         .pipe(map((res: any) => {
    //             if (!res || !res.Customers) {
    //                 return [];
    //             }
    //             return res.Customers
    //                 .map((datum) => {
    //                     const cust = new Customer();
    //                     return Object.assign(cust,
    //                         {
    //                             id: datum.CustomerID || 0,
    //                             refNo: datum.CustomerRef || '',
    //                             address1: datum.Address1 || '',
    //                             address2: datum.Address2 || '',
    //                             address3: datum.Address3 || '',
    //                             address4: datum.Address4 || '',
    //                             name: datum.CustomerName || '',
    //                             postCode: datum.PostCode || '',
    //                             contactName: datum.ContactName || '',
    //                             contactPhone: datum.ContactPhone || '',
    //                             contactEmail: datum.ContactEmail || '',
    //                             countryId: datum.CountryID || 0,
    //                             url: datum.URL || '',
    //                             active: datum.IsActive || false,
    //                             nextContactOn: datum.NextContactOn || '',
    //                             lastContactOn: datum.LastContactOn || '',
    //                             billingEmail1: datum.BillingEmail1 || '',
    //                             billingEmail2: datum.BillingEmail2 || '',
    //                             financeCode: datum.FinanceCode || '',
    //                             vatNo: datum.VATNumber || '',
    //                             country: datum.Country || '',
    //                             telephone: datum.Telephone2 || '',
    //                             type: datum.Type || '',
    //                             notes: datum.Notes || '',
    //                             creditlimit: datum.CreditLimit || 0,
    //                             typeId: 0,
    //                             keyRankId: 0,
    //                             isOther: true

    //                         } as Customer);
    //                 });
    //         })).flatMap((custs: Customer[]) => Observable.from(custs));
    // }

    public addCustomer(model: ICustomerAddModel, api: ApiGateway, db: SQLiteObject,
        accSvc: AccountService): Observable<boolean> {
        const data: any = {
            CustomerName: model.name,
            TypeID: model.typeId,
            KeyRank: model.keyRankId,
            Address1: model.address1,
            Address2: model.address2,
            Address3: model.address3,
            Address4: model.address4,
            PostCode: model.postCode,
            ContactName: model.contactName,
            ContactPhone: model.contactPhone,
            ContactEmail: model.contactEmail,
            PositionHeld: model.positionHeld
        };
        return api.post('/addcustomer.asp', null, data)
            .flatMap((res: any) => {
                return Customer.syncAllData(api, db, accSvc).last()
                    .flatMap(() => CustomerContact.syncData(res.cid, api, db))
                    .last()
                    .flatMap(() => CustomerLocation.syncData(res.cid, api, db))
                    .last()
                    .flatMap(() => CustomerProduct.syncData(res.cid, api, db))
                    .last()
                    .map(() => true);
            }).catch(() => Observable.of(false));
    }

    public editCustomer(model: ICustomerEditModel, api: ApiGateway, db: SQLiteObject,
        accSvc: AccountService): Observable<boolean> {
        const data: any = {
            cid: model.id,
            CustomerName: model.name,
            TypeID: model.typeId,
            KeyRank: model.keyRankId,
            SiteID: model.siteId,
            ContactID: model.contactId,
            Notes: model.notes
        };
        return api.post('/editcustomer.asp', null, data)
            .flatMap((res: any) => {
                return Customer.syncAllData(api, db, accSvc).last().map(() => true);
            }).catch(() => Observable.of(false));
    }
}
