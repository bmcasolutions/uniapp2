/** Represents a Component of calendar page. */

/** Imports Modules */
import { AfterViewInit, Component, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { AlertController, IonicPage } from 'ionic-angular';
import * as $ from 'jquery';

/** Imports calendar service */
import { CalendarComponent } from 'ap-angular2-fullcalendar/src/calendar/calendar';
import { NavController } from 'ionic-angular/navigation/nav-controller';
import { CalendarService } from './calendar.service';

@IonicPage()
@Component({
  selector: 'calendar',
  templateUrl: 'calendar.html',
  providers: [CalendarService]
})

export class FullCalendarComponent implements AfterViewInit {

  /** Define Veriables */
  public visits: any[] = [];
  public actions: any[] = [];
  public calendarEvents: any[] = [];
  public segment: any;

  @ViewChild('mycal')
  private calendar: CalendarComponent;

  public calendarOptions: any = {};

  constructor(private calendarService: CalendarService,
    public alertCtrl: AlertController,
    private navCtrl: NavController) {
    this.alertCtrl = alertCtrl;

    this.calendarService.getAllEvents()
      .subscribe((events) => {
        this.visits = events.filter((e) => e.type === 'visit' && !e.complete)
          .sort((a, b) => +new Date(b.start) - +new Date(a.start));
        this.actions = events.filter((e) => e.type === 'action' && !e.complete)
          .sort((a, b) => +new Date(b.start) - +new Date(a.start));
        this.calendarOptions.events = events;
        this.calendar.fullCalendar('addEventSource', events);
        this.calendar.fullCalendar('refetchEvents');
      });
    this.segment = 'calendarView';
  }

  public ionViewDidEnter() {
    this.calendarService.getAllEvents()
      .subscribe((events) => {
        this.calendarEvents = events;
        this.visits = events.filter((e) => e.type === 'visit' && !e.complete)
          .sort((a, b) => +new Date(b.start) - +new Date(a.start));
        this.actions = events.filter((e) => e.type === 'action' && !e.complete)
          .sort((a, b) => +new Date(b.start) - +new Date(a.start));
        this.calendarOptions.events = this.calendarEvents;
        // this.calendar.fullCalendar('addEventSource', this.calendarEvents);
        // this.calendar.fullCalendar('refetchEvents');
      });
  }

  public ngAfterViewInit() {
    this.calendarOptions = {
      fixedWeekCount: false,
      editable: true,
      height: 480,
      aspectRatio: 2,
      displayEventTime: false,
      eventColor: '#1e115f',
      header: {
        left: 'prev',
        center: 'title',
        right: 'next'
      },
      footer: {
        center: 'month,basicWeek,basicDay,list'
      },
      buttonText: {
        today: 'Today',
        month: 'Month',
        week: 'Week',
        day: 'Day',
        list: 'List'
      },
      eventLimit: true,
      events: this.visits,
      dayClick() {
        this.css('background-color', '#f9d423');
      },
      eventClick: (calEvent, jsEvent, view) => {
        if (calEvent.type === 'visit') {
          return this.visitDetail(calEvent.id);
          // this.navCtrl.push('Visit_detailsComponent', { id: calEvent.id });
        } else {
          return this.actionDetail(calEvent.id);
          // this.navCtrl.push('Action_detailsComponent', { id: calEvent.id });
        }
      }
    };
  }

  public visitDetail(id: number) {
    this.navCtrl.push('Visit_detailsComponent', { id });
  }

  public actionDetail(id: number) {
    this.navCtrl.push('Action_detailsComponent', { id });
  }

  /**
   * Add new event function
   * This function open a alert box controller
   * The alert box controller contains three inputs
   * @input   title   Title of the event
   * @input   date    Start Date of the event
   * @input   date    End Date of the event
   * @button  Cancel
   * @button  Save
   */
  // public addNewEvent() {
  //   const prompt = this.alertCtrl.create({
  //     title: 'New Event',
  //     inputs: [{
  //       name: 'title',
  //       placeholder: 'Title',
  //       type: 'text',
  //       id: 'title'
  //     }, {
  //       name: 'start',
  //       placeholder: 'Start Date',
  //       type: 'date'
  //     }, {
  //       name: 'end',
  //       placeholder: 'End Date',
  //       type: 'date'
  //     }],
  //     buttons: [{
  //       text: 'Cancel',
  //       handler: (data) => {
  //         // console.log('Cancel clicked');
  //       }
  //     }, {
  //       text: 'Save',
  //       handler: (data) => {
  //         this.allEvents.push(data);
  //       }
  //     }]
  //   });
  //   prompt.present();
  // }
}
