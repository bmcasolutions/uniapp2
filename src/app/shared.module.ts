import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { COMPONENTS, DIRECTIVES, PIPES } from './app.imports';

@NgModule({
  declarations: [
    PIPES,
    DIRECTIVES,
    COMPONENTS
  ],
  exports: [
    PIPES,
    COMPONENTS,
    DIRECTIVES
  ],
  imports: [
    IonicModule
  ]
})

export class SharedModule { }
