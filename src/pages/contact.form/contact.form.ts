
import { Component } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { Events, IonicPage, LoadingController, NavParams, ViewController } from 'ionic-angular';
import { Customer } from '../../models/customer';
import { CustomerContact } from '../../models/customerContact';
import { CustomerLocation } from '../../models/customerLocation';
import { DatabaseProvider } from '../../providers/database';
import { AccountService } from '../../services/account.service';
import { ApiGateway } from '../../services/api.gateway.service';

@IonicPage()
@Component({
    selector: 'contact-form',
    templateUrl: 'contact.form.html'
})
export class ContactFormPage {
    private _db: SQLiteObject;
    private contact: any = {};
    private sites: any = [];
    public custs: any[] = [];

    private positions = ['Director',
        'Regional Manager',
        'Branch Manager',
        'Account Manager',
        'Technical Manager',
        'Internal Sales',
        'Contract Manager',
        'Project Manager',
        'Engineer',
        'Estimator',
        'Buyer',
        'Supervisor',
        'Sales Representative',
        'Other'];

    constructor(public loadingCtrl: LoadingController,
        public params: NavParams, public viewCtrl: ViewController, db: DatabaseProvider,
        private api: ApiGateway,
        private accSvc: AccountService) {
        this._db = db.database;

        if (this.params.get('id') > 0) {
            new CustomerContact().getById(this._db, this.params.get('id'))
                .subscribe((ac: CustomerContact) => {
                    this.contact = ac;
                });
        }
        this.contact.customerId = this.params.get('cid');
        Customer.getAll(this._db)
            .subscribe((c) => {
                this.custs = c;
            });
        new CustomerLocation().getByCustomerId(this._db, this.contact.customerId)
            .subscribe((sites) => this.sites = sites);
    }

    public save() {
        const loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.accSvc.getUserInfo()
            .flatMap((u) => {
                return new CustomerContact().addEditContact(this.contact, this.api, this._db);
            }).subscribe((success) => {
                loading.dismiss();
                if (success) {
                    this.dismiss(true);
                }
            }, () => {
                loading.dismiss();
            });
    }

    public dismiss(certChanged: boolean) {
        this.viewCtrl.dismiss(!!certChanged);
    }
}
