export interface IActionModel {
    id: number;
    customerId: number;
    visitId: number;
    name: string;
    detail: string;
    assignedToId: number;
    dueDate: string;
    priority: number;
}
