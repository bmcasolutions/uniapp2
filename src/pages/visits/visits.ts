import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController } from 'ionic-angular';
import { Observable } from 'rxjs';
import { Customer } from '../../models/customer';
import { CustomerVisit } from '../../models/customerVisit';
import { DatabaseProvider } from '../../providers/database';

@IonicPage()
@Component({
	selector: 'visits',
	templateUrl: 'visits.html'
})
export class VisitsComponent {

	public visits: any[] = [];

	private visitStatus: string = 'outstanding';

	constructor(public toastCtrl: ToastController,
		public navCtrl: NavController,
		private dbSvc: DatabaseProvider, private modalCtrl: ModalController) {
		this.navCtrl = navCtrl;
	}

	public ionViewDidEnter() {
		this.loadVisits();
	}

	public loadVisits() {
		CustomerVisit.getAll(this.dbSvc.database)
			.flatMap((cvs: any[]) => {
				cvs.forEach((cv) => {
					cv.text = cv.customerName ? cv.customerName : '';
					cv.color = (!!cv.isClosed || !!cv.complete) ? '#4CAF50' : (cv.isOther ? '#9C27B0' : '#3286d0');
				});
				return Observable.of(cvs);
			})
			.subscribe((cv) => {
				this.visits = cv.sort((a, b) => {
					if (b.startDate && !a.startDate) {
						return 1;
					}
					if (a.startDate && !b.startDate) {
						return -1;
					}
					if (!a.startDate && !b.startDate) {
						return 0;
					}
					return +new Date(b.startDate
						.replace(/(\d+)\/(\d+)\/(\d+)/, '$2/$1/$3'))
						- +new Date(a.startDate
							.replace(/(\d+)\/(\d+)\/(\d+)/, '$2/$1/$3'));
				});
			});
	}

	private outstandingVisits(v: CustomerVisit) {
		return !v.isClosed && !v.complete;
	}

	private completedVisits(v: CustomerVisit) {
		return !!v.isClosed || !!v.complete;
	}

	public visitDetail(id: number) {
		this.navCtrl.push('Visit_detailsComponent', { id });
	}

	public addEditVisit($event, cust) {
		$event.stopPropagation();
		cust = cust || { id: 0 };
		const modal = this.modalCtrl.create('VisitFormPage', {
			id: cust.id,
			action: 'quick'
		});
		modal.onDidDismiss((dataChanged) => {
			if (dataChanged) { this.loadVisits(); }
		});
		modal.present();
	}
}
