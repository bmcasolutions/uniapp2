import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { Customer } from '../../models/customer';
import { DatabaseProvider } from '../../providers/database';

@IonicPage()
@Component({
	selector: 'contacts',
	templateUrl: 'contacts.html'
})
export class ContactsComponent {

	public customers: any[] = [];
	private terms: string;

	constructor(public toastCtrl: ToastController,
		public navCtrl: NavController,
		private dbSvc: DatabaseProvider) {
		this.navCtrl = navCtrl;
	}

	public ionViewDidEnter() {
		this.loadCustomers();
	}

	public loadCustomers() {
		Customer.getAll(this.dbSvc.database)
			.subscribe((c) => {
				c.forEach((el) => {
					el.text = el.name;
				});
				this.customers = c;
			});
	}

	public customerContacts(id: number) {
		this.navCtrl.push('Customer_contactsComponent', { id });
	}
}
