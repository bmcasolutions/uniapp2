import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from 'ng2-translate/ng2-translate';
import { Contact_detailsComponent } from './contact_details';

@NgModule({
  declarations: [
    Contact_detailsComponent
  ],
  imports: [
    IonicPageModule.forChild(Contact_detailsComponent),
    TranslateModule
  ],
  exports: [
    Contact_detailsComponent,
    TranslateModule
  ]
})
export class Contact_detailsModule { }
