import { Component } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { IonicPage, LoadingController, NavParams, ViewController } from 'ionic-angular';
import { Customer } from '../../models/customer';
import { ICustomerAddModel, ICustomerEditModel } from '../../models/customer.models';
import { CustomerContact } from '../../models/customerContact';
import { CustomerLocation } from '../../models/customerLocation';
import { DatabaseProvider } from '../../providers/database';
import { AccountService } from '../../services/account.service';
import { ApiGateway } from '../../services/api.gateway.service';

@IonicPage()
@Component({
    selector: 'customer-form',
    templateUrl: 'customer.form.html'
})
export class CustomerFormPage {
    private _db: SQLiteObject;
    private custAdd: ICustomerAddModel;
    private custEdit: ICustomerEditModel;

    private sites: CustomerLocation[];
    private contacts: CustomerContact[];

    private positions = ['Director',
        'Regional Manager',
        'Branch Manager',
        'Account Manager',
        'Technical Manager',
        'Internal Sales',
        'Contract Manager',
        'Project Manager',
        'Engineer',
        'Estimator',
        'Buyer',
        'Supervisor',
        'Sales Representative',
        'Other'];

    constructor(public loadingCtrl: LoadingController,
        public params: NavParams, public viewCtrl: ViewController, db: DatabaseProvider,
        private api: ApiGateway,
        private accSvc: AccountService) {
        this._db = db.database;
        if (this.params.get('id') > 0) {
            const id = this.params.get('id');
            new Customer().getById(this._db, id)
                // .flatMap((ac) => new CustomerContact().getByCustomerId(this._db, id),
                //     (ac, contacts: CustomerContact[]) => {
                //         ac.contactId = (contacts.filter((c) => !!c.isDefault)[0] || { id: 0 }).id;
                //         return ac;
                //     })
                // .flatMap((ac) => new CustomerLocation().getByCustomerId(this._db, id),
                //     (ac, locations: CustomerLocation[]) => {
                //         ac.locationId = (locations.filter((c) => !!c.isDefault)[0] || { id: 0 }).id;
                //         return ac;
                //     })
                .subscribe((ac: Customer) => {
                    this.custEdit = {
                        id: this.params.get('id'),
                        contactId: '' + ac.contactId,
                        name: ac.name,
                        notes: ac.notes,
                        typeId: ac.typeId,
                        siteId: '' + ac.siteId,
                        keyRankId: ac.keyRankId
                    } as ICustomerEditModel;
                });
            new CustomerContact().getByCustomerId(this._db, id)
                .subscribe((contacts) => this.contacts = contacts);

            new CustomerLocation().getByCustomerId(this._db, id)
                .subscribe((locations) => this.sites = locations);
        } else {
            this.custAdd = {} as ICustomerAddModel;
        }
    }

    public save() {
        const loading = this.loadingCtrl.create({
            content: 'Please wait...'
        });
        loading.present();
        this.accSvc.getUserInfo()
            .flatMap((u) => {
                if (this.custAdd) {
                    return new Customer().addCustomer(this.custAdd, this.api, this._db, this.accSvc);
                } else {
                    return new Customer().editCustomer(this.custEdit, this.api, this._db, this.accSvc);
                }
            }).subscribe((success) => {
                loading.dismiss();
                if (success) {
                    this.dismiss(true);
                }
            }, () => {
                loading.dismiss();
            });
    }

    public dismiss(certChanged: boolean) {
        this.viewCtrl.dismiss(!!certChanged);
    }
}
